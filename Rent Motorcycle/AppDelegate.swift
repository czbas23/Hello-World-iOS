//
//  AppDelegate.swift
//  Rent Motorcycle
//
//  Created by CNT on 18/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import GoogleMaps
import IQKeyboardManagerSwift
import UserNotifications
import Firebase

let googleApiKey = "AIzaSyD8KeCO_llXp2r0goZzoFibgoK9ELUlh10"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        //set color UINavigationBar
        UINavigationBar.appearance().barTintColor = UIColor(red: 29.0/255.0, green: 175.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        GMSServices.provideAPIKey(googleApiKey)
        
        //delay splash screen
        Thread.sleep(forTimeInterval: 2.0)
        
        //regieter Notifications
        // iOS 10 ++ support
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
        application.registerForRemoteNotifications()
        
        
        /*
        //เชค notification เวลา app close
        //ถ้า app close ไม่เข้าเงื่อนไข didReceiveRemoteNotification
        if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
            
            let  userInfo = launchOptions![UIApplicationLaunchOptionsKey.remoteNotification] as! NSDictionary
            let aps = userInfo["aps"]! as! NSDictionary
            let alert = aps["alert"]! as! String
            print("aps: \(aps)")
            print("alert: \(alert)")
            
            let data_promotion = userInfo["data"]! as! NSDictionary
            //print("data: \(data_promotion)")
            if (data_promotion.count > 0){
                let num_id = data_promotion["promotion_id"]! as! Int
                let promotion_id = String(num_id)
                UserDefaults.standard.set(promotion_id, forKey: "notiPromotionID")
            }
        }*/
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        return true
    }
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        /*
        let state = UIApplication.shared.applicationState
        print(state)
        if state == .inactive {
            // inactive
            // Print notification payload data
            print("Push notification received: \(data)")
            let aps = data[AnyHashable("aps")]! as! NSDictionary
            let alert = aps["alert"]! as! String
            print("alert: \(alert)")
            let data_promotion = data[AnyHashable("data")]! as! NSDictionary
            //print("data: \(data_promotion)")
            if (data_promotion.count > 0){
                let num_id = data_promotion["promotion_id"]! as! Int
                let promotion_id = String(num_id)
                UserDefaults.standard.set(promotion_id, forKey: "notiPromotionID")
                
                //เปิดหน้า HomeViewController เพื่อจะมีหน้าสำหรับ back
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                let rootViewController = self.window!.rootViewController as! UINavigationController
                homeVC.openByPushNoti = true
                rootViewController.pushViewController(homeVC, animated: false)
            }
        }
        else {
            
        }
        */
    }
    
    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink) {
        guard let url = dynamicLink.url else {
            print("That's weird . My dynamic link object has no url")
            return
        }
        print("You incoming link parameter is \(url.absoluteString)")
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL {
            print("Incoming URL is \(incomingURL)")
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
                // ...
                guard error == nil else {
                    print("Found an error! \(error!.localizedDescription)")
                    return
                }
                if let dynamicLink = dynamiclink {
                    self.handleIncomingDynamicLink(dynamicLink)
                }
            }
            if linkHandled {
                return true
            }else {
                return false
            }
        }
        return false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) != nil {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            // ...
            return true
        }
        return false
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

