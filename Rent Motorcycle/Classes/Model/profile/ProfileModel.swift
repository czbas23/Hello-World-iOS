//
//  ProfileModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 16/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit

class ProfileModel: NSObject {
    
    var name: String?
    var surname: String?
    var name_prefix: String?
    var birthday: String?
    var email: String?
//    var phone_number: Int?
//    var calling_code: Int?
    var phone_number: String?
    var calling_code: String?
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        name = dict["name"] as? String
        surname =  dict["surname"] as? String
        name_prefix = dict["name_prefix"] as? String
        birthday = dict["birthday"] as? String
        email = dict["email"] as? String
//        phone_number = dict["phone_number"] as? Int
//        calling_code = dict["calling_code"] as? Int
        phone_number = dict["phone_number"] as? String
        calling_code = dict["calling_code"] as? String
    }

}
