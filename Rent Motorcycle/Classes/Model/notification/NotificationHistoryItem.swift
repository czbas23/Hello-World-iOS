//
//  NotificationHistoryItem.swift
//  Rent Motorcycle
//
//  Created by wisuij on 4/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit

class NotificationHistoryItem: NSObject {
    
    var notification_id: String?
    var title: String?
    var date: Int?
    var readed: Bool?
    var body: String?
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        notification_id = dict["id"] as? String
        title =  dict["title"] as? String
        date = dict["date"] as? Int
        readed = dict["readed"] as? Bool
        body = dict["body"] as? String
    }

}
