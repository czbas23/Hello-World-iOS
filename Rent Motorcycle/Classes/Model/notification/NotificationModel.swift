//
//  NotificationModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 4/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit

class NotificationModel: NSObject {
    
    var notificationHistoryList: [NotificationHistoryItem]? = [NotificationHistoryItem]()
    
    override init() {
        super.init()
    }
    
    public init(dict:[Dictionary<String,Any>]) {
        
        if let list = dict as? [Dictionary<String,AnyObject>] {
            for dict in list {
                let object = NotificationHistoryItem(dict: dict)
                self.notificationHistoryList?.append(object)
            }
        }
    }

}
