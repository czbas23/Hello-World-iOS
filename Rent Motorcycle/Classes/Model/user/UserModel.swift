//
//  UserModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 5/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class UserModel: NSObject {
    
    static var ApiToken:String!
    
    static var Balance:Float!
    static var Can_start_trip:Bool!
    
    static var Status:Int! //status การเข้าใช้งาน 1,2,3,4
    
    static var name:String!
    static var surname:String!
}

