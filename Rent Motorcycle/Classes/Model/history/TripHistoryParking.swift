//
//  TripHistoryParking.swift
//  Rent Motorcycle
//
//  Created by wisuij on 29/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class TripHistoryParking: NSObject {
    
    var parkingstart:TripHistoryParkingStart?
    var parkingend:TripHistoryParkingStart?
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        if let parking_start = dict["start"] as? Dictionary<String,AnyObject> {
            parkingstart = TripHistoryParkingStart(dict: parking_start)
        }
        if let parking_end = dict["end"] as? Dictionary<String,AnyObject> {
            parkingend = TripHistoryParkingStart(dict: parking_end)
        }
    }

}
