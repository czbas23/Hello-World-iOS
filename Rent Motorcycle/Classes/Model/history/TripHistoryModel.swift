//
//  TripHistoryModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 29/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class TripHistoryModel: NSObject {
    
    var tripHistoryList: [TripHistoryItem]? = [TripHistoryItem]()
    
    override init() {
        super.init()
    }
    
    public init(dict:[Dictionary<String,Any>]) {
        
        if let list = dict as? [Dictionary<String,AnyObject>] {
            for dict in list {
                let object = TripHistoryItem(dict: dict)
                self.tripHistoryList?.append(object)
            }
        }
    }

}
