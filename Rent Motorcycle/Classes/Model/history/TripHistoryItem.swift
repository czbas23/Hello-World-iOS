//
//  TripHistoryItem.swift
//  Rent Motorcycle
//
//  Created by wisuij on 29/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class TripHistoryItem: NSObject {
    
    var tripHistory_id: Int?
    var ref_code: String?
    var trip_date: String?
    var total_amount: Double?
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        tripHistory_id = dict["id"] as? Int
        ref_code =  dict["ref_code"] as? String
        trip_date = dict["trip_date"] as? String
        total_amount = dict["total_amount"] as? Double
    }


}
