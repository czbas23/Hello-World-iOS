//
//  ParkingModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 5/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class ParkingModel: NSObject {
    
    var parkingList: [ParkingItem]? = [ParkingItem]()
    
    override init() {
        super.init()
    }
    
    public init(dict:[Dictionary<String,Any>]) {
        //if let list = dict["results"] as? [Dictionary<String,AnyObject>] {
        if let list = dict as? [Dictionary<String,AnyObject>] {
            for dict in list {
                let object = ParkingItem(dict: dict)
                self.parkingList?.append(object)
            }
        }
    }

}
