//
//  PolygonItem.swift
//  Rent Motorcycle
//
//  Created by wisuij on 19/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class PolygonItem: NSObject {
    
    var lat: Double?
    var lng: Double?
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        lat = dict["lat"] as? Double
        lng = dict["lng"] as? Double
    }

}
