//
//  ParkingAreaModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 19/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class ParkingAreaModel: NSObject {
    
    var areaList: [AreaItem]? = [AreaItem]()
    
    override init() {
        super.init()
    }
    
    public init(dict:[Dictionary<String,Any>]) {
        //if let list = dict["results"] as? [Dictionary<String,AnyObject>] {
        if let list = dict as? [Dictionary<String,AnyObject>] {
            for dict in list {
                let object = AreaItem(dict: dict)
                self.areaList?.append(object)
            }
        }
    }


}
