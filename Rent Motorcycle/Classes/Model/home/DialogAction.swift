//
//  DialogAction.swift
//  Rent Motorcycle
//
//  Created by wisuij on 21/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class DialogAction: NSObject {

    var dialog_id: Int?
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        dialog_id = dict["dialog_id"] as? Int
    }
}
