//
//  AreaItem.swift
//  Rent Motorcycle
//
//  Created by wisuij on 19/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class AreaItem: NSObject {

    var area_id: Int?
    var name: String?
    var polygonList: [PolygonItem]? = [PolygonItem]()
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        area_id = dict["id"] as? Int
        name = dict["name"] as? String
        
        if let list = dict["polygon"] as? [Dictionary<String,AnyObject>] {
            for dict in list {
                let object = PolygonItem(dict: dict)
                self.polygonList?.append(object)
            }
        }
    }
    
}
