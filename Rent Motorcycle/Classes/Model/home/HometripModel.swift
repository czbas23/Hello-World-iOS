//
//  HometripModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 21/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class HometripModel: NSObject {

    var trip_id: Int?
    var finish_trip: Bool?
    var show_bill: Bool?
    var registration_number: String?
    var battery: Double?
    var distance: Double?
    var time: Int?
    var amount: Double?
    var lat: Double?
    var lng: Double?
    var trip_action:TripAction?
    var dialog:DialogAction?
    
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        trip_id = dict["trip_id"] as? Int
        finish_trip = dict["finish_trip"] as? Bool
        show_bill = dict["show_bill"] as? Bool
        registration_number = dict["registration_number"] as? String
        battery = dict["battery"] as? Double
        distance = dict["distance"] as? Double
        time = dict["time"] as? Int
        amount = dict["amount"] as? Double
        lat = dict["lat"] as? Double
        lng = dict["lng"] as? Double
        
        if let trip_action_data = dict["trip_action"] as? Dictionary<String,AnyObject> {
            trip_action = TripAction(dict: trip_action_data)
        }
        
        if let dialog_data = dict["dialog"] as? Dictionary<String,AnyObject> {
            dialog = DialogAction(dict: dialog_data)
        }
    }
    
}
