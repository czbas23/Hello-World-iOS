//
//  MotorcycleModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 20/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class MotorcycleModel: NSObject {
    
    var motorcycle_id: Int?
    var registration_number: String?
    var battery_level: Double?
    var battery_distance: Double?
    //var images: []
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        motorcycle_id = dict["id"] as? Int
        registration_number =  dict["registration_number"] as? String
        battery_level = dict["battery_level"] as? Double
        battery_distance = dict["battery_distance"] as? Double
    }

}
