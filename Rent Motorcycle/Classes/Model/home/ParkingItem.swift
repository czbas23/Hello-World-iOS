//
//  ParkingItem.swift
//  Rent Motorcycle
//
//  Created by wisuij on 5/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class ParkingItem: NSObject {
    
    var parking_id: Int?
    var name: String?
    var lat: Double?
    var lng: Double?
    var radius: Int?
    var number_of_motorcycle: Int?
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        parking_id = dict["id"] as? Int
        name =  dict["name"] as? String
        lat = dict["lat"] as? Double
        lng = dict["lng"] as? Double
        radius = dict["radius"] as? Int
        number_of_motorcycle = dict["number_of_motorcycle"] as? Int
    }

}
