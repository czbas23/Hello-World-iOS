//
//  ParkingIDModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 19/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class ParkingIDModel: NSObject {
    
    var parking_id: Int?
    var name: String?
    var lat: Double?
    var lng: Double?
    var radius: Int?
    var boarding_time: Int?
    var distance: Int?
    var motorcyclesList: [MotorcycleItem]? = [MotorcycleItem]()
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        
        parking_id = dict["id"] as? Int
        name = dict["name"] as? String
        lat = dict["lat"] as? Double
        lng = dict["lng"] as? Double
        radius = dict["radius"] as? Int
        boarding_time = dict["boarding_time"] as? Int
        distance = dict["distance"] as? Int
        
        if let list = dict["motorcycles"] as? [Dictionary<String,AnyObject>] {
            for dict in list {
                let object = MotorcycleItem(dict: dict)
                self.motorcyclesList?.append(object)
            }
        }
    }

}
