//
//  TripBillModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 21/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class TripBillModel: NSObject {
    
    var ref_code: String?
    var registration_number: String?
    var trip_date: String?
    var trip_time_start: String?
    var trip_time_finish: String?
    var distance: Double?
    var operating_time: Int?
    var total_amount: Double?
    var wallet_balance: Double?
    var pause_time:Int!
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        ref_code = dict["ref_code"] as? String
        registration_number = dict["registration_number"] as? String
        trip_date = dict["trip_date"] as? String
        
        trip_time_start = dict["trip_time_start"] as? String
        trip_time_finish = dict["trip_time_finish"] as? String
        distance = dict["distance"] as? Double
        
        operating_time = dict["operating_time"] as? Int
        total_amount = dict["total_amount"] as? Double
        wallet_balance = dict["wallet_balance"] as? Double
        pause_time = dict["pause_time"] as? Int

    }

}

