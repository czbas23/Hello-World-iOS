//
//  TripAction.swift
//  Rent Motorcycle
//
//  Created by wisuij on 21/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class TripAction: NSObject {
    
    var trip_action_id: Int?
    var code: Int?
    var runtime: Int?
    var countdown: Int?
    
    override init() {
        super.init()
    }
    
    public init(dict:Dictionary<String,Any>) {
        trip_action_id = dict["trip_action_id"] as? Int
        code = dict["code"] as? Int
        runtime = dict["runtime"] as? Int
        countdown = dict["countdown"] as? Int
    }

}
