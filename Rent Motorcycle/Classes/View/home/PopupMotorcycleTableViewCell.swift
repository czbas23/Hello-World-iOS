//
//  PopupMotorcycleTableViewCell.swift
//  Rent Motorcycle
//
//  Created by wisuij on 19/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class PopupMotorcycleTableViewCell: UITableViewCell {

    @IBOutlet weak var registrationNumberLabel: UILabel!
    @IBOutlet weak var batteryDistanceLabel: UILabel!
    
    static let identifier = "PopupMotorcycleTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(with registrationNumber:String ,battery:Double) {
        registrationNumberLabel.text = registrationNumber
        batteryDistanceLabel.text = "\(battery)"
    }

    
}
