//
//  CustomMarkerCircle.swift
//  Rent Motorcycle
//
//  Created by wisuij on 7/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class CustomMarkerCircle: UIView {

    @IBOutlet weak var totalMotocycleLabel: UILabel!
    @IBOutlet weak var mockUpView: UIView!
    
    class func instancefromNib() -> UIView {
        return UINib.init(nibName: "CustomMarkerCircle", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }

}
