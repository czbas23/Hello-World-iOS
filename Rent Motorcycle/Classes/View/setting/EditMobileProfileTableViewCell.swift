//
//  EditMobileProfileTableViewCell.swift
//  Rent Motorcycle
//
//  Created by wisuij on 5/3/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit

class EditMobileProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    static let identifier = "EditMobileProfileTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(title:String, value:String) {
        titleLabel.text = title
        valueLabel.text = value
    }
    
}
