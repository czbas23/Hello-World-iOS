//
//  MenuNotiItemTableViewCell.swift
//  Rent Motorcycle
//
//  Created by wisuij on 19/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit

class MenuNotiItemTableViewCell: UITableViewCell {

    @IBOutlet weak var mockCircleView: UIView!
    @IBOutlet weak var notiNumLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    
    static let identifier = "MenuNotiItemTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mockCircleView.layer.cornerRadius = mockCircleView.frame.width / 2
        mockCircleView.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(imagename:String, title:String, notificationNum:Int) {
        if notificationNum > 0 {
            mockCircleView.isHidden = false
            notiNumLabel.text = "\(notificationNum)"
        }else {
            mockCircleView.isHidden = true
        }
        titleLabel.text = title
        itemImageView.image = UIImage(named: imagename)
    }
    
}
