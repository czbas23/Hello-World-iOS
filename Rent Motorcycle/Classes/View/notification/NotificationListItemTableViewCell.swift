//
//  NotificationListItemTableViewCell.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class NotificationListItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mockView: UIView!
    static let identifier = "NotificationListItemTableViewCell"
    
    @IBOutlet weak var headeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(data:NotificationHistoryItem) {
        mockView.layer.cornerRadius = mockView.frame.width / 2
        self.headeLabel.text = data.title
        self.descLabel.text = data.body
        let date_timestamp = data.date
        let date = Utils.shared.convertTimestampToDateString(timestamp: date_timestamp!)
        self.dateLabel.text = date
        
        if let readed = data.readed {
            if readed {
                mockView.backgroundColor = UIColor.lightGray
                self.headeLabel.textColor = UIColor.lightGray
                self.descLabel.textColor = UIColor.lightGray
                self.dateLabel.textColor = UIColor.lightGray
            }
        }
    }
}
