//
//  HistoryItemTableViewCell.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class HistoryItemTableViewCell: UITableViewCell {
    
    static let identifier = "HistoryItemTableViewCell"
    
    @IBOutlet weak var ref_codeleLabel: UILabel!
    @IBOutlet weak var trip_dateLabel: UILabel!
    @IBOutlet weak var total_amountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(data:TripHistoryItem?) {
        if let ref_code = data?.ref_code {
            let ref_code_title = "TRIPNO".localized();
            ref_codeleLabel.text = "\(ref_code_title) : \(ref_code)"
        }
        if let trip_date = data?.trip_date {
            trip_dateLabel.text =  trip_date
        }
        if let total_amount = data?.total_amount {
            let total_title = "TOTALSERVICECHARGE".localized();
            let bath = "B.".localized();
            total_amountLabel.text = "\(total_title) \(total_amount) \(bath)"
        }
    }
    
}
