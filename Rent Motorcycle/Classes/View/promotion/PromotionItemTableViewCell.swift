//
//  PromotionItemTableViewCell.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class PromotionItemTableViewCell: UITableViewCell {
    
    @IBOutlet var promotionImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    static let identifier = "PromotionItemTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(data:JSON,index:Int) {
        /*
        if let image_url = data[index]["image"].string {
            let url = URL(string:image_url)
            self.promotionImageView.kf.setImage(with: url, placeholder: UIImage(named: "icon_user_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        }*/
        
        if let name = data[index]["name"].string {
            self.nameLabel.text = name
        }
        
    }
}
