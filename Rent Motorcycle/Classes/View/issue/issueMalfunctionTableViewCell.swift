//
//  issueMalfunctionTableViewCell.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit

class issueMalfunctionTableViewCell: UITableViewCell {
    
    @IBOutlet var reportProblemButton: UIButton!
    @IBOutlet var detailTextView: UITextView!
    
    
    @IBOutlet var add1ImageView: UIImageView!
    @IBOutlet var add2ImageView: UIImageView!
    @IBOutlet var add3ImageView: UIImageView!
    
    @IBOutlet var addImage1Button: UIButton!
    @IBOutlet var addImage2Button: UIButton!
    @IBOutlet var addImage3Button: UIButton!
    
    static let identifier = "issueMalfunctionTableViewCell"
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var addPhotoLabel: UILabel!
    @IBOutlet var detailtitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(imageArray:Array<UIImage>) {
        detailTextView.layer.borderWidth = 1
        detailTextView.layer.borderColor = UIColor.lightGray.cgColor
        detailTextView.layer.cornerRadius = 8
        
        reportProblemButton.layer.cornerRadius = 8
        
        titleLabel.text = "ATTACHPHOTOSMALFUNCTION".localized()
        addPhotoLabel.text = "ATTACHPHOTO".localized()
        detailtitleLabel.text = "DESCRIPTION".localized()
        reportProblemButton.setTitle("REPORTPROBLEM".localized(), for: UIControl.State.normal)
        
        
        for index in 0..<imageArray.count {
            if index == 0 {
                add1ImageView.image = imageArray[index]
            }else if index == 1 {
                add2ImageView.image = imageArray[index]
            }else if index == 2 {
                add3ImageView.image = imageArray[index]
            }else {
                
            }
        }
    }
    
}
