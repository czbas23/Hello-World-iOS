//
//  SelectIssueTableViewCell.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit
import DropDown

class SelectIssueTableViewCell: UITableViewCell {
    
    @IBOutlet var issueTextField: UITextField!
    @IBOutlet var selectIssueButton: UIButton!
    
    @IBOutlet var titleLabel: UILabel!
    
    static let identifier = "SelectIssueTableViewCell"
    let selectTypeDropDown = DropDown()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        issueTextField.layer.borderWidth = 1
        issueTextField.layer.borderColor = UIColor.lightGray.cgColor
        issueTextField.layer.cornerRadius = 8
        
        let titleButton = "- \("REPORTPROBLEM".localized()) -"
        issueTextField.text = titleButton
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(dataDropDown:[String]) {
        issueTextField.layer.borderWidth = 1
        issueTextField.layer.borderColor = UIColor.lightGray.cgColor
        issueTextField.layer.cornerRadius = 8
        
        titleLabel.text = "CHOSEPROBLEM".localized()
        
        //set drop down
        selectTypeDropDown.anchorView = self.selectIssueButton
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        selectTypeDropDown.bottomOffset = CGPoint(x: 0, y: self.selectIssueButton.bounds.height)
        let typeDataSource = dataDropDown
        selectTypeDropDown.dataSource = typeDataSource
    }
    
}
