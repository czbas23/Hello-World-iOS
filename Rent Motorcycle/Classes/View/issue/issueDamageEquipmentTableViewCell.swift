//
//  issueDamageEquipmentTableViewCell.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit
import M13Checkbox

class issueDamageEquipmentTableViewCell: UITableViewCell {
    
    @IBOutlet var registrationNumberTextField: UITextField!
    @IBOutlet var reportProblemButton: UIButton!
    @IBOutlet var detailTextView: UITextView!
    
    /*
    1=ไฟหน้า
    2=เบรก
    3=ยาง
    4=กระจกมองข้าง
    5=เบาะรถ
    6=ไฟหลัง
    7=ไฟเลี้ยว
    */
    
    @IBOutlet var equipment1Checkbox: M13Checkbox!
    @IBOutlet var equipment2Checkbox: M13Checkbox!
    @IBOutlet var equipment3Checkbox: M13Checkbox!
    @IBOutlet var equipment4Checkbox: M13Checkbox!
    @IBOutlet var equipment5Checkbox: M13Checkbox!
    @IBOutlet var equipment6Checkbox: M13Checkbox!
    @IBOutlet var equipment7Checkbox: M13Checkbox!
    @IBOutlet var equipment8Checkbox: M13Checkbox!

    @IBOutlet var add1ImageView: UIImageView!
    @IBOutlet var add2ImageView: UIImageView!
    @IBOutlet var add3ImageView: UIImageView!
    
    @IBOutlet var addImage1Button: UIButton!
    @IBOutlet var addImage2Button: UIButton!
    @IBOutlet var addImage3Button: UIButton!
    
    @IBOutlet var mockUpBGView: UIView!
    
    static let identifier = "issueDamageEquipmentTableViewCell"
    
    @IBOutlet var licentplateLabel: UILabel!
    @IBOutlet var brokenmorsaiLabel: UILabel!
    @IBOutlet var addPhotoLabel: UILabel!
    @IBOutlet var detailtitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(imageArray:Array<UIImage>,registrationNumber:String) {
        equipment1Checkbox?.boxType = .square
        equipment1Checkbox?.stateChangeAnimation = .fill
        equipment2Checkbox?.boxType = .square
        equipment2Checkbox?.stateChangeAnimation = .fill
        equipment3Checkbox?.boxType = .square
        equipment3Checkbox?.stateChangeAnimation = .fill
        equipment4Checkbox?.boxType = .square
        equipment4Checkbox?.stateChangeAnimation = .fill
        equipment5Checkbox?.boxType = .square
        equipment5Checkbox?.stateChangeAnimation = .fill
        equipment6Checkbox?.boxType = .square
        equipment6Checkbox?.stateChangeAnimation = .fill
        equipment7Checkbox?.boxType = .square
        equipment7Checkbox?.stateChangeAnimation = .fill
        equipment8Checkbox?.boxType = .square
        equipment8Checkbox?.stateChangeAnimation = .fill
        
        registrationNumberTextField.layer.borderWidth = 1
        registrationNumberTextField.layer.borderColor = UIColor.lightGray.cgColor
        registrationNumberTextField.layer.cornerRadius = 8
        //adding padding in left side my text field
        registrationNumberTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: registrationNumberTextField.frame.height))
        registrationNumberTextField.leftViewMode = .always
        
        mockUpBGView.layer.borderWidth = 1
        mockUpBGView.layer.borderColor = UIColor.lightGray.cgColor
        mockUpBGView.layer.cornerRadius = 8
        
        detailTextView.layer.borderWidth = 1
        detailTextView.layer.borderColor = UIColor.lightGray.cgColor
        detailTextView.layer.cornerRadius = 8
        
        reportProblemButton.layer.cornerRadius = 8
        
        registrationNumberTextField.text = registrationNumber
 
        licentplateLabel.text = "LICENSEPLATE".localized()
        brokenmorsaiLabel.text = "BROKENPART".localized()
        addPhotoLabel.text = "ATTACHPHOTO".localized()
        detailtitleLabel.text = "DESCRIPTION".localized()
        reportProblemButton.setTitle("REPORTPROBLEM".localized(), for: UIControl.State.normal)
        
        for index in 0..<imageArray.count {
            if index == 0 {
                add1ImageView.image = imageArray[index]
            }else if index == 1 {
                add2ImageView.image = imageArray[index]
            }else if index == 2 {
                add3ImageView.image = imageArray[index]
            }else {
                
            }
        }
    }
    
}
