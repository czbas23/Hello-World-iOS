//
//  TermsTableViewCell.swift
//  Rent Motorcycle
//
//  Created by wisuij on 31/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import M13Checkbox

class TermsTableViewCell: UITableViewCell {
    
    static let identifier = "TermsTableViewCell"
    @IBOutlet var registerButton: UIButton!
    @IBOutlet weak var knowCheckbox: M13Checkbox?
    @IBOutlet var webView: UIWebView!
    @IBOutlet weak var textAcceptLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell() {
        registerButton.layer.cornerRadius = 8
        registerButton.isHidden = true
        knowCheckbox?.boxType = .square
        knowCheckbox?.stateChangeAnimation = .fill
    }
    
    func setDataWebView(htmlData:String) {
        webView.loadHTMLString(htmlData, baseURL: nil)
    }
    
}
