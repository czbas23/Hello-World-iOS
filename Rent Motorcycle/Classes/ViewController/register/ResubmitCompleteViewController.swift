//
//  ResubmitCompleteViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 23/2/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit

class ResubmitCompleteViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonPress(_ sender: AnyObject) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MainMapViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

}
