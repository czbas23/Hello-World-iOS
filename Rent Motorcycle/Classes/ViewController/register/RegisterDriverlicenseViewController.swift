//
//  RegisterDriverlicenseViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 24/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class RegisterDriverlicenseViewController: UIViewController {
    
    var titlename:String!
    var name:String!
    var sername:String!
    var date:String!
    var email:String!
    var phone_number:String!
    var name_prefix:Int!
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var driverlicenseImageView: UIImageView!
    @IBOutlet var mockUpTitleView:UIView!
    
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    var driverlicenseImageForUplode:UIImage!
    @IBOutlet var mockUpButton:UIView!
    var isTakePhoto:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setLocalizableText()
    }
    
    @IBAction func takePhotoButtonPress(_ sender: AnyObject) {
        selectPhotoOrCamera()
    }
    
    @IBAction func okPhotoButtonPress(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RegisterPhotoViewController") as! RegisterPhotoViewController
        vc.name = self.name
        vc.sername = self.sername
        vc.date = self.date
        vc.email = self.email
        vc.phone_number = self.phone_number
        vc.titlename = self.titlename
        vc.name_prefix = self.name_prefix
        vc.driverlicenseImageForUplode = self.driverlicenseImageForUplode
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func cancelPhotoButtonPress(_ sender: AnyObject) {
        selectPhotoOrCamera()
    }

    func setupUI() {
        mockUpButton.isHidden = true
        nextButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setLocalizableText(){
        detailLabel.text = "TAKEPHOTODRIVINGLICENSE".localized();
        saveButton.setTitle("SAVE".localized(), for: UIControl.State.normal)
        cancelButton.setTitle("CANCEL".localized(), for: UIControl.State.normal)
        nextButton.setTitle("TAKEPHOTO".localized(), for: UIControl.State.normal)
    }
    
    func setUpData() {
        if isTakePhoto {
            mockUpTitleView.isHidden = true
            mockUpButton.isHidden = false
            nextButton.isHidden = true
        }else {
            mockUpTitleView.isHidden = false
            mockUpButton.isHidden = true
            nextButton.isHidden = false
        }
    }

}

extension RegisterDriverlicenseViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @objc func selectPhotoOrCamera() {
        let camera = DSCameraHandler(delegate_: self)
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        optionMenu.popoverPresentationController?.sourceView = self.view
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert : UIAlertAction!) in
            camera.getCameraOn(self, canEdit: false) //true สามารถ edit รูปได้
        }
        let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (alert : UIAlertAction!) in
            camera.getPhotoLibraryOn(self, canEdit: false) //true สามารถ edit รูปได้
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert : UIAlertAction!) in
        }
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //let image = info[UIImagePickerControllerEditedImage] as! UIImage
        //let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        isTakePhoto = true
        let image = info[.originalImage] as! UIImage
        
        picker.dismiss(animated: true, completion: nil)
        driverlicenseImageForUplode = image
        driverlicenseImageView.image = image
        setUpData()
    }
}
