//
//  ResubmitProfileViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 23/2/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import YYCalendar
import PopupDialog
import Alamofire

class ResubmitProfileViewController: BaseViewController {
    
    var is_profilenotComplete:Bool!
    var is_ImageDriverNotComplete:Bool!
    var is_ImageSefieNotComplete:Bool!

    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var sernameTextField: UITextField!
    @IBOutlet var dateTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var registerButton: UIButton!
    
    let nameprefixDropDown = DropDown()
    
    @IBOutlet var selectNamePrefixButton: UIButton!
    
    var viewModel:RegisterViewModel!
    var currentDate: String!
    var phone_number:String!
    var name_prefix:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RegisterViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        getNamePrefix()
    }
    
    // MARK: IBAction
    
    @IBAction func selectNamePrefixButtonPress(_ sender: AnyObject) {
        nameprefixDropDown.show()
    }
    
    @IBAction func dateButtonPress(_ sender: AnyObject) {
        showDialogDatePicker()
        /*
         currentDate = Utils.shared.timeLocalToTimeZone(date: Date(), formatter: "yyyy-MM-dd", timezone: "UTC")
         let calendar = YYCalendar(langType: .ENG, date: currentDate, format: "yyyy-MM-dd", disableAfterToday: false ) { (date) in
         self.dateTextField.text = date
         }
         calendar.show()*/
    }
    
    @IBAction func nextButtonPress(_ sender: AnyObject) {
        //check data
//        var is_ImageDriverNotComplete:Bool!
//        var is_ImageSefieNotComplete:Bool!
        if nameTextField.text != "" &&  sernameTextField.text != "" && dateTextField.text != "" && emailTextField.text != "" {
            
            if is_ImageDriverNotComplete {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ResubmitDriverlicenseViewController") as! ResubmitDriverlicenseViewController
                vc.name = nameTextField.text
                vc.sername = sernameTextField.text
                vc.date = dateTextField.text
                vc.email = ""
                vc.phone_number = "" 
                vc.titlename = titleTextField.text
                vc.name_prefix = self.name_prefix
                vc.is_ImageSefieNotComplete = self.is_ImageSefieNotComplete
                self.navigationController?.pushViewController(vc, animated:true)
            }else if is_ImageSefieNotComplete {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ResubmitPhotoViewController") as! ResubmitPhotoViewController
                vc.name = nameTextField.text
                vc.sername = sernameTextField.text
                vc.date = dateTextField.text
                vc.email = ""
                vc.phone_number = ""
                vc.titlename = titleTextField.text
                vc.name_prefix = self.name_prefix
                self.navigationController?.pushViewController(vc, animated:true)
            }else {
               //resubmit
                resubmit()
            }
        }else {
            
        }
    }
    
    func setupUI() {
        titleTextField.layer.borderWidth = 1
        titleTextField.layer.borderColor = UIColor.lightGray.cgColor
        titleTextField.layer.cornerRadius = 8
        
        nameTextField.layer.borderWidth = 1
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        nameTextField.layer.cornerRadius = 8
        
        sernameTextField.layer.borderWidth = 1
        sernameTextField.layer.borderColor = UIColor.lightGray.cgColor
        sernameTextField.layer.cornerRadius = 8
        
        dateTextField.layer.borderWidth = 1
        dateTextField.layer.borderColor = UIColor.lightGray.cgColor
        dateTextField.layer.cornerRadius = 8
        
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.cornerRadius = 8
        
        registerButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func getNamePrefix() {
        viewModel.getNamePrefix(api: "/name-prefix", parameters:nil)
    }
    
    func getProfile() {
        let token = UserModel.ApiToken
        viewModel.getProfile(api: "/setting/profile", parameters: nil, token: token!)
    }
    
    func resubmit() {
        print(name_prefix)
        //let phoneNumber:Int = Int(phone_number)!
        let name:String = nameTextField.text!
        let sername:String = sernameTextField.text!
        let date:String =  dateTextField.text!
        let namePrefix = String(name_prefix)
        let parameters: Parameters = [
            "name": name,
            "surname": sername,
            "name_prefix": namePrefix,
            "birthday": date
            ]
        viewModel.resubmit(api: "/register/resubmit", parameters: parameters, imageDrivingLicense: nil, imageSelfieDriving: nil)
    }
    
    func showDialogDatePicker() {
        // Create a custom view controller
        let vc = DialogDatePicker(nibName: "DialogDatePicker", bundle: nil)
        // Create the dialog
        let popup = PopupDialog(viewController: vc,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false)
        // Create button
        let buttonCancel = DefaultButton(title: "ยกเลิก", height: 50) {
            //print("You canceled the rating dialog")
        }
        let buttonOK = DefaultButton(title: "บันทึก", height: 50) {
            self.dateTextField.text = Utils.shared.timeLocalToTimeZone(date: vc.datePicker.date, formatter: "yyyy-MM-dd", timezone: "GMT+7")
        }
        buttonOK.titleColor = UIColor.white
        buttonOK.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.6862745098, blue: 0.9254901961, alpha: 1)
        
        vc.datePicker.datePickerMode = .date
        
        // Add buttons to dialog
        popup.addButtons([buttonCancel,buttonOK])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    // MARK: DropDown
    
    func setUpNameprefixDropDown(data:JSON) {
        nameprefixDropDown.anchorView = selectNamePrefixButton
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        nameprefixDropDown.bottomOffset = CGPoint(x: 0, y: selectNamePrefixButton.bounds.height)
        
        var nameprefixDataSource = [String]()
        var nameprefixIDDataSource = [Int]()
        let num_list:Int = data.count
        if num_list > 0 {
            for index in 0..<num_list {
                let name = data[index]["name_prefix"].stringValue
                nameprefixDataSource.append(name)
                let id = data[index]["id"].intValue
                nameprefixIDDataSource.append(id)
            }
        }else {
        }
        
        nameprefixDropDown.dataSource = nameprefixDataSource
        
        /*
         // You can also use localizationKeysDataSource instead. Check the docs.
         stationDropDown.dataSource = [
         "Lorem ipsum dolor",
         "sit amet consectetur",
         "cadipisci en..."
         ]*/
        
        // Action triggered on selection
        nameprefixDropDown.selectionAction = { [weak self] (index, item) in
            //self?.selectStationButton.setTitle(item, for: .normal)
            
            self?.titleTextField.text = "\(item)"
            self?.name_prefix = nameprefixIDDataSource[index]
            
        }
    }
    
    func setupData(data:ProfileModel!) {
        
        if let name_prefix = data.name_prefix {
            titleTextField.text = name_prefix
        }else {
            titleTextField.text = ""
        }
        
        if let name = data.name {
            nameTextField.text = name
        }else {
            nameTextField.text = ""
        }
        
        if let surname = data.surname {
            sernameTextField.text = surname
        }else {
            sernameTextField.text = ""
        }
        
        if let birthday = data.birthday {
            dateTextField.text = birthday
        }else {
            dateTextField.text = ""
        }
        
        if let email = data.email {
            emailTextField.text = email
        }else {
            emailTextField.text = ""
        }
        
        if let phone = data.phone_number {
            
        }else {
            
        }
    }
    
    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getProfile" {
            setupData(data:viewModel.profileModel)
        }else if viewModel.apiType == "resubmit" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ResubmitCompleteViewController") as! ResubmitDriverlicenseViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }else {
            setUpNameprefixDropDown(data:viewModel.namePrefixData)
            getProfile()
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }

}
