//
//  RegisterCompleteViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 24/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class RegisterCompleteViewController: UIViewController {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var detailLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setLocalizableText()
    }
    
    @IBAction func nextButtonPress(_ sender: AnyObject) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "MainMapViewController") as! MainMapViewController
//        self.navigationController?.pushViewController(vc, animated:true)
        
        //check first login เอาไว้เชคการแสดง How to ถ้า login ครั้งแรกจะแสดง
        if UserDefaults.standard.string(forKey:"FIRSTLOGIN") != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainMapViewController") as! MainMapViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }else {
            // login ครั้งแรก
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HowToViewController") as! HowToViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }

    func setupUI() {
        nextButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setLocalizableText(){
        detailLabel.text = "REGISTERREVIEW24HOURSE".localized();
        nextButton.setTitle("LETSTARTED".localized(), for: UIControl.State.normal)
    }

}
