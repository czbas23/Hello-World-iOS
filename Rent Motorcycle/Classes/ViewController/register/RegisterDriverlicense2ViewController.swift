//
//  RegisterDriverlicense2ViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 24/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class RegisterDriverlicense2ViewController: UIViewController {
    
    var name:String!
    var sername:String!
    var date:String!
    var email:String!
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var okButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    func setupUI() {
        cancelButton.layer.cornerRadius = 8
        okButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }

}
