//
//  RegisterPhotoViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 24/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterPhotoViewController: BaseViewController {
    
    var titlename:String!
    var name:String!
    var sername:String!
    var date:String!
    var email:String!
    var phone_number:String!
    let calling_code = "66"
    var name_prefix:Int!
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var mockUpTitleView:UIView!
    
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    var driverlicenseImageForUplode:UIImage!
    var registerPhotoImageForUplode:UIImage!
    
    @IBOutlet var mockUpButton:UIView!
    var isTakePhoto:Bool = false
    
    var viewModel:RegisterViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RegisterViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        setLocalizableText()
    }
    
    @IBAction func takePhotoButtonPress(_ sender: AnyObject) {
        selectPhotoOrCamera()
    }
    
    @IBAction func okPhotoButtonPress(_ sender: AnyObject) {
        //call api register
        register()
    }
    
    @IBAction func cancelPhotoButtonPress(_ sender: AnyObject) {
        selectPhotoOrCamera()
    }
    

    func setupUI() {
        mockUpButton.isHidden = true
        nextButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setLocalizableText(){
        detailLabel.text = "TAKEPHOTOSELFIEDRIVINGLICENSE".localized();
        saveButton.setTitle("SAVE".localized(), for: UIControl.State.normal)
        cancelButton.setTitle("CANCEL".localized(), for: UIControl.State.normal)
        nextButton.setTitle("SELFIELICENSE".localized(), for: UIControl.State.normal)
    }

    func setUpData() {
        if isTakePhoto {
            mockUpTitleView.isHidden = true
            mockUpButton.isHidden = false
            nextButton.isHidden = true
        }else {
            mockUpTitleView.isHidden = false
            mockUpButton.isHidden = true
            nextButton.isHidden = false
        }
    }
    
    func register() {
        print(name_prefix)
        //let phoneNumber:Int = Int(phone_number)!
        let namePrefix = String(name_prefix)
        let parameters: Parameters = [
            "name": name,
            "surname": sername,
            "name_prefix": namePrefix,
            "birthday": date,
            "locale": "th",
            "email": email,
            "phone_number": phone_number,
            "calling_code": calling_code,
        ]
        viewModel.register(api: "/register", parameters: parameters, imageDrivingLicense: driverlicenseImageForUplode, imageSelfieDriving: registerPhotoImageForUplode)
    }
    
    @objc override func onDataDidLoad() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RegisterCompleteViewController") as! RegisterCompleteViewController
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
}


extension RegisterPhotoViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @objc func selectPhotoOrCamera() {
        let camera = DSCameraHandler(delegate_: self)
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        optionMenu.popoverPresentationController?.sourceView = self.view
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert : UIAlertAction!) in
            camera.getCameraOn(self, canEdit: false) //true สามารถ edit รูปได้
        }
        let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (alert : UIAlertAction!) in
            camera.getPhotoLibraryOn(self, canEdit: false) //true สามารถ edit รูปได้
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert : UIAlertAction!) in
        }
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //let image = info[UIImagePickerControllerEditedImage] as! UIImage
        //let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        isTakePhoto = true
        let image = info[.originalImage] as! UIImage
        
        picker.dismiss(animated: true, completion: nil)
        registerPhotoImageForUplode = image
        photoImageView.image = image
        setUpData()
    }
}
