//
//  RegisterOTPViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 23/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterOTPViewController: BaseViewController {
    
    var titlename:String!
    var name:String!
    var sername:String!
    var date:String!
    var email:String!
    
    let calling_code = "66"
    var phone_number:String!
    var refCode:String!
    var expireMinute:String!
    var otpData:JSON!
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var otp1TextField: UITextField!
    @IBOutlet var otp2TextField: UITextField!
    @IBOutlet var otp3TextField: UITextField!
    @IBOutlet var otp4TextField: UITextField!
    @IBOutlet var otp5TextField: UITextField!
    @IBOutlet var otp6TextField: UITextField!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var inputLabel: UILabel!
    
    var viewModel:RegisterViewModel!
    
    var maxLen:Int = 1;
    
    var seconds = 180
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RegisterViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        setLocalizableText()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    @IBAction func nextButtonPress(_ sender: AnyObject) {
        timer.invalidate()
        verifyOTP()
    }
    
    @IBAction func requestOtpButtonPress(_ sender: UIButton) {
        timer.invalidate()
        requestOtp()
    }
    
    func setupUI() {
        //set up ui multi color detail Label
        let otp6digitLocalizable = "PLEASEINPUT6DIGITS".localized();
        let hindPhoneLocalizable = "HINDPHONE".localized();
        let refLocalizable = "RAF.NO.".localized();
        let expiredLocalizable = "EXPIREDIN".localized();
        let minLocalizable = "MIN.".localized();
        
        let expire = timeString(time: TimeInterval(seconds))
        let cut_phoneNumber = self.phone_number.substring(from: 5)
        //let stringValue = "กรุณาป้อน OTP 6 หลักที่ส่งถึง (+66) xx xxx" + cut_phoneNumber + " รหัสอ้างอิง " + self.refCode + " และจะหมดอายุภายใน " + expire + " นาที"
        let stringValue = otp6digitLocalizable + hindPhoneLocalizable + cut_phoneNumber + refLocalizable + self.refCode + expiredLocalizable + expire + minLocalizable
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        attributedString.setColorForText(textForAttribute: otp6digitLocalizable, withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: hindPhoneLocalizable + cut_phoneNumber, withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: refLocalizable, withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: self.refCode , withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: expiredLocalizable, withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: expire , withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: minLocalizable, withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        detailLabel.attributedText = attributedString
        //
        
        runTimer()
        
        otp1TextField.layer.borderWidth = 1
        otp1TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp1TextField.layer.cornerRadius = 8
        
        otp2TextField.layer.borderWidth = 1
        otp2TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp2TextField.layer.cornerRadius = 8
        
        otp3TextField.layer.borderWidth = 1
        otp3TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp3TextField.layer.cornerRadius = 8
        
        otp4TextField.layer.borderWidth = 1
        otp4TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp4TextField.layer.cornerRadius = 8
        
        otp5TextField.layer.borderWidth = 1
        otp5TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp5TextField.layer.cornerRadius = 8
        
        otp6TextField.layer.borderWidth = 1
        otp6TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp6TextField.layer.cornerRadius = 8
        
        nextButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
        
        otp1TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp2TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp3TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp4TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp5TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp6TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    func setLocalizableText(){
        inputLabel.text = "INPUTOTP".localized();
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(RegisterOTPViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func timeString(time:TimeInterval) -> String {
        //let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        //return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        return String(format:"%02i:%02i",minutes, seconds)
    }
    
    @objc func updateTimer() {
        seconds -= 1
        //timerLabel.text = timeString(time: TimeInterval(seconds))
        let expire = timeString(time: TimeInterval(seconds))
        let cut_phoneNumber = self.phone_number.substring(from: 5)
        
        let otp6digitLocalizable = "PLEASEINPUT6DIGITS".localized();
        let hindPhoneLocalizable = "HINDPHONE".localized();
        let refLocalizable = "RAF.NO.".localized();
        let expiredLocalizable = "EXPIREDIN".localized();
        let minLocalizable = "MIN.".localized();
        
        let stringValue = otp6digitLocalizable + hindPhoneLocalizable + cut_phoneNumber + refLocalizable + self.refCode + expiredLocalizable + expire + minLocalizable
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        attributedString.setColorForText(textForAttribute: otp6digitLocalizable, withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: hindPhoneLocalizable + cut_phoneNumber, withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: refLocalizable, withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: self.refCode , withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: expiredLocalizable, withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: expire , withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: minLocalizable, withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        detailLabel.attributedText = attributedString
        
        if seconds == 0 {
            timer.invalidate()
        }
    }
    
    func verifyOTP() {
        guard let t1 = otp1TextField.text,  let t2 = otp2TextField.text, let t3 = otp3TextField.text, let t4 = otp4TextField.text, let t5 = otp5TextField.text, let t6 = otp6TextField.text else {
        return
        }
        let otp = "\(t1)\(t2)\(t3)\(t4)\(t5)\(t6)"
        let parameters: Parameters = [
            "phone_number": phone_number,
            "calling_code": calling_code,
            "otp": otp
        ]
        viewModel.verifyOTP(api: "/register/verify-otp", parameters: parameters)
    }
    
    func requestOtp() {
        let parameters: Parameters = [
            "phone_number": phone_number,
            "calling_code": calling_code
        ]
        viewModel.requestOTP(api: "/register/request-otp", parameters: parameters)
    }

    @objc override func onDataDidLoad() {
        if viewModel.apiType == "requestOTP" {
            self.otpData = viewModel.otpData
            self.refCode =  self.otpData["ref"].stringValue
            self.expireMinute = self.otpData["expire_minute"].stringValue
            seconds = 180
            runTimer()
        }else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
            vc.phone_number = self.phone_number
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
    
}

extension RegisterOTPViewController: UITextFieldDelegate{
    
    @objc func textFieldDidChange(textField: UITextField)
    {
        let text = textField.text
        if text?.count == 1 {
            switch textField {
            case otp1TextField:
                otp2TextField.becomeFirstResponder()
            case otp2TextField:
                otp3TextField.becomeFirstResponder()
            case otp3TextField:
                otp4TextField.becomeFirstResponder()
            case otp4TextField:
                otp5TextField.becomeFirstResponder()
            case otp5TextField:
                otp6TextField.becomeFirstResponder()
            case otp6TextField:
                otp6TextField.resignFirstResponder()
            default:
                break
            }
        } else {
            switch textField {
            case otp6TextField:
                otp5TextField.becomeFirstResponder()
            case otp5TextField:
                otp4TextField.becomeFirstResponder()
            case otp4TextField:
                otp3TextField.becomeFirstResponder()
            case otp3TextField:
                otp2TextField.becomeFirstResponder()
            case otp2TextField:
                otp1TextField.becomeFirstResponder()
            case otp1TextField:
                otp1TextField.resignFirstResponder()
            default:
                break
            }
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= maxLen
    }
}
