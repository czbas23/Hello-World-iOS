//
//  RegisterByPhoneViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 23/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterByPhoneViewController: BaseViewController {
    
    var titlename:String!
    var name:String!
    var sername:String!
    var date:String!
    var email:String!
    
    let calling_code = "66"
    var refCode:String!
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var inputMobileLabel: UILabel!
    var viewModel:RegisterViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RegisterViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        setLocalizableText()
    }
    
    @IBAction func nextButtonPress(_ sender: AnyObject) {
        requestOtp()
    }
    
    func setupUI() {
        //detailLabel.text = "ใส่หมายเลขโทรศัพท์มือถือของคุณ\nระบบจะส่ง OTP ให้คุณยืนยัน"
        
        phoneTextField.layer.borderWidth = 1
        phoneTextField.layer.borderColor = UIColor.lightGray.cgColor
        phoneTextField.layer.cornerRadius = 8
        
        nextButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setLocalizableText(){
        inputMobileLabel.text = "INPUTYOUMOBILE".localized();
        detailLabel.text = "OTPWILLSENDYOUMOBILE".localized();
        nextButton.setTitle("NEXT".localized(), for: UIControl.State.normal)
    }
    
    func requestOtp() {
        let parameters: Parameters = [
            "phone_number": phoneTextField.text!,
            "calling_code": calling_code
        ]
        viewModel.requestOTP(api: "/register/request-otp", parameters: parameters)
    }

    @objc override func onDataDidLoad() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RegisterOTPViewController") as! RegisterOTPViewController
        //vc.name = self.name
        //vc.sername = self.sername
        //vc.date = self.date
        //vc.email = self.email
        vc.phone_number = phoneTextField.text
        //vc.titlename = self.titlename
        if let ref_code = viewModel.otpData["ref"].string {
            vc.refCode = ref_code
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
}
