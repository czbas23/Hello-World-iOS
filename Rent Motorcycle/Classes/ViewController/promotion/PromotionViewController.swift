//
//  PromotionViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import SwiftyJSON

class PromotionViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var viewModel:PromotionViewModel!
    var isLodeData:Bool = false
    var promotionData:JSON!
    var promotionFindIDData:JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = PromotionViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        setupTableView()
        getPromotion()
        setLocalizableText()
    }

    func setLocalizableText(){
        self.navigationItem.title = "PROMOTION".localized();
    }
    
    func setupUI() {
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: PromotionItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: PromotionItemTableViewCell.identifier)
        
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func getPromotion() {
        let token = UserModel.ApiToken
        viewModel.getPromotion(api: "/promotion", parameters: nil, token: token!)
    }
    
    func getPromotionFindID(promotionId:Int) {
        let token = UserModel.ApiToken
        let url_api = "/promotion"
        let url_data = "/\(promotionId)"
        viewModel.getPromotionFindID(api: url_api + url_data, parameters: nil, token: token!)
    }

    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getPromotion" {
            isLodeData = true
            self.promotionData = viewModel.promotionData
            tableView.reloadData()
        }else if viewModel.apiType == "getPromotionFindID" {
            self.promotionFindIDData = viewModel.promotionFindIDData
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PromotionDetailViewController") as! PromotionDetailViewController
            vc.promotionFindIDData = self.promotionFindIDData
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
}

extension PromotionViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLodeData {
            return self.promotionData.count
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PromotionItemTableViewCell.identifier, for: indexPath) as? PromotionItemTableViewCell else {
            fatalError("Wrong Cell")
        }
        cell.setCell(data: self.promotionData,index:indexPath.row)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let promotionId = self.promotionData[indexPath.row]["id"].int {
            getPromotionFindID(promotionId:promotionId)
        }
    }
    
}
