//
//  PromotionDetailViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit
import SwiftyJSON

class PromotionDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var promotionFindIDData:JSON!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setupTableView()
        setLocalizableText()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "PROMOTION".localized();
    }
    
    func setupUI() {
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: PromotionDetailItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: PromotionDetailItemTableViewCell.identifier)
        
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }

}

extension PromotionDetailViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PromotionDetailItemTableViewCell.identifier, for: indexPath) as? PromotionDetailItemTableViewCell else {
            fatalError("Wrong Cell")
        }
        cell.setCell(data:self.promotionFindIDData)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
