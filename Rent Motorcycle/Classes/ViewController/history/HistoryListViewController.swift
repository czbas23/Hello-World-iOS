//
//  HistoryListViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class HistoryListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel:HistoryViewModel!
    var isLodeData:Bool = false
    var tripHistoryModel:TripHistoryModel!
    var tripHistoryAllList: [TripHistoryItem]? = [TripHistoryItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = HistoryViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupTableView()
        setupUI()
        getTripHistory()
        setLocalizableText()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: HistoryItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: HistoryItemTableViewCell.identifier)
        
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "TRIPHISTORY".localized();
    }
    
    func setupUI() {
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }

    func getTripHistory() {
        let token = UserModel.ApiToken
        viewModel.getTripHistory(api: "/trip/history", parameters: nil,token:token!)
    }
    
    func getFindTripHistory(id:Int) {
        let token = UserModel.ApiToken
        let url_api = "/trip/history"
        let url_data = "/\(id)"
        viewModel.getFindTripHistory(api: url_api + url_data, parameters: nil,token:token!)
    }

    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getTripHistory" {
            isLodeData = true
            self.tripHistoryModel = viewModel.tripHistoryModel
            tableView.reloadData()
        }else if viewModel.apiType == "getFindTripHistory" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HistoryDetailViewController") as! HistoryDetailViewController
            vc.findTripHistoryModel = viewModel.findTripHistoryModel
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
}

extension HistoryListViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLodeData {
            return (self.tripHistoryModel.tripHistoryList?.count)!
        }else {
           return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HistoryItemTableViewCell.identifier, for: indexPath) as? HistoryItemTableViewCell else {
            fatalError("Wrong Cell")
        }
        cell.setCell(data:self.tripHistoryModel.tripHistoryList?[indexPath.row])
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
        
        var id:Int!
        id = self.tripHistoryModel.tripHistoryList?[indexPath.row].tripHistory_id
        getFindTripHistory(id:id)
    }
    
}

