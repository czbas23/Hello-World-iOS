//
//  HistoryDetailViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 29/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import GoogleMaps

class HistoryDetailViewController: UIViewController {
    
    var findTripHistoryModel:FindTripHistoryModel!
    
    @IBOutlet var refcodeLabel: UILabel!
    @IBOutlet var registrationNumberLabel: UILabel!
    @IBOutlet var tripDateLabel: UILabel!
    @IBOutlet var triptimeStartLabel: UILabel!
    @IBOutlet var triptimeFinishLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var operatingTimeLabel: UILabel!
    @IBOutlet var totalAmountLabel: UILabel!
    @IBOutlet var pauseTimeLabel: UILabel!
    @IBOutlet var walletBalanceLabel: UILabel!
    
    @IBOutlet var tripsummaryLabel: UILabel!
    @IBOutlet var tripNoLabel: UILabel!
    @IBOutlet var licentplateLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var startTimeLabel: UILabel!
    @IBOutlet var stopTimeLabel: UILabel!
    @IBOutlet var totalUsageLabel: UILabel!
    @IBOutlet var drivingdistanceLabel: UILabel!
    @IBOutlet var pauseDulationLabel: UILabel!
    @IBOutlet var totalServiceChangeLabel: UILabel!
    
    @IBOutlet weak var mapView: GMSMapView!
    var current_zoom:Float = 13.5

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpData(data: self.findTripHistoryModel)
        setupMap()
        setupUI()
        setLocalizableText()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "TRIPHISTORY".localized();
        tripsummaryLabel.text = "\("TRIPSUMMARY".localized())"
        tripNoLabel.text = "\("TRIPNO".localized()) :"
        licentplateLabel.text = "\("LICENSEPLATE".localized()) :"
        dateLabel.text = "\("DATE".localized()) :"
        startTimeLabel.text = "\("STARTTIME".localized()) :"
        stopTimeLabel.text = "\("STOPTIME".localized()) :"
        totalUsageLabel.text = "\("TOTALUSAGETIME".localized()) :"
        drivingdistanceLabel.text = "\("DRIVINGDISTANCE".localized())"
        pauseDulationLabel.text = "\("PAUSEDURATION".localized())"
        totalServiceChangeLabel.text = "\("TOTALSERVICECHANGE".localized())"
    }
    
    func setupUI() {
        self.navigationItem.title = "TRIPHISTORY".localized();
    }
    
    func setUpData(data:FindTripHistoryModel) {
        
        let bath_text = "BATH".localized();
        let km_text = "KM.".localized();
        let min_text = "MIN.".localized();
        
        if let refcode = data.ref_code {
            refcodeLabel.text = refcode
        }
        
        if let registration = data.registration_number {
            registrationNumberLabel.text = registration
        }
        
        if let trip_date = data.trip_date {
            tripDateLabel.text = trip_date
        }
        
        if let triptime_start = data.trip_time_start {
            triptimeStartLabel.text = triptime_start //+ " น."
        }
        
        if let triptime_finish = data.trip_time_finish {
            triptimeFinishLabel.text = triptime_finish //+ " น."
        }
        
        if let distance = data.distance {
            //set ทศนิยม 2 ตำแหน่ง
            //let this_value = distance.rounded(toPlaces: 2)
            let this_value = String(format: "%.2f", distance)
            distanceLabel.text = "\(this_value) \(km_text)"
            //distanceLabel.text = "\(distance) กม."
        }
        if let operatingTime = data.operating_time {
            let time_str = Utils.shared.convertTimeSecondToTimeString(time: operatingTime)
            operatingTimeLabel.text = "\(time_str) \(min_text)"
        }
        if let totalAmount = data.total_amount {
            totalAmountLabel.text = "\(totalAmount) \(bath_text)"
        }
        /*
        if let walletBalance = data.wallet_balance {
            walletBalanceLabel.text = "\(walletBalance) บาท"
        }*/
        
        if let pauseTime = data.pause_time {
            let time_str = Utils.shared.convertTimeSecondToTimeString(time: pauseTime)
            pauseTimeLabel.text = "\(time_str) \(min_text)"
        }
    }
    
    func setupMap() {
        // Create a GMSCameraPosition that tells the map to display the
        let camera = GMSCameraPosition.camera(withLatitude: 13.752801, longitude: 100.501587, zoom: current_zoom) //zoom 5.5 see all map thailand
        mapView.camera = camera
        mapView.delegate = self
        setMarkerToMap(data: self.findTripHistoryModel.parking!)
    }

    func setMarkerToMap(data:TripHistoryParking) {
        var lat_camera:Double = 13.752801
        var long_camera:Double = 100.501587
        let num_stationlist:Int = 2 // start , end
        if num_stationlist > 0 {
            for index in 0..<num_stationlist {
                let marker = GMSMarker()
                // unwrap the optional lat,long string
                var lat:Double?
                var long:Double?
                
                if index == 0 {
                    //start
                    //lat = Double((data.parkingstart?.lat)!)
                    //long = Double((data.parkingstart?.lng)!)
                    
                    if let lat_double = data.parkingstart?.lat {
                        lat = lat_double
                    }else {
                        lat = nil
                    }
                    
                    if let long_double = data.parkingstart?.lng {
                        long = long_double
                    }else {
                        long = nil
                    }
                    
                }else {
                    //end
                    //lat = Double((data.parkingend?.lat)!)
                    //long = Double((data.parkingend?.lng)!)
                    
                    if let lat_double = data.parkingend?.lat {
                        lat = lat_double
                    }else {
                        lat = nil
                    }
                    
                    if let long_double = data.parkingend?.lng {
                        long = long_double
                    }else {
                        long = nil
                    }
                }
                
                if lat != nil {
                    /*
                     marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                     marker.title = data.parkingList?[index].name
                     let icon:UIImage = UIImage(named: "icon_marker_parking")!
                     marker.icon = icon
                     */
                    
                    // custom ui customMarker แล้ว cpu 100%
                    // ใช้ marker.iconView cpu จะทำงาน 100%
                    // เซท marker.tracksViewChanges = false เพื่อลดการใช้ cpu
                    marker.tracksViewChanges = false
                    marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                    let customMarker = CustomMarkerCircle.instancefromNib() as! CustomMarkerCircle
                    //customMarker.center = mapView.projection.point(for: marker.position)
                    customMarker.mockUpView.isHidden = true
                    marker.iconView = customMarker
                    marker.accessibilityLabel = "\(index)"
                    marker.map = mapView
                    
                    if index == 0 {
                        lat_camera = lat!
                        long_camera = long!
                    }
                }else {
                    //lat == nil
                }
                
               
            }
        }
        
        let camera = GMSCameraPosition.camera(withLatitude:lat_camera, longitude: long_camera, zoom: current_zoom)
        mapView.camera = camera
        
        setUPPolyline(data: self.findTripHistoryModel)
    }
    
    func setUPPolyline(data:FindTripHistoryModel) {
        let path = GMSMutablePath()
        let num_polyline:Int = (data.polyline?.count)!
        for index in 0..<num_polyline {
            let lat = Double((data.polyline?[index].lat)!)
            let long = Double((data.polyline?[index].lng)!)
            path.addLatitude(lat, longitude:long)
        }
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 5.0
        polyline.strokeColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        polyline.geodesic = true
        polyline.map = mapView
    }
}

// extension for GMSMapViewDelegate
extension HistoryDetailViewController: GMSMapViewDelegate {
    
    // tap map marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //print("didTap marker \(marker.title!)")
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let zoom = mapView.camera.zoom
        current_zoom = zoom
        //print("map zoom is ",String(zoom))
    }
}

