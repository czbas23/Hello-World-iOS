//
//  QRcodeScanViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 29/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader
import FirebaseDynamicLinks

class QRcodeScanViewController: UIViewController,QRCodeReaderViewControllerDelegate {
    
    @IBOutlet var flashlightButton: UIButton!
    
    var userlocation_latitude:Double!
    var userlocation_longitude:Double!
    
    @IBOutlet weak var previewView: QRCodeReaderView! {
        didSet {
            previewView.setupComponents(showCancelButton: false, showSwitchCameraButton: false, showTorchButton: false, showOverlayView: false, reader: reader)
        }
    }
    
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
        
        scanInPreviewAction()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //set color UINavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.clear
        //close flash
        self.closeFlash()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //set color UINavigationBar
        navigationController?.navigationBar.barTintColor = UIColor(red: 29.0/255.0, green: 175.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        //close flash
        self.closeFlash()
    }
    
    // MARK: IBAction
    
    @IBAction func flashlightButtonPress(_ sender: UIButton) {
        toggleFlash()
    }
    
    //open flash
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
                let image = UIImage(named: "flashlight_off")
                flashlightButton.setImage(image, for: .normal)
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
                let image = UIImage(named: "flashlight_on")
                flashlightButton.setImage(image, for: .normal)
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    func closeFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            device.torchMode = AVCaptureDevice.TorchMode.off
            let image = UIImage(named: "flashlight_off")
            flashlightButton.setImage(image, for: .normal)
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    func scanInPreviewAction() {
        guard checkScanPermissions(), !reader.isRunning else { return }
        reader.didFindCode = { result in
            //close flash
            self.closeFlash()
            print("Completion with result: \(result.value) of type \(result.metadataType)")
            
            /*
            //var firbase:FirebaseDynamicLinks!
            //let convert_DynamicLinks = DynamicLink.
            if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromUniversalLink: URL(string: result.value)!) {
                
                print(dynamicLink)
            }*/
            
            let resultString = "\(result.value)"
            var replaceString = resultString.replacingOccurrences(of: "https://moresai.com/v/", with: "", options: .literal, range: nil)
            
            //ตัด space เผื่อ url มามี spaces " https://moresai.com/v/"
            replaceString = replaceString.trimmingCharacters(in: .whitespaces)
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "StartMotorcycleViewController") as! StartMotorcycleViewController
            vc.qrcode = replaceString
            vc.userlocation_latitude = self.userlocation_latitude
            vc.userlocation_longitude = self.userlocation_longitude
            self.navigationController?.pushViewController(vc, animated:true)
        }
        reader.startScanning()
    }
    
    // MARK: - Actions
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            present(alert, animated: true, completion: nil)
            return false
        }
    }

    // MARK: - QRCodeReader Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    
}
