//
//  LoginViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 23/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Localize_Swift

class LoginViewController: UIViewController {
    
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    
    @IBOutlet weak var welcomeLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setLocalizableText()
        checkLocalLanguage()
        checkTokenLocal()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //hidden NavigationBar
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //show NavigationBar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func setupUI() {
        loginButton.layer.cornerRadius = 8
        registerButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setLocalizableText(){
        welcomeLabel.text = "WELCOME".localized();
        loginButton.setTitle("LOGIN".localized(), for: UIControl.State.normal)
        registerButton.setTitle("REGISTER".localized(), for: UIControl.State.normal)
    }
    
    func checkTokenLocal() {
        
        if let thisApiToken = UserDefaults.standard.string(forKey:"APITOKEN") {
            //มีข้อมูล token ที่เก็บไว้
            //set api token
            UserModel.ApiToken = thisApiToken
            
            //goto home
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainMapViewController") as! MainMapViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    
    func checkLocalLanguage() {
        //LOCAL_LANGUAGE
        if let thisLocalLanguage = UserDefaults.standard.string(forKey:"LOCAL_LANGUAGE") {
            //set Localize
            Localize.setCurrentLanguage(thisLocalLanguage)
        }else {
            //defalse language
            Localize.setCurrentLanguage("th")
        }
    }

}
