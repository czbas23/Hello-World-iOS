//
//  LoginByPhoneViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 24/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginByPhoneViewController: BaseViewController {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var inputMobileLabel: UILabel!
    
    let calling_code = "66"
    var maxLen:Int = 9;
    
    var viewModel: LoginViewModel!
    var otpData:JSON!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewModel = LoginViewModel(delegate: self)
        setupUI()
        setLocalizableText()
    }
    
    // MARK: IBAction
    
    @IBAction func nextButtonPress(_ sender: UIButton) {
        if phoneTextField.text != "" {
            requestOtp()
        }else {
            AlertMessageAction.sharedManager.showAlertMessageErrorTitle(vc: self, message: "กรุณากรอกหมายเลขโทรศัพท์มือถือของคุณ", buttonTitle: "ตกลง", title: "")
        }
    }

    // MARK: Function
    
    func setupUI() {
        //detailLabel.text = "ใส่หมายเลขโทรศัพท์มือถือของคุณ\nระบบจะส่ง OTP ให้คุณยืนยัน"
        
        phoneTextField.layer.borderWidth = 1
        phoneTextField.layer.borderColor = UIColor.lightGray.cgColor
        phoneTextField.layer.cornerRadius = 8
        nextButton.layer.cornerRadius = 8
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setLocalizableText(){
        inputMobileLabel.text = "INPUTYOUMOBILE".localized();
        detailLabel.text = "OTPWILLSENDYOUMOBILE".localized();
        nextButton.setTitle("NEXT".localized(), for: UIControl.State.normal)
    }
    
    func requestOtp() {
        let parameters: Parameters = [
            "phone_number": phoneTextField.text!,
            "calling_code": calling_code
        ]
        viewModel.requestOTP(api: "/login/request-otp", parameters: parameters)
    }

    
    @objc override func onDataDidLoad() {
        self.otpData = viewModel.otpData
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginOTPViewController") as! LoginOTPViewController
        vc.callingCode = calling_code
        vc.phoneNumber = phoneTextField.text!
        vc.refCode =  self.otpData["ref"].stringValue
        vc.expireMinute = self.otpData["expire_minute"].stringValue
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }

}

extension LoginByPhoneViewController: UITextFieldDelegate{
    
    // MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= maxLen
    }
}
