//
//  BaseViewController.swift
//  Cu Cudson
//
//  Created by wisuij on 7/21/2561 BE.
//  Copyright © 2561 wisuij. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension BaseViewController: BaseViewModelDelegate {
    @objc public func showLoading() {
        LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
    }
    
    @objc public func hideLoading() {
        LoadingActivityIndicator.sharedManager.hideActivityIndicator(uiView: view)
    }
    
    @objc public func onDataDidLoad() {
        
    }
    
    @objc public func onDataDidLoadErrorWithMessage(errorMessage: String) {
        
    }
    
    @objc public func onUploadeImageComplete() {
        
    }
    
    // Load data trip action
    @objc public func onDataTripActionDidLoad() {
        
    }
    
    @objc public func onDataTripActionDidLoadErrorWithMessage(errorMessage:String) {
        
    }
    
}
