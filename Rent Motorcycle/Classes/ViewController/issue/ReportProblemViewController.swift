//
//  ReportProblemViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown
import PopupDialog

class ReportProblemViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel:IssueViewModel!
    var imageArray:Array = [UIImage]()
    
    var selectType:Int! //0 = ทำงานผิดปกติ, 1 = อุปกรณ์ที่ตัวรถเสียหาย
    //var typeArray:Array = ["ทำงานผิดปกติ","อุปกรณ์ที่ตัวรถเสียหาย"]
    var typeArray:Array = ["MALFUNCTION".localized(),"COMPONENTSDAMAGED".localized()]
    
    var selectTypeDropDown = DropDown()
    var isSetDropDown:Bool = false
    
    var imageTag:Int = 0
    
    var motorcycleID:String!
    var registrationNumber:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = IssueViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        setupTableView()
        setLocalizableText()
    }
    
    // MARK: IBAction
    
    @IBAction func reportButtonPress(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "USAGEREPORTISSUES".localized();
    }
    
    func setupUI() {
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setUPData() {
        if selectType == 0 {
            
        }else {
            /*
            let indexPath = IndexPath(row: 1, section: 0)
            let cell : issueDamageEquipmentTableViewCell = tableView.cellForRow(at: indexPath) as! issueDamageEquipmentTableViewCell
            var id = ""
            if let motocycleid_ = motorcycleID {
                id = motocycleid_
            }
            cell.registrationNumberTextField.text = id*/
        }
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: issueMalfunctionTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: issueMalfunctionTableViewCell.identifier)
        
        tableView.register(UINib(nibName: SelectIssueTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: SelectIssueTableViewCell.identifier)
        
        tableView.register(UINib(nibName: issueDamageEquipmentTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: issueDamageEquipmentTableViewCell.identifier)
        
        
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    @objc func showDropDown(sender: UIButton) {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell : SelectIssueTableViewCell = tableView.cellForRow(at: indexPath) as! SelectIssueTableViewCell
        
        selectTypeDropDown = cell.selectTypeDropDown
        selectTypeDropDown.selectionAction = { [weak self] (index, item) in
            cell.issueTextField.text = item
            self?.selectType = index
            self?.imageArray.removeAll()
            self?.tableView.reloadData()
            self?.setUPData()
        }
        
        selectTypeDropDown.show()
    }
    
    @objc func addImageForIssue(sender: UIButton) {
        let tag = sender.tag
        imageTag = tag
        selectPhotoOrCamera()
    }
    
    @objc func setUpDataForIssue() {
        if selectType == 0 {
            let indexPath = IndexPath(row: 1, section: 0)
            let cell : issueMalfunctionTableViewCell = tableView.cellForRow(at: indexPath) as! issueMalfunctionTableViewCell
            let desc = cell.detailTextView.text
            issueMalfunction(desc: desc!, imageArr: imageArray)
        }else {
            /*
            1=ไฟหน้า
            2=เบรก
            3=ยาง
            4=กระจกมองข้าง
            5=เบาะรถ
            6=ไฟหลัง
            7=ไฟเลี้ยว
            8=อื่นๆ
            */
            
            let indexPath = IndexPath(row: 1, section: 0)
            let cell : issueDamageEquipmentTableViewCell = tableView.cellForRow(at: indexPath) as! issueDamageEquipmentTableViewCell
            
            var equipmentsArr:Array = [String]()
            if let selectequipment1 = cell.equipment1Checkbox?.checkState {
                if selectequipment1 == .checked {
                    let id:String = "1"
                    equipmentsArr.append(id)
                }
            }
            if let selectequipment2 = cell.equipment2Checkbox?.checkState {
                if selectequipment2 == .checked {
                    let id:String = "2"
                    equipmentsArr.append(id)
                }
            }
            if let selectequipment3 = cell.equipment3Checkbox?.checkState {
                if selectequipment3 == .checked {
                    let id:String = "3"
                    equipmentsArr.append(id)
                }
            }
            if let selectequipment4 = cell.equipment4Checkbox?.checkState {
                if selectequipment4 == .checked {
                    let id:String = "4"
                    equipmentsArr.append(id)
                }
            }
            if let selectequipment5 = cell.equipment5Checkbox?.checkState {
                if selectequipment5 == .checked {
                    let id:String = "5"
                    equipmentsArr.append(id)
                }
            }
            if let selectequipment6 = cell.equipment6Checkbox?.checkState {
                if selectequipment6 == .checked {
                    let id:String = "6"
                    equipmentsArr.append(id)
                }
            }
            if let selectequipment7 = cell.equipment7Checkbox?.checkState {
                if selectequipment7 == .checked {
                    let id:String = "7"
                    equipmentsArr.append(id)
                }
            }
            
            if let selectequipment7 = cell.equipment8Checkbox?.checkState {
                if selectequipment7 == .checked {
                    let id:String = "8"
                    equipmentsArr.append(id)
                }
            }
            
            let desc = cell.detailTextView.text
            let registration_number_str = cell.registrationNumberTextField.text
            //let id:Int = Int(motorcycleid_str!)!
            issueDamageEquipment(registrationNumber:registration_number_str! ,desc:desc! ,imageArr:imageArray,equipmentsArr:equipmentsArr)
            
        }
    }
    
    func issueMalfunction(desc:String ,imageArr:[UIImage]) {
        let token = UserModel.ApiToken
        let parameters: Parameters = [
            "description": desc
        ]
        viewModel.issueMalfunction(api: "/issue/malfunction", parameters: parameters, image: imageArr, token: token!)
    }
    
    func issueDamageEquipment(registrationNumber:String ,desc:String ,imageArr:[UIImage],equipmentsArr:Array<String>) {
        
        /*
        motorcycle_id *
        integer
        equipments *
        array
        description *
        string
        images *
        array*/
        
        //ใส่ค่า motorcycle_id = int error
        //ใส่ค่า equipments = [int] error
        //ต้องส่ง motorcycle_id เป็น = "4" ส่งเป็น int ในรูปแบบ string ใน parameter
                
        let token = UserModel.ApiToken
        let parameters: Parameters = [
            "description": desc,
            "registration_number": registrationNumber,
            "equipments": equipmentsArr,
        ]
        viewModel.issueDamageEquipment(api: "/issue/damage-equipment", parameters: parameters, image: imageArr, token: token!)
    }
    
    // MARK: DropDown
    
    /*
    func setupTypeDropDown() {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell : SelectIssueTableViewCell = tableView.cellForRow(at: indexPath) as! SelectIssueTableViewCell
        selectTypeDropDown.anchorView = cell.selectIssueButton
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        selectTypeDropDown.bottomOffset = CGPoint(x: 0, y: cell.selectIssueButton.bounds.height)
        let typeDataSource = typeArray
        selectTypeDropDown.dataSource = typeDataSource
        // Action triggered on selection
        selectTypeDropDown.selectionAction = { [weak self] (index, item) in
            cell.issueTextField.text = item
            self?.selectType = index
            self?.tableView.reloadData()
        }
    }*/
    
    func showDialogIssueComplete(title:String,message:String,imagename:String,buttontitle:String) {
        // Create a custom view controller
        let vc = DialogTripActionViewController(nibName: "DialogTripActionViewController", bundle: nil)
        // Create the dialog
        let popup = PopupDialog(viewController: vc,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false)
        // Create button
        let buttonOne = DefaultButton(title: buttontitle, height: 50) {
            self.navigationController?.popViewController(animated: true)
        }
        buttonOne.titleColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonOne.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.6862745098, blue: 0.9254901961, alpha: 1)
        
        vc.titleLabel.text = title
        vc.timeLabel.text = ""
        vc.messageLabel.text = message
        vc.iconImageView.image = UIImage(named:imagename)
        
        // Add buttons to dialog
        popup.addButtons([buttonOne])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    @objc override func onDataDidLoad() {
        if viewModel.apiType == "issueMalfunction" {
            showDialogIssueComplete(title:"REPORTPROBLEMCOMPLETETITLE".localized(),message:"REPORTPROBLEMCOMPLETEDESC".localized(),imagename:"icon_issue_complete",buttontitle:"DISMISS".localized())
            //showDialogIssueComplete(title:"แจ้งปัญหาการใช้งานแล้ว",message:"ทีมงานจะทำการตรวจสอบปัญหา และเร่งดำเนินการแก้ไข ขอบคุณสำหรับการแจ้งปัญหา",imagename:"icon_issue_complete",buttontitle:"รับทราบ")
        }else if viewModel.apiType == "issueDamageEquipment" {
            showDialogIssueComplete(title:"REPORTPROBLEMCOMPLETETITLE".localized(),message:"REPORTPROBLEMCOMPLETEDESC".localized(),imagename:"icon_issue_complete",buttontitle:"DISMISS".localized())
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }

}

extension ReportProblemViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectType == nil {
            return 1
        }else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SelectIssueTableViewCell.identifier, for: indexPath) as? SelectIssueTableViewCell else {
                fatalError("Wrong Cell")
            }
            cell.setCell(dataDropDown: self.typeArray)
            cell.selectIssueButton.addTarget(self, action: #selector(ReportProblemViewController.showDropDown(sender:)), for: .touchUpInside)
            return cell
        }else {
            if selectType == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: issueMalfunctionTableViewCell.identifier, for: indexPath) as? issueMalfunctionTableViewCell else {
                    fatalError("Wrong Cell")
                }
                cell.setCell(imageArray:imageArray)
                cell.reportProblemButton.addTarget(self, action: #selector(ReportProblemViewController.setUpDataForIssue), for: .touchUpInside)
                
                cell.addImage1Button.tag = 0
                cell.addImage2Button.tag = 1
                cell.addImage3Button.tag = 2
                cell.addImage1Button.addTarget(self, action: #selector(ReportProblemViewController.addImageForIssue(sender:)), for: .touchUpInside)
                cell.addImage2Button.addTarget(self, action: #selector(ReportProblemViewController.addImageForIssue(sender:)), for: .touchUpInside)
                cell.addImage3Button.addTarget(self, action: #selector(ReportProblemViewController.addImageForIssue(sender:)), for: .touchUpInside)
                
                return cell
                
                
            }else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: issueDamageEquipmentTableViewCell.identifier, for: indexPath) as? issueDamageEquipmentTableViewCell else {
                    fatalError("Wrong Cell")
                }
                var registration_number = ""
                if let number = registrationNumber {
                    registration_number = number
                }
                cell.setCell(imageArray:imageArray,registrationNumber:registration_number)
                cell.reportProblemButton.addTarget(self, action: #selector(ReportProblemViewController.setUpDataForIssue), for: .touchUpInside)
                
                cell.addImage1Button.tag = 0
                cell.addImage2Button.tag = 1
                cell.addImage3Button.tag = 2
                cell.addImage1Button.addTarget(self, action: #selector(ReportProblemViewController.addImageForIssue(sender:)), for: .touchUpInside)
                cell.addImage2Button.addTarget(self, action: #selector(ReportProblemViewController.addImageForIssue(sender:)), for: .touchUpInside)
                cell.addImage3Button.addTarget(self, action: #selector(ReportProblemViewController.addImageForIssue(sender:)), for: .touchUpInside)
                
                
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                // do here...
                //เชคว่า tableView lode complete
                //setupTypeDropDown()
            }
        }
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 120
        }else {
            if selectType == 0 {
                return 550
            }else {
                return 1050
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


extension ReportProblemViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @objc func selectPhotoOrCamera() {
        let camera = DSCameraHandler(delegate_: self)
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        optionMenu.popoverPresentationController?.sourceView = self.view
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert : UIAlertAction!) in
            camera.getCameraOn(self, canEdit: false) //true สามารถ edit รูปได้
        }
        let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (alert : UIAlertAction!) in
            camera.getPhotoLibraryOn(self, canEdit: false) //true สามารถ edit รูปได้
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert : UIAlertAction!) in
        }
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[.originalImage] as! UIImage
        if imageArray.count == 3 {
            imageArray.remove(at: imageTag)
            imageArray.insert(image, at: imageTag)
        }else {
            imageArray.append(image)
        }
        picker.dismiss(animated: true, completion: nil)
        tableView.reloadData()
    }
}
