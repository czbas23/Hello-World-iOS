//
//  MainMapViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 24/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import GoogleMaps
import SideMenu
import Alamofire
import SwiftyJSON
import PopupDialog

class MainMapViewController: BaseViewController {
    
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var scanButton: UIButton!
    @IBOutlet var statusButton: UIButton!
    @IBOutlet var pauseTripButton: UIButton!
    @IBOutlet var endTripButton: UIButton!
    @IBOutlet var issueButton: UIButton!
    
    @IBOutlet var menuView: UIView!
    @IBOutlet var refreshView: UIView!
    @IBOutlet var navView: UIView!
    @IBOutlet var mockTripOnView: UIView!
    @IBOutlet var mockBGTripOnView: UIView!
    
    @IBOutlet var mockDialogTripPauseView: UIView!
    @IBOutlet var mockBGDialogTripPauseView: UIView!
    @IBOutlet var timeIsPauseLabel: UILabel!
    @IBOutlet var titlePauseLabel: UILabel!
    @IBOutlet var descPauseLabel: UILabel!
    @IBOutlet var resumePauseButton: UIButton!
    
    //out of coverage
    @IBOutlet var mockDialogOutOfCoverageView: UIView!
    @IBOutlet var mockBGDialogOutOfCoverageView: UIView!
    @IBOutlet var timeIsCountdownLabel: UILabel!
    
    //pop up data motorcycle
    @IBOutlet var registrationNumberLabel:UILabel!
    @IBOutlet var batteryLevelLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    //
    
    @IBOutlet var menuButton: UIButton!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var alertPopUp:Bool = true

    var viewModel:MainMapViewModel!
    var parkingModel:ParkingModel!
    var parkingIDModel:ParkingIDModel!
    var parkingAreaModel:ParkingAreaModel!
    
    var isCreateAreaPolygons:Bool = false
    var mainfillingPolygon:GMSPolygon!
    var mainGMSline:GMSPolyline!
    
    private let locationManager = CLLocationManager()
    
    var userlocation_latitude:Double = 13.726875  //set sample เพื่อกรณีไม่ได้ค่า lat,lng จาก userlocation
    var userlocation_longitude:Double = 100.544513 //set sample
    var hometripModel:HometripModel!
    
    var isTripActionPause:Bool = false
    var isfinish_trip:Bool = false
    var timer = Timer()
    
    var isLodeHometrip:Bool = false
    
    //marker
    var motorcycleMarker:GMSMarker!
    var current_zoom:Float = 15.5 //13.5
    
    var idPageComeFrom:Int = 0 //0=currentpage || 1=scanqr,starttrip || 2=bill
    
    var isMoveTheMap:Bool = false
    var registrationNumberMotocycle:String!
    
    var timer_Pause = Timer()
    var currentTimePause:Int = 0
    var isRuntimePauseAction:Bool = false
    
    //exict area
    var timer_countdown = Timer()
    var currentTimeCountDown:Int = 0
    var isRuntimeCountDownAction:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        viewModel = MainMapViewModel(delegate: self)
        setupSideMenu()
        setupUI()
        setupMap()
        getUserStatus()
        
        //check first login
        setUpShowPromotion()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //set color UINavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.white
        //hind navigationItem back button
        self.navigationItem.setHidesBackButton(true, animated:false);
        //set ภาษา
        setLocalizableText()
        
        //ถ้าไม่ใช่การโหลดครั้งแรก
        if isLodeHometrip {
            //0=currentpage
            //1=scanqr,starttrip || 2=bill
            if idPageComeFrom == 2 {
                //clear map เวลากลับมาจากหน้า bill
                if motorcycleMarker != nil {
                    motorcycleMarker.map = nil
                    motorcycleMarker = nil
                }
                mapView.clear()
                //call ทุกอย่างใหม่
                getUserStatus()
                idPageComeFrom = 0
            }else if idPageComeFrom == 1 {
                //แสดง loading รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน loading ตาม trip_Action_code
                //LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
                //call home trip เวลากลับจากหน้าอื่น
                getHomeTrip()
                idPageComeFrom = 0
            }else {
                //กลับมาจากหน้าอื่นๆ idPageComeFrom = 3
                if motorcycleMarker != nil {
                    motorcycleMarker.map = nil
                    motorcycleMarker = nil
                }
                mapView.clear()
                //call ทุกอย่างใหม่
                getUserStatus()
                idPageComeFrom = 0
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //clear timer เวลาไปหน้าอื่น
        timer.invalidate()
        //clear timer pause
        timer_Pause.invalidate()
        //clear timmer countdown
        timer_countdown.invalidate()
        //เชค idPageComeFrom เท่ากับ 0 แสดงว่าไม่ได้ไป id 1กับ2,ถ้า 1 scanqr start trip ,2 bill
        if idPageComeFrom == 0 {
            //เซทไปหน้าอื่นๆ = 3
            idPageComeFrom = 3
        }
        
        //set color UINavigationBar
        navigationController?.navigationBar.barTintColor = UIColor(red: 29.0/255.0, green: 175.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        //hind navigationItem back button
        self.navigationItem.setHidesBackButton(false, animated:true);
    }
    
    // MARK: IBAction
    
    @IBAction func menuButtonPress(_ sender: UIButton) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func scanQRButtonPress(_ sender: UIButton) {
        if UserModel.Status == 1 {
            //เชคว่า สตารท trip ได้ไหม
            if UserModel.Can_start_trip {
                idPageComeFrom = 1 //1= scanqr , starttrip
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "QRcodeScanViewController") as! QRcodeScanViewController
                vc.userlocation_latitude = self.userlocation_latitude
                vc.userlocation_longitude = self.userlocation_longitude
                self.navigationController?.pushViewController(vc, animated:true)
            }else {
                //แสดง dialog เงินไม่พอ
                //showDialogAlert(mesasage: "ยอดเงินคงเหลือไม่เพียงพอต่อการเริ่มใช้งาน คุณสามารถเติมเงินได้ที่เมนูกระเป๋า", buttontitle: "ตกลง")
                let message = "YOURBALANCEISLESS".localized();
                let buttontitle = "OK".localized();
                showDialogAlert(mesasage: message, buttontitle: buttontitle)
            }
        }else if UserModel.Status == 2 {
            //แสดง dialog รออนุมัติ
            //showDialogAlert(mesasage: "ทีมงานกำลังจรวจสอบข้อมูลการลงทะเบียนของคุณ และจะแจ้งผลภายใน 24 ชั่วโมง", buttontitle: "ตกลง")
            let message = "REVIEWED24HR".localized();
            let buttontitle = "OK".localized();
            showDialogAlert(mesasage: message, buttontitle: buttontitle)
        }else if UserModel.Status == 3 {
            //ปฎิเสธการอนุมัติ
            //call getRejectreason
            getRejectreason()
        }else{
            //status 4 ปิดการใช้งาน
            //logout
        }
    }
    
    @IBAction func gotoMyLocationButtonPress(_ sender: UIButton) {
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: current_zoom)
        self.mapView.animate(to: camera)
    }
    
    @IBAction func refreshMapButtonPress(_ sender: UIButton) {
        isMoveTheMap = false
        if motorcycleMarker != nil {
            motorcycleMarker.map = nil
            motorcycleMarker = nil
        }
        mapView.clear()
        //call ทุกอย่างใหม่
        getUserStatus()
        idPageComeFrom = 0
    }
    
    @IBAction func pauseTripActionButtonPress(_ sender: UIButton) {
        tripActionPause()
    }
    
    @IBAction func resumeTripActionButtonPress(_ sender: UIButton) {
        tripActionResume()
    }
    
    @IBAction func endTripActionButtonPress(_ sender: UIButton) {
        showDialogConfirmEndTrip(title:"ท่านต้องการจบการใช้งานใช่หรือไม่",message:"อย่าลืมเสียบปลั๊กชาร์จ เพื่อให้เพื่อนๆได้ใช้งานต่อ",imagename:"icon_endtrip_confirm_dialog")
    }
    
    @IBAction func reportProblemButtonPress(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ReportProblemViewController") as! ReportProblemViewController
        vc.registrationNumber = registrationNumberMotocycle
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    func setupUI() {
        //alertPopUpView.isHidden = alertPopUp
        
        menuView.layer.cornerRadius = menuView.frame.width / 2
        refreshView.layer.cornerRadius = refreshView.frame.width / 2
        navView.layer.cornerRadius = navView.frame.width / 2
        
        statusButton.layer.cornerRadius = 8
        scanButton.layer.cornerRadius = 8
        
        let scan_text = "SCANTORIDE".localized();
        scanButton.setTitle(scan_text,for: .normal)
        
        statusButton.isHidden = true
        
        mockBGTripOnView.layer.cornerRadius = 8
        //ซ่อนแถบข้อมูลรถ ปุ่ม pause trip, ปุ่ม end trip ต้องอยู่ใน ทริบถึงแสดง
        mockTripOnView.isHidden = true
        
        //ซ่อน dialog pause trip
        mockDialogTripPauseView.isHidden = true
        mockBGDialogTripPauseView.layer.cornerRadius = 8
        
        pauseTripButton.layer.cornerRadius = 8
        endTripButton.layer.cornerRadius = 8
        issueButton.layer.cornerRadius = 8
        
        //ซ่อน dialog ออกนอกพื้นที่
        mockDialogOutOfCoverageView.isHidden = true
        mockBGDialogOutOfCoverageView.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
        
    }
    
    func setLocalizableText(){
        titlePauseLabel.text = "PAUSE".localized();
        let desc_lin1 = "YOURTRIPHASBEENPAUSED".localized();
        let desc_line2 = "PLEASEPRESSSTART".localized();
        descPauseLabel.text = "\(desc_lin1)\n\(desc_line2)"
        resumePauseButton.setTitle("START".localized(), for: UIControl.State.normal)
        pauseTripButton.setTitle("PAUSEBUTTON".localized(), for: UIControl.State.normal)
        endTripButton.setTitle("ENDTRIP".localized(), for: UIControl.State.normal)
    }
    
    func setUpShowPromotion() {
        if UserDefaults.standard.string(forKey:"FIRSTLOGIN") != nil {
            //promotion show ทุกครั้งที่เปิด app
            //show promotion
            getPromotionData()
        }else {
            //set first login
            UserDefaults.standard.set("FIRST", forKey:"FIRSTLOGIN")
            //show promotion
            getPromotionData()
        }
    }
    
    func getPromotionData() {
        let token = UserModel.ApiToken
        getPromotion(api: "/promotion", parameters: nil, token: token!)
    }
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // Set up a cool background image for demo purposes
        //SideMenuManager.default.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        
        //mode show menu
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        
        //set width menuslide
        SideMenuManager.default.menuWidth = view.frame.width - 40
        
        //เซทไม่ให้ back ground ด้านบน ดำ
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor.clear
    }
    
    func setupMap() {
        // Create a GMSCameraPosition that tells the map to display the
        let camera = GMSCameraPosition.camera(withLatitude: 13.752801, longitude: 100.501587, zoom: current_zoom) //zoom 5.5 see all map thailand
        mapView.camera = camera
        mapView.delegate = self
        mapView.setMinZoom(5, maxZoom: 18)
    }
    
    func setupLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
    }
    
    func getUserStatus() {
        let token = UserModel.ApiToken
        viewModel.getUserStatus(api: "/user/status", parameters: nil,token:token!)
    }
    
    func getBalance() {
        let token = UserModel.ApiToken
        viewModel.getBalance(api: "/wallet/balance", parameters: nil, token: token!)
    }
    
    func getHomeParking() {
        let token = UserModel.ApiToken
        viewModel.getHomeParking(api: "/home/parking", parameters: nil,token:token!)
    }
    
    func getHomeParkingArea() {
        let token = UserModel.ApiToken
        viewModel.getHomeArea(api: "/home/area", parameters: nil,token:token!)
    }
    
    func getHomeParkingFindID(id:Int,device_lat:Double,device_lng:Double) {
        let token = UserModel.ApiToken
        let url_api = "/home/parking"
        let url_data = "/\(id)?device_lat=\(device_lat)&device_lng=\(device_lng)"
        viewModel.getHomeParkingFindID(api: url_api + url_data, parameters: nil,token:token!)
    }
    
    func getHomeTrip() {
        let token = UserModel.ApiToken
        viewModel.getHometrip(api: "/home/trip", parameters: nil,token:token!)
    }
    
    func tripActionStart(motorcycle_id:Int) {
        let token = UserModel.ApiToken
        let device_parameters:Parameters = [
            "lat": userlocation_latitude,
            "lng": userlocation_longitude
        ]
        let parameters: Parameters = [
            "motorcycle_id": motorcycle_id,
            "device_location": device_parameters
        ]
        viewModel.tripActionStart(api: "/trip/action/start", parameters: parameters, token: token!)
    }
    
    func tripActionPause() {
        let token = UserModel.ApiToken
        viewModel.tripActionPause(api: "/trip/action/pause", parameters: nil, token: token!)
    }
    
    func tripActionResume() {
        let token = UserModel.ApiToken
        viewModel.tripActionResume(api: "/trip/action/resume", parameters: nil, token: token!)
    }
    
    func tripActionEnd() {
        let token = UserModel.ApiToken
        viewModel.tripActionEnd(api: "/trip/action/end", parameters: nil, token: token!)
    }
    
    func getRejectreason() {
        let token = UserModel.ApiToken
        viewModel.getRejectreason(api: "/register/reject-reason", parameters: nil, token: token!)
    }
    
    func getPromotion(api:String ,parameters:Parameters?, token:String) {
        APIService.sharedManager.getRequestAPIWithHeaderAndResponseList(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                if(responseObject != nil){
                    var promotionData = JSON(responseObject!)
                    //print(promotionData)
                    if promotionData.count > 0 {
                        if let url = promotionData[0]["image"].string {
                            self.popUPPromotion(imageUrl:url)
                        }
                    }
                }else{
                    //let errorMessage = error
                }
            }
        }
    }
    
    func setMarkerParkingToMap(data:ParkingModel) {
        var lat_camera:Double = 13.752801
        var long_camera:Double = 100.501587
        let num_stationlist:Int = (data.parkingList?.count)!
        if num_stationlist > 0 {
            for index in 0...num_stationlist-1 {
                let marker = GMSMarker()
                // unwrap the optional lat,long string
                let lat = Double((data.parkingList?[index].lat)!)
                let long = Double((data.parkingList?[index].lng)!)
                
                /*
                marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                marker.title = data.parkingList?[index].name
                let icon:UIImage = UIImage(named: "icon_marker_parking")!
                marker.icon = icon
                */
                
                // custom ui customMarker แล้ว cpu 100%
                // ใช้ marker.iconView cpu จะทำงาน 100%
                // เซท marker.tracksViewChanges = false เพื่อลดการใช้ cpu
                marker.tracksViewChanges = false
                let num_motocycle:Int = (data.parkingList?[index].number_of_motorcycle) ?? 0
                marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                marker.title = data.parkingList?[index].name
                let customMarker = CustomMarkerCircle.instancefromNib() as! CustomMarkerCircle
                //customMarker.center = mapView.projection.point(for: marker.position)
                if num_motocycle > 0 {
                    customMarker.totalMotocycleLabel.text = String(num_motocycle)
                }else {
                    customMarker.mockUpView.isHidden = true
                }
                marker.iconView = customMarker
                marker.accessibilityLabel = "\(index)"
                marker.map = mapView
                
                if index == 0 {
                    lat_camera = lat
                    long_camera = long
                }
            }
        }
        
        let camera = GMSCameraPosition.camera(withLatitude:lat_camera, longitude: long_camera, zoom: current_zoom)
        mapView.camera = camera
        
        setupLocation()
    }
    
    func setUpPolygonsMap(data:ParkingAreaModel) {
        // 1. Create area earth fill polygon
        let fillingPath = GMSMutablePath()
        fillingPath.addLatitude(-85.0511, longitude: -180.0)
        fillingPath.addLatitude(-85.0511, longitude: 0)
        fillingPath.addLatitude(-85.0511, longitude: 179.9999)
        fillingPath.addLatitude(85.0511, longitude: 179.9999)
        fillingPath.addLatitude(85.0511, longitude: 0)
        fillingPath.addLatitude(85.0511, longitude: -180)
        
        let fillingPolygon = GMSPolygon(path:fillingPath)
        let fillColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.26)
        fillingPolygon.fillColor = fillColor
        fillingPolygon.map = self.mapView
        
        var holesAreaPolygon = [GMSMutablePath]()
        let area_num:Int = (data.areaList?.count)!
        for i in 0 ..< area_num {
            let hold_rect = GMSMutablePath()
            let polygon_num:Int = (data.areaList?[i].polygonList?.count)!
            for a in 0 ..< polygon_num {
                let lat = data.areaList?[i].polygonList?[a].lat
                let lng = data.areaList?[i].polygonList?[a].lng
                hold_rect.add(CLLocationCoordinate2DMake(lat!, lng!))
            }
            holesAreaPolygon.append(hold_rect)
        }
        
        // 2. Add prepared array of GMSPath , holes Area
        fillingPolygon.holes = holesAreaPolygon
        
        // 3. Add lines for boundaries
        //var line:GMSPolyline!
        for path in holesAreaPolygon {
            let line = GMSPolyline(path: path)
            //line = GMSPolyline(path: path)
            line.map = self.mapView
            line.strokeColor = #colorLiteral(red: 0.9647058824, green: 0.5764705882, blue: 0.5764705882, alpha: 1)
            line.strokeWidth = 2
            
            mainGMSline = line
        }
        
        mainfillingPolygon = fillingPolygon
        
    }
    
    func addMarkerMotorcycleToMap(data:HometripModel) {
        if motorcycleMarker == nil {
            let lat = Double((data.lat)!)
            let long = Double((data.lng)!)
            
            let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let marker = GMSMarker(position: position)
            marker.icon = UIImage(named: "icon_motorcycle")
            motorcycleMarker = marker
            //marker to top
            motorcycleMarker.zIndex = 1;
            motorcycleMarker.map = mapView
        }
    }
    
    func updateMarkerMotorcycleToMap(data:HometripModel) {
        if motorcycleMarker != nil {
            let lat = Double((data.lat)!)
            let long = Double((data.lng)!)

            if !isMoveTheMap {
                //check trip_action code ว่าพร้อมใช้งานหรือเปล่าค่อย set ตำแหน่งรถไม่งั้นไป set lat long = 0
                //code = 500 ผู้ใช้งานสั่งเริ่มการเดินทาง กำลังดำเนินการ
                if let code = data.trip_action?.code {
                    if code == 500 {
                        //waiting
                    }else {
                        //check lat long != 0
                        if lat != 0 && long != 0 {
                            let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                            motorcycleMarker.position = position
                            
                            let camera = GMSCameraPosition.camera(withLatitude:lat, longitude: long, zoom: current_zoom)
                            //mapView.camera = camera
                            //set animation
                            mapView.animate(to: camera)
                        }else {
                            
                        }
                    }
                }
            }
        }
    }
    
    // MARK: Popup Dialog
    
    func popUPPromotion(imageUrl:String) {
        let vc = PopUpPromotionViewController(nibName: "PopUpPromotionViewController", bundle: nil)
        let popup = PopupDialog(viewController: vc,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false)
        // Create button
        let buttonOne = DefaultButton(title: "X", height: 50) {
            
        }
        buttonOne.titleColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        buttonOne.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.6862745098, blue: 0.9254901961, alpha: 0)
        
        let url = URL(string:imageUrl)
        vc.promotionImageView.kf.setImage(with: url, placeholder: UIImage(named: "icon_user_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        
        popup.addButtons([buttonOne])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    func popUpParkingDeatail(name:String,time:String,distance:String) {
        let motocyclist = (self.parkingIDModel.motorcyclesList?.count)!
        if motocyclist > 0 {
            // Create a custom view controller
            let vc = PopUpParkingDetailViewController(nibName: "PopUpParkingDetailViewController", bundle: nil)
            // Create the dialog
            let popup = PopupDialog(viewController: vc,
                                    buttonAlignment: .horizontal,
                                    transitionStyle: .zoomIn,
                                    tapGestureDismissal: true,
                                    panGestureDismissal: false)
            vc.nameLabel.text = name
            vc.timeLabel.text = time
            vc.distanceLabel.text = distance
            vc.parkingIDModel = self.parkingIDModel
            // Present dialog
            present(popup, animated: true, completion: nil)
        }else {
            let vc = PopUpParkingDetailNoMotorcyecleViewController(nibName: "PopUpParkingDetailNoMotorcyecleViewController", bundle: nil)
            let popup = PopupDialog(viewController: vc,
                                    buttonAlignment: .horizontal,
                                    transitionStyle: .zoomIn,
                                    tapGestureDismissal: true,
                                    panGestureDismissal: false)
            vc.nameLabel.text = name
            vc.timeLabel.text = time
            vc.distanceLabel.text = distance
            present(popup, animated: true, completion: nil)
        }
    }
    
    func showDialogAlert(mesasage:String,buttontitle:String) {
        // Prepare the popup
        let title = ""
        let message = mesasage
        // Create the dialog
        let popup = PopupDialog(title: title,
                                message: message,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false,
                                hideStatusBar: false)
        // Create first button
        let buttonOne = DefaultButton(title: buttontitle) {
            //self.label.text = "You canceled the default dialog"
        }
        buttonOne.titleColor = UIColor.white
        buttonOne.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2823529412, blue: 0.3333333333, alpha: 1)
        // Add buttons to dialog
        popup.addButtons([buttonOne])
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func showDialogRejectReasonAlert(mesasage:String,buttoncanceltitle:String,buttonoktitle:String,data:JSON) {
        //var getRejectreasonData:JSON!
        var is_profilenotComplete:Bool = false
        var is_ImageDriverNotComplete:Bool = false
        var is_ImageSefieNotComplete:Bool = false
        for index in 0..<data.count {
            if let id = data[index]["id"].int {
                if id == 1 {
                    is_profilenotComplete = true
                }
                if id == 2 {
                    is_ImageDriverNotComplete = true
                }
                if id == 3 {
                    is_ImageSefieNotComplete = true
                }
            }
        }
        // Prepare the popup
        let title = ""
        let message = mesasage
        // Create the dialog
        let popup = PopupDialog(title: title,
                                message: message,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false,
                                hideStatusBar: false)
        // Create first button
        let buttonOne = DefaultButton(title: buttoncanceltitle) {
            //self.label.text = "You canceled the default dialog"
        }
        buttonOne.titleColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        buttonOne.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        let buttonTwo = DefaultButton(title: buttonoktitle) {
            if is_profilenotComplete || is_ImageDriverNotComplete || is_ImageSefieNotComplete {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ResubmitProfileViewController") as! ResubmitProfileViewController
                vc.is_profilenotComplete = is_profilenotComplete
                vc.is_ImageDriverNotComplete = is_ImageDriverNotComplete
                vc.is_ImageSefieNotComplete = is_ImageSefieNotComplete
                self.navigationController?.pushViewController(vc, animated:true)
            }
        }
        buttonTwo.titleColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonTwo.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.6862745098, blue: 0.9254901961, alpha: 1)
        
        // Add buttons to dialog
        popup.addButtons([buttonOne,buttonTwo])
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func showDialogActionPause(title:String,message:String,time:String,imagename:String,buttontitle:String) {
        //isTripActionPause = true
        // Create a custom view controller
        let vc = DialogTripActionViewController(nibName: "DialogTripActionViewController", bundle: nil)
        // Create the dialog
        let popup = PopupDialog(viewController: vc,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false)
         // Create button
         let buttonOne = DefaultButton(title: buttontitle, height: 50) {
            //self.isTripActionPause = false
            //self.tripActionResume()
         }
        buttonOne.titleColor = UIColor.white
        buttonOne.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.6862745098, blue: 0.9254901961, alpha: 1)
        
        vc.titleLabel.text = title
        vc.timeLabel.text = time
        vc.messageLabel.text = message
        vc.iconImageView.image = UIImage(named:imagename)
        
        // Add buttons to dialog
        popup.addButtons([buttonOne])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    func showDialogAction(title:String,message:String,imagename:String,buttontitle:String) {
        // Create a custom view controller
        let vc = DialogTripActionViewController(nibName: "DialogTripActionViewController", bundle: nil)
        // Create the dialog
        let popup = PopupDialog(viewController: vc,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false)
        // Create button
        let buttonOne = DefaultButton(title: buttontitle, height: 50) {
            //print("You canceled the rating dialog")
            //self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
        }
        buttonOne.titleColor = UIColor.white
        buttonOne.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2823529412, blue: 0.3333333333, alpha: 1)
        
        vc.titleLabel.text = title
        vc.timeLabel.text = ""
        vc.messageLabel.text = message
        vc.iconImageView.image = UIImage(named:imagename)
        
        // Add buttons to dialog
        popup.addButtons([buttonOne])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    func showDialogConfirmEndTrip(title:String,message:String,imagename:String) {
        // Create a custom view controller
        let vc = DialogTripActionViewController(nibName: "DialogTripActionViewController", bundle: nil)
        // Create the dialog
        let popup = PopupDialog(viewController: vc,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false)
        // Create button
        let buttonCancel = DefaultButton(title: "ยกเลิก", height: 50) {
            //print("You canceled the rating dialog")
        }
        let buttonOK = DefaultButton(title: "จบการใช้งาน", height: 50) {
            self.tripActionEnd()
        }
        buttonOK.titleColor = UIColor.white
        buttonOK.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.6862745098, blue: 0.9254901961, alpha: 1)
        
        vc.titleLabel.text = title
        vc.timeLabel.text = ""
        vc.messageLabel.text = message
        vc.iconImageView.image = UIImage(named:imagename)
        
        // Add buttons to dialog
        popup.addButtons([buttonCancel,buttonOK])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    //
    
    // MARK: Lopp home/trip ทุก 5 วิ
    
    func runTimerHomeTrip() {
        if isfinish_trip {
            
        }else {
            timer = Timer.scheduledTimer(timeInterval: 2, target: self,   selector: (#selector(MainMapViewController.callHomeTrip)), userInfo: nil, repeats: false)
        }
    }
    
    @objc func callHomeTrip() {
        timer.invalidate()
        getHomeTrip()
    }
    
    //
    
    func setUpDataPopUpMotorcycleOnTripAcctive(data:HometripModel) {
        
        if let registrationNumber = data.registration_number {
            registrationNumberMotocycle = registrationNumber
            registrationNumberLabel.text = "หมายเลขทะเบียนรถ: \(registrationNumber)"
        }
        if let battery = data.battery {
            batteryLevelLabel.text = "\(battery) %"
        }
        if let distance = data.distance {
            distanceLabel.text = "\(distance) กม."
        }
        if let time = data.time {
            let time_str = Utils.shared.convertTimeSecondToTimeString(time: time)
            timeLabel.text = "\(time_str) น."
        }
        if let amount = data.amount {
            amountLabel.text = "\(amount) บาท"
        }
        
        //set data in  Dialog pause Action
        //let pauase_duration = "PAUSEDURATION".localized();
        //let min = "MIN.".localized();
        if let time_pause = data.trip_action?.runtime {
            //let time_pause_str = Utils.shared.convertTimeSecondToTimeString(time: time_pause)
            //timeIsPauseLabel.text = "หยุดรถมาแล้ว : \(time_pause_str) นาที"
            //timeIsPauseLabel.text = pauase_duration + " \(time_pause_str) " + min
            if mockDialogTripPauseView.isHidden == false {
                if !isRuntimePauseAction {
                    currentTimePause = time_pause
                    isRuntimePauseAction = true
                    runtimePauseAction()
                }
            }
        }
        
        //set data in  Dialog ออกนอกพื้นที่
        if let time_countdown = data.trip_action?.countdown {
            //let time_countdown_str = Utils.shared.convertTimeSecondToTimeString(time: time_countdown)
            //timeIsCountdownLabel.text = "จะหยุกรถใน : \(time_countdown_str) นาที"
            //let willstop = "WILLSTOPIN".localized();
            //timeIsCountdownLabel.text = willstop + " \(time_countdown_str) " + min
            if mockDialogOutOfCoverageView.isHidden == true {
                if !isRuntimeCountDownAction {
                    currentTimeCountDown = time_countdown
                    isRuntimeCountDownAction = true
                    runtimeCountdownAction()
                }
            }
        }
    }
    
    func runtimePauseAction() {
        timer_Pause = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(MainMapViewController.calculateTimePause)), userInfo: nil, repeats: true)
    }
    
    @objc func calculateTimePause() {
        currentTimePause += 1
        let pauase_duration = "PAUSEDURATION".localized();
        let min = "MIN.".localized();
        let time_pause_str = Utils.shared.convertTimeSecondToTimeString(time: currentTimePause)
        timeIsPauseLabel.text = pauase_duration + " \(time_pause_str) " + min
        if mockDialogTripPauseView.isHidden == true {
            timer_Pause.invalidate()
            isRuntimePauseAction = false
            currentTimePause = 0
        }
    }
    
    func runtimeCountdownAction() {
        timer_countdown = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(MainMapViewController.calculateTimeCountdown)), userInfo: nil, repeats: true)
    }
    
    @objc func calculateTimeCountdown() {
        if currentTimeCountDown != 0 {
            currentTimeCountDown -= 1
        }
        let willstop = "WILLSTOPIN".localized();
        let min = "MIN.".localized();
        let time_countdown_str = Utils.shared.convertTimeSecondToTimeString(time: currentTimeCountDown)
        timeIsCountdownLabel.text = willstop + " \(time_countdown_str) " + min
        if mockDialogOutOfCoverageView.isHidden == true {
            timer_countdown.invalidate()
            isRuntimeCountDownAction = false
            currentTimeCountDown = 0
        }
    }
    
    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getUserStatus" {
            /*STAUS
            1=ใช้งานได้ปกติ
            2=รอการอนุมัติการใช้งาน
            3=ปฎิเสธการอนุมัติ
            4=ถูกยกเลิกการใช้งานถาวร*/
            if UserModel.Status == 1 {
                getBalance()
            }else if UserModel.Status == 2 {
                // เซทข้อความ รอ อนุมัติ
                statusButton.setTitle("เจ้าหน้าที่กำลังตรวจสอบข้อมูลการลงทะเบียน",for: .normal)
                statusButton.setTitleColor(.white, for: .normal)
                statusButton.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2823529412, blue: 0.3333333333, alpha: 1)
                statusButton.isHidden = false
                getHomeParking()
            }else if UserModel.Status == 3 {
                //ปฎิเสธการอนุมัติ
                statusButton.setTitle("การลงทะเบียนไม่สำเร็จกรุณาตรวจสอบข้อมมูลใหม่",for: .normal)
                statusButton.setTitleColor(.white, for: .normal)
                statusButton.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2823529412, blue: 0.3333333333, alpha: 1)
                statusButton.isHidden = false
                getHomeParking()
            }
            else {
                //status 4 return to login
                UserDefaults.standard.removeObject(forKey:"APITOKEN")
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: LoginViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }else if viewModel.apiType == "getBalance" {
            let scan_text = "SCANTORIDE".localized();
            scanButton.setTitle(scan_text,for: .normal)
            // เซทข้อความ รอ จำนวนเงิน
            if let total_money = viewModel.balanceData["balance"].double {
                UserModel.Can_start_trip = viewModel.balanceData["can_start_trip"].boolValue
                if total_money < 30 {
                    //let this_value = total_money.rounded(toPlaces: 2)
                    let this_value = String(format: "%.2f", total_money)
                    let balance_text1 = "YOURBALANC1".localized();
                    let balance_text2 = "YOURBALANC2".localized();
                    let bath_text = "BATH".localized();
                    //statusButton.setTitle("ยอดเงินคงเหลือ \(this_value) บาท ไม่เพียงพอต่อการเริ่มใช้งาน",for: .normal)
                    statusButton.setTitle("\(balance_text1) \(this_value) \(bath_text) \(balance_text2)",for: .normal)
                    statusButton.setTitleColor(.white, for: .normal)
                    statusButton.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2823529412, blue: 0.3333333333, alpha: 1)
                    statusButton.isHidden = false
                }else {
                    //let this_value = total_money.rounded(toPlaces: 2)
                    let this_value = String(format: "%.2f", total_money)
                    let balance_text1 = "YOURBALANC1".localized();
                    let bath_text = "BATH".localized();
                    //statusButton.setTitle("ยอดเงินคงเหลือ \(this_value) บาท",for: .normal)
                    statusButton.setTitle("\(balance_text1) \(this_value) \(bath_text)",for: .normal)
                    statusButton.setTitleColor(.black, for: .normal)
                    statusButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    statusButton.isHidden = false
                }
            }
            getHomeParking()
        }else if viewModel.apiType == "getHomeParking" {
            self.parkingModel = viewModel.parkingModel
            setMarkerParkingToMap(data: viewModel.parkingModel)
            getHomeParkingArea()
        }else if viewModel.apiType == "getHomeArea" {
            self.parkingAreaModel = viewModel.parkingAreaModel
            //set Polygons Area Map
            setUpPolygonsMap(data:self.parkingAreaModel)
            //setUpPolygonsMap(data:self.parkingAreaModel)
            
            /*STAUS
             1=ใช้งานได้ปกติ
             2=รอการอนุมัติการใช้งาน
             3=ถูกยกเลิกการใช้งานชั่วคราว
             4=ถูกยกเลิกการใช้งานถาวร*/
            if UserModel.Status == 1 {
                //ถ้าstatus == 1 ทำการเรียก home trip
                //call home trip
                getHomeTrip()
            }
        }else if viewModel.apiType == "getHomeParkingFindID" {
            self.parkingIDModel = viewModel.parkingIDModel
            let statin_fix = "STATION".localized();
            let min = "MIN.".localized();
            let meter = "MATER".localized();
            //let full_name = "สถานี " + parkingIDModel.name!
            let full_name = statin_fix + " " + parkingIDModel.name!
            //let time = "\(parkingIDModel.boarding_time ?? 0)   นาที"
            let time = "\(parkingIDModel.boarding_time ?? 0)   " + min
            var distance = "" 
            
            if let distance_ = parkingIDModel.distance {
                if distance_ > 999 {
                    //distance = ">=999  เมตร"
                    distance = ">=999  " + meter
                }else {
                    //distance = "\(parkingIDModel.distance ?? 0)  เมตร"
                    distance = "\(parkingIDModel.distance ?? 0)  " + meter
                }
            }else  {
                distance = "  " + meter
            }
            
            popUpParkingDeatail(name: full_name, time: time, distance: distance)
        }else if viewModel.apiType == "getHometrip" {
            isLodeHometrip  = true
            self.hometripModel = viewModel.hometripModel
            //set data popup motorcycle
            setUpDataPopUpMotorcycleOnTripAcctive(data: self.hometripModel)
            //เริ่มต้นเชคว่ามี trip ไหม ถ้าไม่มี api return {} มา,ทำให้ trip_id ใน HometripModel มีค่า --> trip_id = nil
            if self.hometripModel?.trip_id != nil {
                //has trip
                //เชคว่าทริบจบหรือยัง
                //check finish_trip is true
                if self.hometripModel.finish_trip! {
                    //is true
                    //ทริปจบแล้ว
                    //ซ่อนแถบข้อมูลรถ ปุ่ม pause trip, ปุ่ม end trip ต้องอยู่ใน ทริบถึงแสดง
                    mockTripOnView.isHidden = true
                    //แสดงปุ่ม scanButton , statusButton ,เวลาทริปจบแล้ว
                    scanButton.isHidden = false
                    statusButton.isHidden = false
                    //ทริปจบแล้ว ลบ marker รูป icon mortotctcle
                    if motorcycleMarker != nil {
                        motorcycleMarker.map = nil
                        motorcycleMarker = nil
                    }
                    isfinish_trip = true
                    //ซ่อน loding จาก "tripActionEnd"
                    LoadingActivityIndicator.sharedManager.hideActivityIndicator(uiView: view)
                    if self.hometripModel.show_bill! {
                        //if show_bill is true
                        //go to trip bill summary screen
                        idPageComeFrom = 2 //2= bill
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "MotorcycleUsageCompletedViewController") as! MotorcycleUsageCompletedViewController
                        vc.tripId = self.hometripModel.trip_id
                        self.navigationController?.pushViewController(vc, animated:true)
                    }else {
                        //end
                    }
                }else {
                    //set trip data
                    isfinish_trip = false
                    //แสดงแถบข้อมูลรถ ปุ่ม pause trip, ปุ่ม end trip ต้องอยู่ใน ทริบถึงแสดง
                    mockTripOnView.isHidden = false
                    //ซ่อนปุ่ม scanButton , statusButton ,เวลาอยู่ในทริป
                    scanButton.isHidden = true
                    statusButton.isHidden = true
                    
                    //add marker motorcycle
                    addMarkerMotorcycleToMap(data: self.hometripModel)
                    //update
                    updateMarkerMotorcycleToMap(data: self.hometripModel)
                    
                    if let trip_Action_code = self.hometripModel.trip_action?.code {
                        switch(trip_Action_code) {
                            
                        case 100...199:
                            mockDialogTripPauseView.isHidden = true
                            mockDialogOutOfCoverageView.isHidden = true
                            LoadingActivityIndicator.sharedManager.hideActivityIndicator(uiView: view)
                            break
                        case 200...299:
                            LoadingActivityIndicator.sharedManager.hideActivityIndicator(uiView: view)
                            mockDialogOutOfCoverageView.isHidden = true
                            //show dialog pause trip
                            if mockDialogTripPauseView.isHidden == true {
                                mockDialogTripPauseView.isHidden = false
                            }
                            break
                        case 300...399:
                            mockDialogTripPauseView.isHidden = true
                            mockDialogOutOfCoverageView.isHidden = true
                            LoadingActivityIndicator.sharedManager.hideActivityIndicator(uiView: view)
                            break
                        case 400...499:
                            LoadingActivityIndicator.sharedManager.hideActivityIndicator(uiView: view)
                            break
                        case 500...599:
                            //show dialog waiting
                            LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
                            break
                        case 600...699:
                            LoadingActivityIndicator.sharedManager.hideActivityIndicator(uiView: view)
                            //show dialog countdown
                            //ออกนอกพื้นที่
                            if mockDialogOutOfCoverageView.isHidden == true {
                                mockDialogOutOfCoverageView.isHidden = false
                            }
                            break
                        default :
                            break
                        }
                    }
                    
                    //เชค dialog แสดง dialog
                    if let dialog_id = self.hometripModel.dialog?.dialog_id {
                        //dialog_id != nil
                        switch(dialog_id) {
                        case 1:
                            //1=รถกำลังใกล้ออกจากเขตบริการ
                            let you_exit = "YOUAREGOINGTOEXIT".localized();
                            let message = "YOUMPTORCYCLEWILLBESTOPPED".localized();
                            let dissmiss = "DISMISS".localized();
                            //showDialogAction(title:"คุณกำลังใกล้ออกนอกพื้นที่ให้บริการ",message:"คำเตือน เมื่อรถออกนอกพื้นที่ให้บริการ ระบบจะหยุดรถอัตโนมัติ กรุณาติดต่อเจ้าหน้าที่",imagename:"icon_area_dialog",buttontitle:"รับทราบ")
                        showDialogAction(title:you_exit,message:message,imagename:"icon_area_dialog",buttontitle:dissmiss)
                            break
                        case 2:
                            //2=ยอดเงินของท่านกำลังจะหมด กรุณาเติดเงินเข้าระบบ
                            showDialogAction(title:"ยอดเงินใกล้จะหมด",message:"ยอดเงินของท่านกำลังจะหมด กรุณาเติมเงินเข้าระบบด้วย",imagename:"icon_amount_remaining_dialog",buttontitle:"รับทราบ")
                            break
                        default :
                            break
                        }
                    }
                }
            }else {
                //no trip
            }
            //call home trip 2 Second
            runTimerHomeTrip()
        }
        /*
        else if viewModel.apiType == "tripActionPause"{
            //show dialog pause
            //print(viewModel.actionPauseData["result"].boolValue)
            if let result = viewModel.actionPauseData["result"].bool {
                //แสดง dialog pause trip
                print(result)
                //รอให้ getHomeTrip() เป็นตัวจัดการ แสดง loading ตาม trip_Action_code
                //mockDialogTripPauseView.isHidden = false
                
                //แสดง loading รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน loading ตาม trip_Action_code
                LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
                
                //เรียก getHomeTrip เลย
                //timer.invalidate()
                //getHomeTrip()
            }else {
                if let message = viewModel.actionPauseData["message"].string {
                    AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: message, buttonTitle: "ปิด")
                }
            }
        }else if viewModel.apiType == "tripActionResume"{
            //close dialog pause
            if let result = viewModel.actionResumeData["result"].bool {
                //ซ่อน dialog pause trip
                print(result)
                //รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน  loading ตาม trip_Action_code
                //mockDialogTripPauseView.isHidden = true
                
                //แสดง loading รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน loading ตาม trip_Action_code
                LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
                
                //เรียก getHomeTrip เลย
                //timer.invalidate()
                //getHomeTrip()
            }else {
                if let message = viewModel.actionResumeData["message"].string {
                    AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: message, buttonTitle: "ปิด")
                }
            }
        }else if viewModel.apiType == "tripActionEnd"{
            //result is true
            if viewModel.actionEndData["result"].boolValue {
                //ให้ getHomeTrip() เป็นตัวเชคว่าให้ไปหน้า bill จะได้ไม่ทำงานซ้ำซ้อน
                
                //แสดง loading รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน loading ตาม if self.hometripModel.show_bill = true
                LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
                
                //เรียก getHomeTrip เลย เพื่อเชคว่าจบการทำงานหรือยัง
                //timer.invalidate()
                //getHomeTrip()
                
                /*
                //ทริปจบแล้ว ลบ marker รูป icon mortotctcle
                if motorcycleMarker != nil {
                    motorcycleMarker.map = nil
                    motorcycleMarker = nil
                }
                
                //goto bill
                idPageComeFrom = 2 //2= bill
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MotorcycleUsageCompletedViewController") as! MotorcycleUsageCompletedViewController
                vc.tripId = self.hometripModel.trip_id
                self.navigationController?.pushViewController(vc, animated:true)
                */
            }else {
                //show dialog please park in the area
                showDialogAction(title:"กรุณาคืนรถในบริเวณจุดจอดที่กำหนดไว้เท่านั้น",message:"หากไม่จอดรถในบริเวณที่กำหนด จะไม่สามารถจบการใช้งานได้",imagename:"icon_park_dialog",buttontitle:"รับทราบ")
            }
        }*/
        else if viewModel.apiType == "getRejectreason" {
            let data:JSON = viewModel.getRejectreasonData
            var message = "มีการตรวจสอบข้อมูลการลงทะเบียนแล้ว พบข้อมูลบ้างส่วนไม่ถูกต้อง กรุณาแก้ไขข้อมูลต่อไปนี้\n"
            let fix_title = "• "
            for index in 0..<data.count {
                message += fix_title
                message += data[index]["name"].stringValue
                message += "\n"
           }
          showDialogRejectReasonAlert(mesasage:message,buttoncanceltitle:"รับทราบ",buttonoktitle:"แก้ไขข้อมูล",data:data)
        }
        else {
            
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        if viewModel.apiType == "getHometrip" {
            //call home trip 2 Second
            runTimerHomeTrip()
            AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
        }
        /*
        else if viewModel.apiType == "tripActionEnd" {
            //http error 400 กรณีจอดรถไม่ตรงสถานี
            //show dialog please park in the area
            showDialogAction(title:"กรุณาคืนรถในบริเวณจุดจอดที่กำหนดไว้เท่านั้น",message:"หากไม่จอดรถในบริเวณที่กำหนด จะไม่สามารถจบการใช้งานได้",imagename:"icon_park_dialog",buttontitle:"รับทราบ")
            /*
            if errorMessage == "หากไม่จอดรถในบริเวณที่กำหนด จะไม่สามารถจบการใช้งานได้" {
                //http error 400 กรณีจอดรถไม่ตรงสถานี
                //show dialog please park in the area
                showDialogAction(title:"กรุณาคืนรถในบริเวณจุดจอดที่กำหนดไว้เท่านั้น",message:"หากไม่จอดรถในบริเวณที่กำหนด จะไม่สามารถจบการใช้งานได้",imagename:"icon_park_dialog",buttontitle:"รับทราบ")
            }else {
                AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
            }*/
        }
        */
        else {
            AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
        }
    }
    
    // Load data trip action
    @objc override func onDataTripActionDidLoad() {
        if viewModel.apiType == "tripActionPause"{
            //show dialog pause
            //print(viewModel.actionPauseData["result"].boolValue)
            if let result = viewModel.actionPauseData["result"].bool {
                //แสดง dialog pause trip
                print(result)
                //รอให้ getHomeTrip() เป็นตัวจัดการ แสดง loading ตาม trip_Action_code
                //mockDialogTripPauseView.isHidden = false
                
                //แสดง loading รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน loading ตาม trip_Action_code
                LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
            }else {
                if let message = viewModel.actionPauseData["message"].string {
                    AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: message, buttonTitle: "ปิด")
                }
            }
        }else if viewModel.apiType == "tripActionResume"{
            //close dialog pause
            if let result = viewModel.actionResumeData["result"].bool {
                //ซ่อน dialog pause trip
                print(result)
                //รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน  loading ตาม trip_Action_code
                //mockDialogTripPauseView.isHidden = true
                
                //แสดง loading รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน loading ตาม trip_Action_code
                LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
            }else {
                if let message = viewModel.actionResumeData["message"].string {
                    AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: message, buttonTitle: "ปิด")
                }
            }
        }else if viewModel.apiType == "tripActionEnd"{
            //result is true
            if viewModel.actionEndData["result"].boolValue {
                //ให้ getHomeTrip() เป็นตัวเชคว่าให้ไปหน้า bill จะได้ไม่ทำงานซ้ำซ้อน
                
                //แสดง loading รอให้ getHomeTrip() เป็นตัวจัดการ ซ่อน loading ตาม if self.hometripModel.show_bill = true
                LoadingActivityIndicator.sharedManager.showActivityIndicator(uiView: view)
            }else {
                //show dialog please park in the area
                let return_motocye = "RETURNINLOCATIONONLY".localized();
                let noparkarea = "IFMOTOCYCLENOTPARKED".localized();
                let dissmiss = "DISMISS".localized();
                //showDialogAction(title:"กรุณาคืนรถในบริเวณจุดจอดที่กำหนดไว้เท่านั้น",message:"หากไม่จอดรถในบริเวณที่กำหนด จะไม่สามารถจบการใช้งานได้",imagename:"icon_park_dialog",buttontitle:"รับทราบ")
            showDialogAction(title:return_motocye,message:noparkarea,imagename:"icon_park_dialog",buttontitle:dissmiss)
            }
        }else {
            
        }
    }
    
    @objc override func onDataTripActionDidLoadErrorWithMessage(errorMessage:String) {
        if viewModel.apiType == "tripActionEnd" {
            //http error 400 กรณีจอดรถไม่ตรงสถานี
            //show dialog please park in the area
            //showDialogAction(title:"กรุณาคืนรถในบริเวณจุดจอดที่กำหนดไว้เท่านั้น",message:"หากไม่จอดรถในบริเวณที่กำหนด จะไม่สามารถจบการใช้งานได้",imagename:"icon_park_dialog",buttontitle:"รับทราบ")
            let return_motocye = "RETURNINLOCATIONONLY".localized();
            let noparkarea = "IFMOTOCYCLENOTPARKED".localized();
            let dissmiss = "DISMISS".localized();
        showDialogAction(title:return_motocye,message:noparkarea,imagename:"icon_park_dialog",buttontitle:dissmiss)
        }else {
            AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
        }
    }

}

// extension for GMSMapViewDelegate
extension MainMapViewController: GMSMapViewDelegate {
    
    // tap map marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //print("didTap marker \(marker.title!)")
        
        if marker != motorcycleMarker {
            let index:Int = Int(marker.accessibilityLabel!)!
            let parking_id = self.parkingModel.parkingList?[index].parking_id
            
            let lat:Double = self.userlocation_latitude
            let long:Double = self.userlocation_longitude
            getHomeParkingFindID(id:parking_id!,device_lat:lat,device_lng:long)
        }
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let zoom = mapView.camera.zoom
        current_zoom = zoom
        //print("map zoom is ",String(zoom))
        
        //isMoveTheMap = true
        
        /*
        if zoom >= 15 {
            if self.parkingAreaModel != nil && !isCreateAreaPolygons {
                isCreateAreaPolygons = true
                setUpPolygonsMap(data:self.parkingAreaModel)
            }
        }else {
            isCreateAreaPolygons = false
            if mainfillingPolygon != nil {
                mainfillingPolygon.map = nil
                if mainGMSline != nil {
                    mainGMSline.map = nil
                }
            }
        }*/
    }
}


// MARK: - CLLocationManagerDelegate
    //1
extension MainMapViewController: CLLocationManagerDelegate {
    //2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //3
        guard status == .authorizedWhenInUse else {
            return
        }
        //4
        locationManager.startUpdatingLocation()
        
        //5
        mapView.isMyLocationEnabled = true
        //mapView.settings.myLocationButton = true
    }
    
    //6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        let userlocation: CLLocation = locations.last!
        self.userlocation_latitude = userlocation.coordinate.latitude
        self.userlocation_longitude = userlocation.coordinate.longitude
        
        //disable camera user location center
        //7
        //mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: current_zoom, bearing: 0, viewingAngle: 0)
        
        //8
        locationManager.stopUpdatingLocation()
    }
}
