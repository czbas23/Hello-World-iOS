//
//  MotorcycleUsageCompletedViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 30/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire

class MotorcycleUsageCompletedViewController: BaseViewController {
    
    @IBOutlet var completeButton: UIButton!
    var viewModel:MainMapViewModel!
    var tripBillModel:TripBillModel!
    
    @IBOutlet var refcodeLabel: UILabel!
    @IBOutlet var registrationNumberLabel: UILabel!
    @IBOutlet var tripDateLabel: UILabel!
    @IBOutlet var triptimeStartLabel: UILabel!
    @IBOutlet var triptimeFinishLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var operatingTimeLabel: UILabel!
    @IBOutlet var totalAmountLabel: UILabel!
    @IBOutlet var pauseTimeLabel: UILabel!
    @IBOutlet var walletBalanceLabel: UILabel!
    
    @IBOutlet var tripsummaryLabel: UILabel!
    @IBOutlet var tripNoLabel: UILabel!
    @IBOutlet var licentplateLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var startTimeLabel: UILabel!
    @IBOutlet var stopTimeLabel: UILabel!
    @IBOutlet var totalUsageLabel: UILabel!
    @IBOutlet var drivingdistanceLabel: UILabel!
    @IBOutlet var pauseDulationLabel: UILabel!
    @IBOutlet var totalServiceChangeLabel: UILabel!
    @IBOutlet var detailissueLabel: UILabel!
    @IBOutlet var rateLabel: UILabel!
    
    @IBOutlet var star1ImageView: UIImageView!
    @IBOutlet var star2ImageView: UIImageView!
    @IBOutlet var star3ImageView: UIImageView!
    @IBOutlet var star4ImageView: UIImageView!
    @IBOutlet var star5ImageView: UIImageView!
    
    @IBOutlet var recommendView: UITextView!
    
    var starRating:Int = 5
    
    var tripId:Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MainMapViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        getTripBill()
        setLocalizableText()
    }
    
    // MARK: IBAction
    
    @IBAction func completeButtonPress(_ sender: UIButton) {
        /*
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MainMapViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }*/
        sendRecomment(comment:recommendView.text)
    }
    
    @IBAction func ratingButtonPress(_ sender: UIButton) {
        let index = sender.tag
        setUpStar(starIndex:index)
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "PAYMENT".localized();
        tripsummaryLabel.text = "\("TRIPSUMMARY".localized())"
        tripNoLabel.text = "\("TRIPNO".localized()) :"
        licentplateLabel.text = "\("LICENSEPLATE".localized()) :"
        dateLabel.text = "\("DATE".localized()) :"
        startTimeLabel.text = "\("STARTTIME".localized()) :"
        stopTimeLabel.text = "\("STOPTIME".localized()) :"
        totalUsageLabel.text = "\("TOTALUSAGETIME".localized()) :"
        
        drivingdistanceLabel.text = "\("DRIVINGDISTANCE".localized())"
        pauseDulationLabel.text = "\("PAUSEDURATION".localized())"
        totalServiceChangeLabel.text = "\("TOTALSERVICECHANGE".localized())"
        detailissueLabel.text = "ISSUEDETAIL".localized()
        rateLabel.text = "RATEAPP".localized()
        completeButton.setTitle("DISMISS".localized(),for: .normal)
    }
    

    func setupUI() {
        completeButton.layer.cornerRadius = 8
        recommendView.layer.cornerRadius = 8
        recommendView.layer.borderWidth = 1
        recommendView.layer.borderColor = UIColor.lightGray.cgColor
        
        //default star = 5 ,index = 4
        setUpStar(starIndex:4)
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func getTripBill() {
        let token = UserModel.ApiToken
        let trip_id:Int = self.tripId
        viewModel.getTripBill(api: "/trip/bill/\(trip_id)", parameters: nil,token:token!)
    }
    
    func sendRecomment(comment:String) {
        let token = UserModel.ApiToken
        let trip_id:Int = self.tripId
        let star_str:String = "\(starRating)"
        let parameters: Parameters = [
            "star": star_str,
            "comment": comment
        ]
        viewModel.tripSatisfaction(api: "/trip/satisfaction/\(trip_id)", parameters: parameters,token:token!)
    }
    
    func setUpData(data:TripBillModel) {
        let bath_text = "BATH".localized();
        let km_text = "KM.".localized();
        let min_text = "MIN.".localized();
        
        refcodeLabel.text = data.ref_code
        registrationNumberLabel.text = data.registration_number
        tripDateLabel.text = data.trip_date
        triptimeStartLabel.text = data.trip_time_start! //+ min_text
        triptimeFinishLabel.text = data.trip_time_finish! //+ min_text
        if let distance = data.distance {
            //set ทศนิยม 2 ตำแหน่ง
            //let this_value = distance.rounded(toPlaces: 2)
            let this_value = String(format: "%.2f", distance)
            distanceLabel.text = "\(this_value) \(km_text)"
            
            //distanceLabel.text = "\(distance) กม."
        }
        if let operatingTime = data.operating_time {
            let time_str = Utils.shared.convertTimeSecondToTimeString(time: operatingTime)
            operatingTimeLabel.text = "\(time_str)\(min_text)"
        }
        if let totalAmount = data.total_amount {
            totalAmountLabel.text = "\(totalAmount) \(bath_text)"
        }
        if let walletBalance = data.wallet_balance {
            //walletBalanceLabel.text = "\(walletBalance) บาท"
        }
        if let pauseTime = data.pause_time {
            let time_str = Utils.shared.convertTimeSecondToTimeString(time: pauseTime)
            pauseTimeLabel.text = "\(time_str)\(min_text)"
        }
    }
    
    func setUpStar(starIndex:Int) {
        //5 star
        for index in 0 ..< 5 {
            if index <= starIndex {
                if index == 0 {
                    star1ImageView.image = UIImage(named: "icon_star_rating_active")
                    starRating = 1
                }else if index == 1 {
                    star2ImageView.image = UIImage(named: "icon_star_rating_active")
                    starRating = 2
                }else if index == 2 {
                    star3ImageView.image = UIImage(named: "icon_star_rating_active")
                    starRating = 3
                }else if index == 3 {
                    star4ImageView.image = UIImage(named: "icon_star_rating_active")
                    starRating = 4
                }else if index == 4 {
                    star5ImageView.image = UIImage(named: "icon_star_rating_active")
                    starRating = 5
                }
            }else {
                if index == 0 {
                    star1ImageView.image = UIImage(named: "icon_star_rating_unactive")
                }else if index == 1 {
                    star2ImageView.image = UIImage(named: "icon_star_rating_unactive")
                }else if index == 2 {
                    star3ImageView.image = UIImage(named: "icon_star_rating_unactive")
                }else if index == 3 {
                    star4ImageView.image = UIImage(named: "icon_star_rating_unactive")
                }else if index == 4 {
                    star5ImageView.image = UIImage(named: "icon_star_rating_unactive")
                }
            }
        }
    }
    
    @objc override func onDataDidLoad() {
        if viewModel.apiType == "tripSatisfaction" {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MainMapViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }else {
            self.tripBillModel = viewModel.tripBillModel
            setUpData(data:self.tripBillModel)
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }

}
