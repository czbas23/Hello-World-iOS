//
//  PopUpParkingDetailViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 8/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class PopUpParkingDetailViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var parkingIDModel:ParkingIDModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpTableView()
    }


    func setUpTableView() {
        tableView.register(UINib(nibName: PopupMotorcycleTableViewCell.identifier, bundle: Bundle.main),
                           forCellReuseIdentifier: PopupMotorcycleTableViewCell.identifier)
        //tableView.reloadData()
    }

}

extension PopUpParkingDetailViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (parkingIDModel.motorcyclesList?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PopupMotorcycleTableViewCell.identifier, for: indexPath) as? PopupMotorcycleTableViewCell else {
            fatalError("Wrong Cell")
        }
        let registration_number = parkingIDModel.motorcyclesList?[indexPath.row].registration_number
        let battery_distance = parkingIDModel.motorcyclesList?[indexPath.row].battery_distance
        cell.setCell(with: registration_number!, battery: battery_distance!)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
