//
//  NotificationListViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel:NotificationViewModel!
    var isLodeData:Bool = false
    var notificationModel:NotificationModel!
    var FindNotificationHistoryData:JSON!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = NotificationViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupTableView()
        setupUI()
        getNotificationHistory()
        setLocalizableText()
    }

    func setupTableView() {
        tableView.register(UINib(nibName: NotificationListItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: NotificationListItemTableViewCell.identifier)
        
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "NOTIFICATION".localized();
    }
    
    func setupUI() {
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func getNotificationHistory() {
        let token = UserModel.ApiToken
        viewModel.getNotificationHistory(api: "/notification/history", parameters: nil, token: token!)
    }
    
    func getFindNotificationHistory(notificationId:String) {
        let token = UserModel.ApiToken
        let url_api = "/notification/history"
        let url_data = "/\(notificationId)"
        viewModel.getFindNotificationHistory(api: url_api + url_data, parameters: nil, token: token!)
    }
    
    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getNotificationHistory" {
            isLodeData = true
            self.notificationModel = viewModel.notificationModel
            tableView.reloadData()
        }else if viewModel.apiType == "getFindNotificationHistory" {
            self.FindNotificationHistoryData = viewModel.FindNotificationHistoryData
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationDetailViewController") as! NotificationDetailViewController
            vc.FindNotificationHistoryData = self.FindNotificationHistoryData
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }

}


extension NotificationListViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLodeData {
            return (self.notificationModel.notificationHistoryList?.count)!
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotificationListItemTableViewCell.identifier, for: indexPath) as? NotificationListItemTableViewCell else {
            fatalError("Wrong Cell")
        }
        cell.setCell(data: (self.notificationModel.notificationHistoryList?[indexPath.row])!)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let notiId = self.notificationModel.notificationHistoryList?[indexPath.row].notification_id {
            getFindNotificationHistory(notificationId:notiId)
        }
        
    }
    
}
