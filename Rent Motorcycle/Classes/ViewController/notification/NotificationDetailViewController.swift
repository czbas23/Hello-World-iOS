//
//  NotificationDetailViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 29/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationDetailViewController: UIViewController {
    
    @IBOutlet weak var mockCircleView: UIView!
    
    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet var mockView: UIView!
    
    var FindNotificationHistoryData:JSON!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    

    // MARK: IBAction
    
    @IBAction func reportButtonPress(_ sender: UIButton) {
        //_ = navigationController?.popViewController(animated: true)
    }

    
    func setupUI() {
        self.navigationItem.title = "การแจ้งเตือน"
        //mockCircleView.layer.cornerRadius = mockCircleView.frame.width / 2
        self.topicLabel.text = FindNotificationHistoryData["title"].stringValue
        self.descLabel.text = FindNotificationHistoryData["body"].stringValue
        
        let date_timestamp = FindNotificationHistoryData["date"].intValue
        let date = Utils.shared.convertTimestampToDateString(timestamp: date_timestamp)
        self.dateLabel.text = date
        
        mockView.layer.masksToBounds = false
        mockView.layer.shadowColor = UIColor.black.cgColor
        mockView.layer.shadowOpacity = 0.5
        mockView.layer.shadowOffset = CGSize(width: -1, height: 1)
        mockView.layer.shadowRadius = 4
    }
    
}
