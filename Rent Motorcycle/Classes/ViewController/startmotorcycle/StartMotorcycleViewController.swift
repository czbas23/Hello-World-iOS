//
//  StartMotorcycleViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 24/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StartMotorcycleViewController: BaseViewController ,UIScrollViewDelegate{
    
    @IBOutlet var issueButton: UIButton!
    @IBOutlet var startButton: UIButton!
    
    @IBOutlet var registrationNumberLabel: UILabel!
    @IBOutlet var batteryLevelLabel: UILabel!
    @IBOutlet var batteryDistanceLabel: UILabel!
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var mockupView: UIView!
    
    @IBOutlet var registrationNumberTitleLabel: UILabel!
    @IBOutlet var batteryLevelTitleLabel: UILabel!
    @IBOutlet var batteryDistanceTitleLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    
    var registration_number:String!
    var battery_level:Int!
    var battery_distance:Double!
    
    var qrcode:String!
    
    var viewModel:MainMapViewModel!
    var motorcycleModel:MotorcycleModel!
    
    var userlocation_latitude:Double!
    var userlocation_longitude:Double!
    
    var motorcycleID:String!
    
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    @IBOutlet weak var pageControl: UIPageControl!
    var slides:[SlidePopUp] = [];

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MainMapViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        setupScrollView()
        getQRCodeMotorcycle()
        setLocalizableText()
    }
    
    // MARK: IBAction
    
    @IBAction func startTripButtonPress(_ sender: UIButton) {
        if let id = self.motorcycleModel?.motorcycle_id {
            tripActionStart(motorcycle_id:id)
        }
    }
    
    @IBAction func issueButtonPress(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ReportProblemViewController") as! ReportProblemViewController
        //vc.motorcycleID = self.motorcycleID
        vc.registrationNumber = registrationNumberLabel.text
        self.navigationController?.pushViewController(vc, animated:true)
    }

    func setLocalizableText() {
        registrationNumberTitleLabel.text = "LICENCEPLATENO".localized();
        batteryLevelTitleLabel.text = "\("BATERRYLEVEL".localized()) : "
        batteryDistanceTitleLabel.text = "\("REMAININGDISTANCE".localized()) : "
        detailLabel.text = "USAGEDETAIL".localized();
        issueButton.setTitle("REPORTPROBLEMUSAGE".localized(),for: .normal)
        startButton.setTitle("START".localized(),for: .normal)
    }
    
    func setupUI() {
        issueButton.layer.cornerRadius = 8
        startButton.layer.cornerRadius = 8
        
        mockupView.layer.cornerRadius = 8
        mockupView.layer.borderWidth = 2
        mockupView.layer.borderColor = UIColor.orange.cgColor
        
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setupScrollView() {
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    func getQRCodeMotorcycle() {
        let token = UserModel.ApiToken
        viewModel.getQRCodeMotorcycle(api: "/home/qrcode/"+self.qrcode, parameters: nil,token:token!)
    }
    
    func tripActionStart(motorcycle_id:Int) {
        
        let token = UserModel.ApiToken
        let device_parameters:Parameters = [
            "lat": userlocation_latitude,
            "lng": userlocation_longitude

        ]
        let parameters: Parameters = [
            "motorcycle_id": motorcycle_id,
            "device_location": device_parameters
        ]
        viewModel.tripActionStart(api: "/trip/action/start", parameters: parameters, token: token!)
    }

    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getQRCodeMotorcycle" {
            self.motorcycleModel = viewModel.motorcycleModel
            if let level = self.motorcycleModel?.battery_level {
                batteryLevelLabel.text = "\(level) %"
            }
            if let distance = self.motorcycleModel?.battery_distance {
                let km_text = "KM.".localized();
                batteryDistanceLabel.text = "\(distance) \(km_text)"
            }
            registrationNumberLabel.text = self.motorcycleModel?.registration_number ?? ""
            if let id = self.motorcycleModel?.motorcycle_id {
                motorcycleID = String(id)
            }
        }else if viewModel.apiType == "tripActionStart" {
            //start
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MainMapViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }else {
            
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
    
    
    // MARK: UIScrollView
    
    func createSlides() -> [SlidePopUp] {
        
        let slide1:SlidePopUp = Bundle.main.loadNibNamed("SlidePopUp", owner: self, options: nil)?.first as! SlidePopUp
        slide1.imageView.image = UIImage(named: "pop_up_01")
        //slide1.labelTitle.text = "A real-life bear"
        //slide1.labelDesc.text = "ค้นหาตำแหน่งของรถจักรยานยนต์ที่พร้อมให้บริการ ได้จากแผนที่บนแอปพลิเคชัน"
        
        let slide2:SlidePopUp = Bundle.main.loadNibNamed("SlidePopUp", owner: self, options: nil)?.first as! SlidePopUp
        slide2.imageView.image = UIImage(named: "pop_up_02")
        //slide3.labelTitle.text = "A real-life bear"
        //slide2.labelDesc.text = "แสกน QR code เพื่อเริ่มขับขี่กันได้เลย"
        
        let slide3:SlidePopUp = Bundle.main.loadNibNamed("SlidePopUp", owner: self, options: nil)?.first as! SlidePopUp
        slide3.imageView.image = UIImage(named: "pop_up_03")
        //slide2.labelTitle.text = "A real-life bear"
        //slide3.labelDesc.text = "จบการใช้งานได้เอง โดยการคืนรถที่จุดจอดที่กำหนดไว้"
        
        return [slide1, slide2, slide3]
    }
    
    
    func setupSlideScrollView(slides : [SlidePopUp]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: 300)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    
    /*
     * default function called when view is scolled. In order to enable callback
     * when scrollview is scrolled, the below code needs to be called:
     * slideScrollView.delegate = self or
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
    
    func scrollView(_ scrollView: UIScrollView, didScrollToPercentageOffset percentageHorizontalOffset: CGFloat) {
        if(pageControl.currentPage == 0) {
            //Change background color to toRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1
            //Change pageControl selected color to toRed: 103/255, toGreen: 58/255, toBlue: 183/255, fromAlpha: 0.2
            //Change pageControl unselected color to toRed: 255/255, toGreen: 255/255, toBlue: 255/255, fromAlpha: 1
            
            let pageUnselectedColor: UIColor = fade(fromRed: 255/255, fromGreen: 255/255, fromBlue: 255/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
            pageControl.pageIndicatorTintColor = pageUnselectedColor
            
            
            let bgColor: UIColor = fade(fromRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1, toRed: 255/255, toGreen: 255/255, toBlue: 255/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
            slides[pageControl.currentPage].backgroundColor = bgColor
            
            let pageSelectedColor: UIColor = fade(fromRed: 81/255, fromGreen: 36/255, fromBlue: 152/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
            pageControl.currentPageIndicatorTintColor = pageSelectedColor
        }
    }
    
    func fade(fromRed: CGFloat,
              fromGreen: CGFloat,
              fromBlue: CGFloat,
              fromAlpha: CGFloat,
              toRed: CGFloat,
              toGreen: CGFloat,
              toBlue: CGFloat,
              toAlpha: CGFloat,
              withPercentage percentage: CGFloat) -> UIColor {
        
        let red: CGFloat = (toRed - fromRed) * percentage + fromRed
        let green: CGFloat = (toGreen - fromGreen) * percentage + fromGreen
        let blue: CGFloat = (toBlue - fromBlue) * percentage + fromBlue
        let alpha: CGFloat = (toAlpha - fromAlpha) * percentage + fromAlpha
        
        // return the fade colour
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}
