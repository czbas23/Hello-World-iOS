//
//  LanguageViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Localize_Swift

class LanguageViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var menutitleDataArr = ["ภาษาไทย","English"]
    var menuimageDataArr = ["icon_select.png","icon_unselect.png"]
    var localDataArr = ["th","en"]
    
    var viewModel:LocaleSettingViewModel!
    
    var local_language:String!
    var isLodeData:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = LocaleSettingViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupTableView()
        setupUI()
        getLocale()
        setLocalizableText()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: LanguageItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: LanguageItemTableViewCell.identifier)
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "LANGUAGE".localized();
    }
    
    func setupUI() {
        self.navigationItem.title = "ภาษา"
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func getLocale() {
        let token = UserModel.ApiToken
        viewModel.getLocale(api: "/setting/locale", parameters: nil, token: token!)
    }
    
    func updateLocale() {
        let token = UserModel.ApiToken
        let parameters: Parameters = [
            "locale": local_language
        ]
        viewModel.updateLocale(api: "/setting/locale", parameters: parameters, token: token!)
    }
    
    @objc func selectLocal(sender: UIButton) {
        let tag = sender.tag
        local_language = localDataArr[tag]
        updateLocale()
    }
    
    func setUPImageSelect(local:String) {
        if local == "th" {
            menuimageDataArr.removeAll()
            menuimageDataArr.append("icon_select.png")
            menuimageDataArr.append("icon_unselect.png")
        }else {
            menuimageDataArr.removeAll()
            menuimageDataArr.append("icon_unselect.png")
            menuimageDataArr.append("icon_select.png")
        }
    }

    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getLocale" {
            isLodeData = true
            if let locale = viewModel.localeData["locale"].string {
                local_language = locale
            }
            //Localize.setCurrentLanguage()
            Localize.setCurrentLanguage(local_language)
            setUPImageSelect(local: local_language)
            tableView.reloadData()
        }else if viewModel.apiType == "updateLocale" {
            //Localize.setCurrentLanguage()
            Localize.setCurrentLanguage(local_language)
            //save local
            UserDefaults.standard.set(local_language, forKey:"LOCAL_LANGUAGE")
            setUPImageSelect(local: local_language)
            tableView.reloadData()
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
}

extension LanguageViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLodeData {
            return 2
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LanguageItemTableViewCell.identifier, for: indexPath) as? LanguageItemTableViewCell else {
            fatalError("Wrong Cell")
        }
        cell.selectLocalButton.tag = indexPath.row
        cell.selectLocalButton.addTarget(self, action: #selector(LanguageViewController.selectLocal(sender:)), for: .touchUpInside)
        cell.setCell(imagename: menuimageDataArr[indexPath.row], title: menutitleDataArr[indexPath.row])
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
