//
//  SettingViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
   // var menutitleDataArr = ["ข้อมูลส่วนตัว","วิธีใช้งาน","ภาษา","การแจ้งเตือน","เกี่ยวกับเรา"]
    var menutitleDataArr = ["PERSONALINFOMATION".localized(),"HOWTOUSE".localized(),"LANGUAGE".localized(),"NOTIFICATION".localized(),"ABOUTUS".localized()]
    
    var menuimageDataArr = ["icon_user_setting","icon_motorcycle","icon_language_setting","icon_notification_setting","icon_about_setting"]
    
    @IBOutlet var logOutButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTableView()
        setupUI()
        setLocalizableText()
    }
    
    // MARK: IBAction
    
    @IBAction func logoutButtonPress(_ sender: UIButton) {
        UserDefaults.standard.removeObject(forKey:"APITOKEN")
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: SettingItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: SettingItemTableViewCell.identifier)
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "SETTINGS".localized();
    }

    func setupUI() {
        logOutButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
}

extension SettingViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingItemTableViewCell.identifier, for: indexPath) as? SettingItemTableViewCell else {
            fatalError("Wrong Cell")
        }
        cell.setCell(imagename: menuimageDataArr[indexPath.row], title: menutitleDataArr[indexPath.row])
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SettingHowtoViewController") as! SettingHowtoViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 2) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LanguageViewController") as! LanguageViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 3) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 4) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
