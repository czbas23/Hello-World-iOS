//
//  AboutViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet var detailLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setLocalizableText()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "ABOUTUS".localized();
    }
    
    func setupUI() {
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
        
        detailLabel.text = "ABOUTUSDETAIL".localized(); //"ไทยคม ผู้นำธุรกิจดาวเทียมแห่งเอเชีย และผู้ให้บริการด้านสื่อสารผ่านดาวเทียมครบวงจร\n\nสถานีดาวเทียมไทยคม บริษัท ไทยคม จำกัด (มหาชน) สถานีดาวเทียมไทยคม 63/21 ถ.รัตนาธิเบศร์ ต.บางกระสอ อ.เมือง จ.นนทบุรี 11000 (ประเทศไทย) +66 2-596-5060\n\n ไทยคมซิตี้เซ็นเตอร์ 349 ชั้น 28 อาคารเอสเจ อินฟินิทวัน บิสสิเนส คอมเพล็กซ์ ถนนวิภาวดีรังสิต แขวงจอมพล เขตจตุจักร กรุงเทพฯ 10900 +66 2-596-5095 ต่อ 6900\n\n สถานีบริการภาคพื้นดินไทยคม 50 หมู่ 1 ซอยวัดเจดีย์หอย ต.บ่อเงิน อ.ลาดหลุมเเก้ว จ.ปทุมธานี +66 2-593-3000, +66 2-593-3001"
    }

}
