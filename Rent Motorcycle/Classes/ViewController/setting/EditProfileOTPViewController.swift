//
//  EditProfileOTPViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 23/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditProfileOTPViewController: BaseViewController {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var otp1TextField: UITextField!
    @IBOutlet var otp2TextField: UITextField!
    @IBOutlet var otp3TextField: UITextField!
    @IBOutlet var otp4TextField: UITextField!
    @IBOutlet var otp5TextField: UITextField!
    @IBOutlet var otp6TextField: UITextField!
    
    @IBOutlet var detailLabel: UILabel!
    
    var maxLen:Int = 1;
    
    var viewModel:ProfileViewModel!
    
    var email:String!
    var phoneNumber:Int!
    var callingCode:Int = 66
    var refCode:String!
    
    var seconds = 180
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProfileViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    // MARK: IBAction
    
    @IBAction func updateProfileButtonPress(_ sender: UIButton) {
        timer.invalidate()
        updateProfile()
    }
    
    @IBAction func requestOtpButtonPress(_ sender: UIButton) {
        timer.invalidate()
        requestOtp()
    }
    
    func setupUI() {
        
        //set up ui multi color detail Label
        let expire = timeString(time: TimeInterval(seconds))
        let phoneNumber_str = String(self.phoneNumber)
        let cut_phoneNumber = phoneNumber_str.substring(from: 5)
        let stringValue = "กรุณาป้อน OTP 6 หลักที่ส่งถึง (+66) xx xxx" + cut_phoneNumber + " รหัสอ้างอิง " + self.refCode + " และจะหมดอายุภายใน " + expire + " นาที"
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        attributedString.setColorForText(textForAttribute: "กรุณาป้อน OTP 6 หลักที่ส่งถึง ", withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: "(+66) xx xxx" + cut_phoneNumber, withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: " รหัสอ้างอิง ", withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: self.refCode , withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: " และจะหมดอายุภายใน ", withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: expire , withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: " นาที", withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        detailLabel.attributedText = attributedString
        //
        
        runTimer()
        
        otp1TextField.layer.borderWidth = 1
        otp1TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp1TextField.layer.cornerRadius = 8
        
        otp2TextField.layer.borderWidth = 1
        otp2TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp2TextField.layer.cornerRadius = 8
        
        otp3TextField.layer.borderWidth = 1
        otp3TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp3TextField.layer.cornerRadius = 8
        
        otp4TextField.layer.borderWidth = 1
        otp4TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp4TextField.layer.cornerRadius = 8
        
        otp5TextField.layer.borderWidth = 1
        otp5TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp5TextField.layer.cornerRadius = 8
        
        otp6TextField.layer.borderWidth = 1
        otp6TextField.layer.borderColor = UIColor.lightGray.cgColor
        otp6TextField.layer.cornerRadius = 8
        
        nextButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
        
        otp1TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp2TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp3TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp4TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp5TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        otp6TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(LoginOTPViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func timeString(time:TimeInterval) -> String {
        //let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        //return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        return String(format:"%02i:%02i",minutes, seconds)
    }
    
    @objc func updateTimer() {
        seconds -= 1
        //timerLabel.text = timeString(time: TimeInterval(seconds))
        let expire = timeString(time: TimeInterval(seconds))
        let phoneNumber_str = String(self.phoneNumber)
        let cut_phoneNumber = phoneNumber_str.substring(from: 5)
        //detailLabel.text = "กรุณาป้อนป้อน OTP 6 หลักที่ส่งถึง (+66) xx xxx" + cut_phoneNumber + " รหัสอ้างอิง " + self.refCode + " และจะหมดอายุภายใน " + expire + " นาที"
        
        let stringValue = "กรุณาป้อน OTP 6 หลักที่ส่งถึง (+66) xx xxx" + cut_phoneNumber + " รหัสอ้างอิง " + self.refCode + " และจะหมดอายุภายใน " + expire + " นาที"
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        attributedString.setColorForText(textForAttribute: "กรุณาป้อน OTP 6 หลักที่ส่งถึง ", withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: "(+66) xx xxx" + cut_phoneNumber, withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: " รหัสอ้างอิง ", withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: self.refCode , withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: " และจะหมดอายุภายใน ", withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        attributedString.setColorForText(textForAttribute: expire , withColor: #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2, alpha: 1))
        attributedString.setColorForText(textForAttribute: " นาที", withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        detailLabel.attributedText = attributedString
        
        if seconds == 0 {
            timer.invalidate()
        }
    }
    
    func requestOtp() {
        let token = UserModel.ApiToken
        let parameters: Parameters = [
            "phone_number": phoneNumber,
            "calling_code": callingCode
        ]
        viewModel.requestOtpUpdateProfile(api: "/setting/profile/request-otp", parameters: parameters, token: token!)
    }

    func updateProfile() {
        guard let t1 = otp1TextField.text,  let t2 = otp2TextField.text, let t3 = otp3TextField.text, let t4 = otp4TextField.text, let t5 = otp5TextField.text, let t6 = otp6TextField.text else {
        return
        }
        let otp = "\(t1)\(t2)\(t3)\(t4)\(t5)\(t6)"
        let token = UserModel.ApiToken
        let parameters: Parameters = [
            "email": email,
            "phone_number": phoneNumber,
            "calling_code":callingCode,
            "otp": otp
        ]
        viewModel.updateProfile(api: "/setting/profile", parameters: parameters, token: token!)
    }
    
    @objc override func onDataDidLoad() {
        if viewModel.apiType == "updateProfile" {
            _ = navigationController?.popViewController(animated: true)
        }else {
        
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }

}

extension EditProfileOTPViewController: UITextFieldDelegate{
    
    @objc func textFieldDidChange(textField: UITextField)
    {
        let text = textField.text
        if text?.count == 1 {
            switch textField {
            case otp1TextField:
                otp2TextField.becomeFirstResponder()
            case otp2TextField:
                otp3TextField.becomeFirstResponder()
            case otp3TextField:
                otp4TextField.becomeFirstResponder()
            case otp4TextField:
                otp5TextField.becomeFirstResponder()
            case otp5TextField:
                otp6TextField.becomeFirstResponder()
            case otp6TextField:
                otp6TextField.resignFirstResponder()
            default:
                break
            }
        } else {
            switch textField {
            case otp6TextField:
                otp5TextField.becomeFirstResponder()
            case otp5TextField:
                otp4TextField.becomeFirstResponder()
            case otp4TextField:
                otp3TextField.becomeFirstResponder()
            case otp3TextField:
                otp2TextField.becomeFirstResponder()
            case otp2TextField:
                otp1TextField.becomeFirstResponder()
            case otp1TextField:
                otp1TextField.resignFirstResponder()
            default:
                break
            }
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= maxLen
    }
}
