//
//  NotificationViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    //var menutitleDataArr = ["การแจ้งเตือน"]
    var menutitleDataArr = ["ALLOWNOTIFICATION".localized()]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTableView()
        setupUI()
        setLocalizableText()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: NotificationItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: NotificationItemTableViewCell.identifier)
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "NOTIFICATION".localized();
    }
    
    func setupUI() {
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }

    
}

extension NotificationViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotificationItemTableViewCell.identifier, for: indexPath) as? NotificationItemTableViewCell else {
            fatalError("Wrong Cell")
        }
        cell.setCell(title: menutitleDataArr[indexPath.row])
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
