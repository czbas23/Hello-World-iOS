//
//  ProfileViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import PopupDialog
import Alamofire
import SwiftyJSON

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    //var titleDataArr = ["คำนำหน้าชื่อ","ชื่อ","นามสกุล","วันเกิด","อีเมล","เบอร์โทร"]
    var titleDataArr = ["TITLE".localized(),"FIRSTNAME".localized(),"FAMILYNAME".localized(),"DATEOFBIRTH".localized(),"EMAIL".localized(),"MOBILEPHONE".localized()]
    var valueDataArr = ["นาย","ประหยัด","ขับเคลื่อน","03/03/2540","Motorcycle@gmail.com","0871122456"]
    
    var viewModel:ProfileViewModel!
    
    @IBOutlet var editButton: UIButton!
    
    var current_email:String!
    var current_phone:Int!
    
    var callingCode:Int = 66
    
    var old_email:String!
    var old_phone:Int!
    
    var maxLen:Int = 9;

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProfileViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupTableView()
        setupUI()
        setLocalizableText()
        //getProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getProfile()
    }
    
    // MARK: IBAction
    
    @IBAction func backButtonPress(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editButtonPress(_ sender: UIButton) {
        if old_email != current_email && old_phone != current_phone {
            requestOtp()
        }else {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: ProfileItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: ProfileItemTableViewCell.identifier)
        tableView.register(UINib(nibName: EditProfileTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: EditProfileTableViewCell.identifier)
        tableView.register(UINib(nibName: EditMobileProfileTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: EditMobileProfileTableViewCell.identifier)
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "PERSONALINFOMATION".localized();
        editButton.setTitle("SAVE".localized(), for: UIControl.State.normal)
    }
    
    func setupUI() {
        editButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func setupData(data:ProfileModel!) {
        valueDataArr.removeAll()
        
        if let name_prefix = data.name_prefix {
            valueDataArr.append(name_prefix)
        }else {
            valueDataArr.append("")
        }
        
        if let name = data.name {
            valueDataArr.append(name)
        }else {
            valueDataArr.append("")
        }
        
        if let surname = data.surname {
            valueDataArr.append(surname)
        }else {
            valueDataArr.append("")
        }
        
        if let birthday = data.birthday {
            valueDataArr.append(birthday)
        }else {
            valueDataArr.append("")
        }
        
        if let email = data.email {
            valueDataArr.append(email)
            current_email = email
        }else {
            valueDataArr.append("")
            current_email = ""
        }
        
        if let phone = data.phone_number {
            //let phone_str = String(phone)
            valueDataArr.append(phone)
            current_phone = Int(phone)
        }else {
            valueDataArr.append("")
            current_phone = 0
        }
        
        tableView.reloadData()
    }
    
    @objc func showDialogEditEmail(sender: UIButton) {
        showDialogEdit(title:"แก้ไขอีเมล",buttontitle:"ตกลง",type:"email")
    }
    
    @objc func showDialogEditMobil(sender: UIButton) {
        showDialogEdit(title:"แก้ไขเบอร์โทร",buttontitle:"ตกลง",type:"mobile")
    }
    
    func showDialogEdit(title:String,buttontitle:String,type:String) {
        //isTripActionPause = true
        // Create a custom view controller
        let vc = DialogEditProfileViewController(nibName: "DialogEditProfileViewController", bundle: nil)
        // Create the dialog
        let popup = PopupDialog(viewController: vc,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false)
        // Create button
        let buttonCancel = DefaultButton(title: "ยกเลิก", height: 50) {
            
        }
        buttonCancel.titleColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        buttonCancel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        if type == "mobile" {
            vc.editTextField.keyboardType = .numberPad
            vc.editTextField.delegate = self
        }
        
        // Create button
        let buttonOK = DefaultButton(title: buttontitle, height: 50) {
            if type == "email" {
                self.current_email = vc.editTextField.text
                self.valueDataArr.remove(at: 4)
                //insert ตามตำแหน่งข้อมูล email
                self.valueDataArr.insert(self.current_email, at: 4)
            }else {
                let phone = Int(vc.editTextField.text!)
                self.current_phone = phone
                self.valueDataArr.remove(at: 5)
                self.valueDataArr.insert(vc.editTextField.text!, at: 5)
            }
            //self.updateProfile()
            self.tableView.reloadData()
        }
        buttonOK.titleColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonOK.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.6862745098, blue: 0.9254901961, alpha: 1)
        
        vc.titleLabel.text = title
        
        // Add buttons to dialog
        popup.addButtons([buttonCancel,buttonOK])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    func getProfile() {
        let token = UserModel.ApiToken
        viewModel.getProfile(api: "/setting/profile", parameters: nil, token: token!)
    }
    
    func requestOtp() {
        let token = UserModel.ApiToken
//        let parameters: Parameters = [
//            "phone_number": current_phone,
//            "calling_code": callingCode
//        ]
        let parameters: Parameters = [:]
        viewModel.requestOtpUpdateProfile(api: "/setting/profile/request-otp", parameters: parameters, token: token!)
    }

    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getProfile" {
            setupData(data:viewModel.profileModel)
        }else if viewModel.apiType == "requestOtpUpdateProfile" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EditProfileOTPViewController") as! EditProfileOTPViewController
            vc.email = current_email
            vc.phoneNumber = current_phone
            vc.refCode = viewModel.otpData["ref"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
    
}

extension ProfileViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row <= 3 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileItemTableViewCell.identifier, for: indexPath) as? ProfileItemTableViewCell else {
                fatalError("Wrong Cell")
            }
            cell.setCell(title: titleDataArr[indexPath.row], value: valueDataArr[indexPath.row])
            return cell
        }else if indexPath.row == 4 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditProfileTableViewCell.identifier, for: indexPath) as? EditProfileTableViewCell else {
                fatalError("Wrong Cell")
            }
            cell.setCell(title: titleDataArr[indexPath.row], value: valueDataArr[indexPath.row])
            //edit email
            cell.editButton.addTarget(self, action: #selector(ProfileViewController.showDialogEditEmail(sender:)), for: .touchUpInside)
            return cell
        }else {
            //EditMobileProfileTableViewCell
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditMobileProfileTableViewCell.identifier, for: indexPath) as? EditMobileProfileTableViewCell else {
                fatalError("Wrong Cell")
            }
            cell.setCell(title: titleDataArr[indexPath.row], value: valueDataArr[indexPath.row])
            //edit mobile
            cell.editButton.addTarget(self, action: #selector(ProfileViewController.showDialogEditMobil(sender:)), for: .touchUpInside)
            return cell
        }
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension ProfileViewController: UITextFieldDelegate{
    
    // MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= maxLen
    }
}
