//
//  MenulistViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SwiftyJSON

class MenulistViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var versionAppLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    
    //var menutitleDataArr = ["ประวัติการเดินทาง","การแจ้งเตือน","โปรโมชั่น","กระเป๋า","แจ้งปัญหาการใช้งาน","ตั้งค่า"]
    var menutitleDataArr = ["TRIPHISTORY".localized(),"NOTIFICATION".localized(),"PROMOTION".localized(),"WALLET".localized(),"USAGEREPORTISSUES".localized(),"SETTINGS".localized()]
    var menuimageDataArr = ["icon_history_menu","icon_notofication_menu","icon_promotion_menu","icon_bag_menu","icon_issue_menu","icon_setting_menu"]
    
    var notificationHistoryData:JSON!
    var notificationModel:NotificationModel!
    
    var totalNotification_notReade:Int = 0
    
    var profileData:JSON!
    var profileModel:ProfileModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTableView()
        versionAppLabel.text = Utils.shared.getVersionApp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //hidden NavigationBar
        navigationController?.setNavigationBarHidden(true, animated: animated)
        //set color UINavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.white
        setLocalizableText()
        getNotificationHistory()
        
        if let fullname_user = UserDefaults.standard.string(forKey:"USERFULLNAME") {
            self.fullnameLabel.text = fullname_user
        }else {
            getProfile()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //show NavigationBar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: IBAction
    
    @IBAction func termAndServiceButtonPress(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TermsOfServiceViewController") as! TermsOfServiceViewController
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    func setLocalizableText() {
        menutitleDataArr = ["TRIPHISTORY".localized(),"NOTIFICATION".localized(),"PROMOTION".localized(),"WALLET".localized(),"USAGEREPORTISSUES".localized(),"SETTINGS".localized()]
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: MenuItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: MenuItemTableViewCell.identifier)
        tableView.register(UINib(nibName: MenuNotiItemTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: MenuNotiItemTableViewCell.identifier)
        
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func getNotificationHistory() {
        let token = UserModel.ApiToken
        getNotificationHistory(api: "/notification/history", parameters: nil, token: token!)
    }
    
    func getProfile() {
        let token = UserModel.ApiToken
        getProfile(api: "/setting/profile", parameters: nil, token: token!)
    }
    
    func getNotificationHistory(api:String ,parameters:Parameters?, token:String) {
        APIService.sharedManager.getRequestAPIWithHeaderAndResponseList(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                if(responseObject != nil){
                    self.notificationHistoryData = JSON(responseObject!)
                    print(self.notificationHistoryData)
                    self.notificationModel = NotificationModel(dict:  responseObject as! [Dictionary<String, Any>])
                    self.totalNotification_notReade = 0
                    //check reded
                    let num:Int = self.notificationModel.notificationHistoryList!.count
                    for index in 0..<num {
                        if let readed = self.notificationModel.notificationHistoryList?[index].readed {
                            if readed {
                            }else {
                                self.totalNotification_notReade += 1
                            }
                        }
                    }
                    self.tableView.reloadData()
                }else{
                    //let errorMessage = error
                }
            }
        }
    }
    
    func getProfile(api:String ,parameters:Parameters?, token:String) {
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                if(responseObject != nil){
                    self.profileData = JSON(responseObject!)
                    print(self.profileData)
                    self.profileModel = ProfileModel(dict: responseObject as! Dictionary<String, Any>)
                    
                    var fullname = ""
                    if let name = self.profileModel.name {
                        fullname = name
                    }
                    if let surname = self.profileModel.surname {
                        fullname += " \(surname)"
                    }
                    //save full name user
                    UserDefaults.standard.set(fullname, forKey:"USERFULLNAME")
                    
                    if let fullname_user = UserDefaults.standard.string(forKey:"USERFULLNAME") {
                        self.fullnameLabel.text = fullname_user
                    }
                }else{
                    //let errorMessage = error
                }
            }
        }
    }

}


extension MenulistViewController: UITableViewDataSource ,UITableViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 1 {
            //notification list
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MenuNotiItemTableViewCell.identifier, for: indexPath) as? MenuNotiItemTableViewCell else {
                fatalError("Wrong Cell")
            }
            cell.setCell(imagename: menuimageDataArr[indexPath.row], title: menutitleDataArr[indexPath.row],notificationNum:self.totalNotification_notReade)
            return cell
        }else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MenuItemTableViewCell.identifier, for: indexPath) as? MenuItemTableViewCell else {
                fatalError("Wrong Cell")
            }
            cell.setCell(imagename: menuimageDataArr[indexPath.row], title: menutitleDataArr[indexPath.row])
            
            return cell
        }
        
        
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == 0) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HistoryListViewController") as! HistoryListViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationListViewController") as! NotificationListViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 2) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PromotionViewController") as! PromotionViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 3) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 4) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ReportProblemViewController") as! ReportProblemViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        if (indexPath.row == 5) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
