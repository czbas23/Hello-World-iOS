//
//  TermsOfServiceViewController.swift
//  Rent Motorcycle
//
//  Created by wisuij on 4/3/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TermsOfServiceViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var isSelectTerm:Bool = false
    
    var phone_number:String!
    
    var termData:JSON!
    var isLodeData:Bool = false
    var htmlData:String = ""
    var contentHeights : [CGFloat] = [0.0, 0.0]
    var isKnowContentSizeHeight:Bool = false
    var checkHeight:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setupTableView()
        getDataTerm()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: TermsTableViewCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: TermsTableViewCell.identifier)
        
        //ลบเส้นด้านล่างใน cell อื่นๆที่ว่าง
        tableView.tableFooterView = UIView()
    }
    
    func setupUI() {
        self.navigationItem.title = "เงื่อนไขการให้บริการ"
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func getDataTerm() {
        getTerm(api: "/terms-and-data-privacy")
    }
    
    func getTerm(api:String) {
        APIService.sharedManager.getRequestAPI(api: api){ responseObject,error in
            DispatchQueue.main.async {
                if(responseObject != nil){
                    self.termData = JSON(responseObject!)
                    //print(self.termData)
                    self.isLodeData = true
                    if let term = self.termData["terms"].string {
                        self.htmlData += term
                    }
                    if let dataprivacy = self.termData["data_privacy"].string {
                        self.htmlData += dataprivacy
                    }
                    self.tableView.reloadData()
                }else{
                    //let errorMessage = error
                }
            }
        }
    }
}

extension TermsOfServiceViewController: UITableViewDataSource ,UITableViewDelegate ,UIWebViewDelegate{
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLodeData {
            return 1
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TermsTableViewCell.identifier, for: indexPath) as? TermsTableViewCell else {
            fatalError("Wrong Cell")
        }
        //cell.knowCheckbox?.addTarget(self, action: #selector(RegisterViewController.gotItTerm(sender:)), for: .valueChanged)
        //cell.registerButton?.addTarget(self, action: #selector(RegisterViewController.nextRegister(sender:)), for: .touchUpInside)
        //cell.setDataWebView(htmlData: self.htmlData)
        cell.knowCheckbox?.isHidden = true
        cell.registerButton.isHidden = true
        cell.textAcceptLabel.isHidden = true
        cell.webView.tag = indexPath.row
        cell.webView.delegate = self
        cell.webView.loadHTMLString(self.htmlData, baseURL: nil)
        cell.setCell()
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 14000
        //300 fix สำหรับ logo,check mark,ปุ่ม
        return contentHeights[indexPath.row] + 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // un higlight tableView
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        if isKnowContentSizeHeight {
            if (contentHeights[webView.tag] != 0.0)
            {
                // we already know height, no need to reload cell
                return
            }
        }
        
        checkHeight += 1
        if checkHeight >= 2 {
            //know height
            isKnowContentSizeHeight = true
        }
        
        contentHeights[webView.tag] = webView.scrollView.contentSize.height
        tableView.reloadRows(at: [NSIndexPath(row: webView.tag, section: 0) as IndexPath], with: .automatic)
    }
    
}

