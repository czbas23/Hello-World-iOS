//
//  QRTopUpViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

class QRTopUpViewController: UIViewController {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var qrcodeImageView: UIImageView!
    @IBOutlet var topUpLabel: UILabel!
    var image_string_base64:String!
    var cash:Int!
    
    @IBOutlet var detailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setLocalizableText()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "TOPUP".localized();
        detailLabel.text = "THISSAVEQRCODE".localized();
        nextButton.setTitle("CLOSE".localized(), for: UIControl.State.normal)
    }
    
    func setupUI() {
        nextButton.layer.cornerRadius = 8
    
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
        
        qrcodeImageView.image = convertBase64ToImage(imageString: self.image_string_base64)
        let bath = "BATH".localized();
        topUpLabel.text = "\("TOPUP".localized()) \(cash ?? 0) \(bath)"
        
        //- Saving Image here
        if let image = qrcodeImageView.image {
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        
    }
    
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    // MARK: IBAction
    
    @IBAction func backButtonPress(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: WalletViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showAlertWith(title: "Save error", message: error.localizedDescription)
        } else {
            //showAlertWith(title: "Saved!", message: "Your image has been saved to your photos.")
        }
    }
    
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }

}
