//
//  TopUpViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown

class TopUpViewController: BaseViewController {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var moneyTextField: UITextField!
    @IBOutlet var selectCashButton: UIButton!
    
    @IBOutlet var titleLabel: UILabel!

    let cashDropDown = DropDown()
    
    var viewModel:WalletViewModel!
    var select_cash:Int = 50
    var cashesData:JSON!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = WalletViewModel(delegate: self)
        // Do any additional setup after loading the view.
        setupUI()
        setupCashDropDown()
        setLocalizableText()
    }
    
    // MARK: IBAction
    
    @IBAction func selectCashButtonPress(_ sender: AnyObject) {
        cashDropDown.show()
    }
    
    @IBAction func topupButtonPress(_ sender: UIButton) {
        topUp()
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "TOPUP".localized();
        titleLabel.text = "SELECTTOPUP".localized();
        nextButton.setTitle("CREATQRCODE".localized(), for: UIControl.State.normal)
    }

    func setupUI() {
        let monney_default:Double = Double(select_cash)
        let this_value = String(format: "%.2f", monney_default)
        let bath = "BATH".localized();
        moneyTextField.text = "\(this_value) \(bath)"
        //moneyTextField.text = "50 บาท"
        
        moneyTextField.layer.borderWidth = 1
        moneyTextField.layer.borderColor = UIColor.lightGray.cgColor
        moneyTextField.layer.cornerRadius = 8
        
        nextButton.layer.cornerRadius = 8
        
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
    }
    
    func topUp() {
        if select_cash != 0 {
            let token = UserModel.ApiToken
            let parameters: Parameters = [
                "cash": select_cash
            ]
            viewModel.topUpGenerateQRcode(api: "/wallet/generate-qrcode", parameters: parameters, token: token!)
        }
    }
    
    @objc override func onDataDidLoad() {
        let image_base64 = viewModel.generateQRcodeData["qrcode"]["image_base64"].stringValue
        let cash = viewModel.generateQRcodeData["cash"].intValue
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QRTopUpViewController") as! QRTopUpViewController
        vc.image_string_base64 = image_base64
        vc.cash = cash
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
    
    
    // MARK: DropDown
    
    func setupCashDropDown() {
        cashDropDown.anchorView = selectCashButton
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        cashDropDown.bottomOffset = CGPoint(x: 0, y: selectCashButton.bounds.height)
        
        var cashDataSource = [String]()
        let num_list:Int = self.cashesData["cashes"].count
        if num_list > 0 {
            for index in 0...num_list-1 {
                let cash = self.cashesData["cashes"][index].intValue
                let cash_string = "\(cash)"
                cashDataSource.append(cash_string)
            }
        }else {
        }
        
        cashDropDown.dataSource = cashDataSource
        
        /*
         // You can also use localizationKeysDataSource instead. Check the docs.
         stationDropDown.dataSource = [
         "Lorem ipsum dolor",
         "sit amet consectetur",
         "cadipisci en..."
         ]*/
        
        // Action triggered on selection
        cashDropDown.selectionAction = { [weak self] (index, item) in
            //self?.selectStationButton.setTitle(item, for: .normal)
            
            let cash = self?.cashesData["cashes"][index].intValue
            self?.select_cash = cash!
            let monney_double:Double = Double(cash!)
            let this_value = String(format: "%.2f", monney_double)
            let bath = "BATH".localized();
            self?.moneyTextField.text = "\(this_value) \(bath)"
        }
    }

}
