//
//  WalletViewController.swift
//  Rent Motorcycle
//
//  Created by CNT on 25/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WalletViewController: BaseViewController {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var mockView: UIView!
    @IBOutlet var balanceLabel: UILabel!
    
    @IBOutlet var balanceTitleLabel: UILabel!
    @IBOutlet var balanceDetailLabel: UILabel!
    
    var viewModel:WalletViewModel!
    
    var cashesData:JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = WalletViewModel(delegate: self)

        // Do any additional setup after loading the view.
        setupUI()
        setLocalizableText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBalance()
    }
    
    @IBAction func nextButtonPress(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TopUpViewController") as! TopUpViewController
        vc.cashesData = self.cashesData
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    func setLocalizableText(){
        self.navigationItem.title = "TOPUP".localized();
        balanceTitleLabel.text = "BALANCE".localized();
        balanceDetailLabel.text = "MINIMUMBALANCE".localized();
        nextButton.setTitle("TOPUP".localized(), for: UIControl.State.normal)
    }
    
    func setupUI() {
        nextButton.layer.cornerRadius = 8
        //set backBarButtonItem notitle
        navigationItem.backBarButtonItem = Utils.shared.setNotextBackBarButtonItem(backButton: navigationItem.backBarButtonItem ?? nil)
        
        mockView.layer.masksToBounds = false
        mockView.layer.shadowColor = UIColor.black.cgColor
        mockView.layer.shadowOpacity = 0.5
        mockView.layer.shadowOffset = CGSize(width: -1, height: 1)
        mockView.layer.shadowRadius = 4
    }
    
    func getBalance() {
        let token = UserModel.ApiToken
        viewModel.getBalance(api: "/wallet/balance", parameters: nil, token: token!)
    }
    
    func getCash() {
        let token = UserModel.ApiToken
        viewModel.getCash(api: "/wallet/cash", parameters: nil, token: token!)
    }
    
    @objc override func onDataDidLoad() {
        if viewModel.apiType == "getBalance" {
            let user_balance = viewModel.balanceData["balance"].doubleValue
            //set ทศนิยม 2 ตำแหน่ง
            //let this_value = user_balance.rounded(toPlaces: 2)
            let this_value = String(format: "%.2f", user_balance)
            self.balanceLabel.text = "\(this_value) ฿"
            getCash()
        }else if viewModel.apiType == "getCash" {
            self.cashesData = viewModel.cashData
        }
    }
    
    @objc override func onDataDidLoadErrorWithMessage(errorMessage: String) {
        AlertMessageAction.sharedManager.showAlertMessage(vc: self, message: errorMessage, buttonTitle: "ปิด")
    }
}
