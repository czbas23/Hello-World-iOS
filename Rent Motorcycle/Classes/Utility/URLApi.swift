//
//  URLApi.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

public class URLApi{
    
    //dev
    //https://app-dev.moresai.com/api/mobile
    //staging
    //https://app-staging.moresai.com/api/mobile
    public static let baseUrl = "https://app-dev.moresai.com/api/mobile"
    public static let mockUpApiUrl = "https://820994e6-8837-483d-aeb3-2ad24db4ed62.mock.pstmn.io"
    
    
}
