//
//  AlertMessageAction.swift
//  Rent Motorcycle
//
//  Created by wisuij on 4/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit

enum Erortype :String{
    case Fillin
    case FormatEmail
}

class AlertMessageAction: NSObject {
    
    static let sharedManager = AlertMessageAction()
    
    func showAlertEror(caseErortype:Erortype ,vc:UIViewController) {
        var errorMessage:String
        switch caseErortype {
        case .Fillin:
            errorMessage = "Please fill in all information." //กรุณากรอกข้อมูลให้ครบ
        case .FormatEmail:
            errorMessage = "Invalid email format."
        }
        
        
        let alert = UIAlertController(title: "", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
        
        
        
        /*
         let alertview = JSSAlertView().show(vc,
         title: errorMessage,
         text: "",
         buttonText: "OK",
         color: UIColorFromHex(0xffffff, alpha: 1),
         iconImage: nil)
         alertview.setTextTheme(.dark)*/
    }
    
    func showAlertMessage(vc:UIViewController ,message: String ,buttonTitle:String) {
        //        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        //        vc.present(alert, animated: true, completion: nil)
        
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: nil))
        alert.show()
    }
    
    func showAlertMessageErrorTitle(vc:UIViewController ,message: String ,buttonTitle:String ,title:String) {
        //        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        //        vc.present(alert, animated: true, completion: nil)
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: nil))
        alert.show()
    }
    
    /*
     func showAlertMessageNotitle(vc:UIViewController, message:String, buttonTitle:String){
     let customIcon = UIImage(named: "error_popup_icn")
     let alertview = JSSAlertView().show(vc,
     title: message,
     text: "",
     buttonText: buttonTitle,
     color: UIColorFromHex(0xffffff, alpha: 1),
     iconImage: customIcon)
     alertview.setTextTheme(.dark)
     }*/
    
}

public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}
