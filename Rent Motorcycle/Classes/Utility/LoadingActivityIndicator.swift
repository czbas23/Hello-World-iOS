//
//  LoadingActivityIndicator.swift
//  Cu Cudson
//
//  Created by wisuij on 7/21/2561 BE.
//  Copyright © 2561 wisuij. All rights reserved.
//

import Foundation
import UIKit

class LoadingActivityIndicator: NSObject {
    
    static let sharedManager = LoadingActivityIndicator()
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func showActivityIndicator(uiView: UIView) {
        let screenSize: CGRect = UIScreen.main.bounds
        container.frame =  screenSize
        //container.center = uiView.center
        container.backgroundColor = UIColorFromHex(rgbValue: 0x000000, alpha: 0.35)
        
        loadingView.frame = CGRect(x: 0.0, y: 0.0, width: 60, height: 60)
        loadingView.center = container.center //uiView.center
        loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.0)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        activityIndicator.transform = transform
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y:loadingView.frame.size.height / 2)
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        //uiView.addSubview(container)
        
        let window = UIApplication.shared.keyWindow!
        window.addSubview(container)
        
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
}

