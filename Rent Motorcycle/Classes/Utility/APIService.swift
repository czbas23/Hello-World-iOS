//
//  APIService.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class APIService: NSObject {
    
    static let sharedManager = APIService()
    
    func sendRequestAPI(api:String, parameters: Parameters, completionHandler: @escaping (NSDictionary?, String?)-> ()){
        
        let url = URLApi.baseUrl + api
        Alamofire.request(url , method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON{ response in
            switch response.result {
            case .success(let value):
                //completionHandler(value as? NSDictionary, nil)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 200:
                        completionHandler(value as? NSDictionary, nil)
//                    case 401:
//                        message = ThisErrorMessage.EXPIREDTOKEN
//                        completionHandler(nil, message)
                    default :
                        let valueResponse = value as? NSDictionary
                        let responseData = JSON(valueResponse!)
                        //print(self.responseData)
                        if let message_response = responseData["message"].string {
                            message = message_response
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                        }
                        completionHandler(nil, message)
                    }
                }else {
                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                    completionHandler(nil, message)
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    func sendRequestAPIWithHeader(api:String, parameters: Parameters?, token: String, completionHandler: @escaping (NSDictionary?, String?)-> ()){
        
        let headers: [String : String] = [
            "Authorization": "Bearer \(token)"
        ]
        
        let url = URLApi.baseUrl + api
        Alamofire.request(url , method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                //completionHandler(value as? NSDictionary, nil)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 200:
                        completionHandler(value as? NSDictionary, nil)
//                    case 400:
//                        message = ThisErrorMessage.ERRORSTATUS400
//                        completionHandler(nil, message)
//                    case 401:
//                        message = ThisErrorMessage.EXPIREDTOKEN
//                        completionHandler(nil, message)
                    default :
                        let valueResponse = value as? NSDictionary
                        let responseData = JSON(valueResponse!)
                        //print(self.responseData)
                        if let message_response = responseData["message"].string {
                            message = message_response
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                        }
                        completionHandler(nil, message)
                    }
                }else {
                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                    completionHandler(nil, message)
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    func getRequestAPIWithHeaderAndResponseList(api:String, parameters: Parameters?, token: String, completionHandler: @escaping ([NSDictionary]?, String?)-> ()){
        
        let headers: [String : String] = [
            "Authorization": "Bearer \(token)"
        ]
        
        let url = URLApi.baseUrl + api
        Alamofire.request(url , method: .get, parameters: parameters,encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 200:
                        //.ใช้ [NSDictionary] เพราะ response เป็น[]
                        completionHandler(value as? [NSDictionary], nil)
                    //completionHandler(value as? NSDictionary, nil)
//                    case 401:
//                        message = ThisErrorMessage.EXPIREDTOKEN
//                        completionHandler(nil, message)
                    default :
                        let valueResponse = value as? NSDictionary
                        let responseData = JSON(valueResponse!)
                        //print(self.responseData)
                        if let message_response = responseData["message"].string {
                            message = message_response
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                        }
                        completionHandler(nil, message)
                    }
                }else {
                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                    completionHandler(nil, message)
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    func getRequestAPIWithHeader(api:String, parameters: Parameters?, token: String, completionHandler: @escaping (NSDictionary?, String?)-> ()){
        
        let headers: [String : String] = [
            "Authorization": "Bearer \(token)"
        ]
        
        let url = URLApi.baseUrl + api
        Alamofire.request(url , method: .get, parameters: parameters,encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                //completionHandler(value as? NSDictionary, nil)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 200:
                        completionHandler(value as? NSDictionary, nil)
//                    case 401:
//                        message = ThisErrorMessage.EXPIREDTOKEN
//                        completionHandler(nil, message)
                    default :
                        let valueResponse = value as? NSDictionary
                        let responseData = JSON(valueResponse!)
                        //print(self.responseData)
                        if let message_response = responseData["message"].string {
                            message = message_response
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                        }
                        completionHandler(nil, message)
                    }
                }else {
                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                    completionHandler(nil, message)
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    

    func getRequestAPIMockUp(api:String, parameters: Parameters?, token: String, completionHandler: @escaping ([NSDictionary]?, String?)-> ()){
        
        let headers: [String : String] = [
            "Authorization": "Bearer \(token)"
        ]
        
        //let url = URLApi.mockUpApiUrl + api
        let url = URLApi.baseUrl + api
        Alamofire.request(url , method: .get, parameters: parameters,encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 200:
                        //.ใช้ [NSDictionary] เพราะ response เป็น[]
                        completionHandler(value as? [NSDictionary], nil)
                        //completionHandler(value as? NSDictionary, nil)
//                    case 401:
//                        message = ThisErrorMessage.EXPIREDTOKEN
//                        completionHandler(nil, message)
                    default :
                        let valueResponse = value as? NSDictionary
                        let responseData = JSON(valueResponse!)
                        //print(self.responseData)
                        if let message_response = responseData["message"].string {
                            message = message_response
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                        }
                        completionHandler(nil, message)
                    }
                }else {
                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                    completionHandler(nil, message)
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    func getRequestAPI(api:String, completionHandler: @escaping (NSDictionary?, String?)-> ()){
        let url = URLApi.baseUrl + api
        Alamofire.request(url , method: .get ,encoding: JSONEncoding.default, headers: nil).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 200:
                        completionHandler(value as? NSDictionary, nil)
                        //                    case 401:
                        //                        message = ThisErrorMessage.EXPIREDTOKEN
                    //                        completionHandler(nil, message)
                    default :
                        let valueResponse = value as? NSDictionary
                        let responseData = JSON(valueResponse!)
                        //print(self.responseData)
                        if let message_response = responseData["message"].string {
                            message = message_response
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                        }
                        completionHandler(nil, message)
                    }
                }else {
                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                    completionHandler(nil, message)
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    func getRequestAPIAndResponseList(api:String, parameters: Parameters?, completionHandler: @escaping ([NSDictionary]?, String?)-> ()){
        
        let url = URLApi.baseUrl + api
        Alamofire.request(url , method: .get, parameters: parameters,encoding: JSONEncoding.default).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 200:
                        //.ใช้ [NSDictionary] เพราะ response เป็น[]
                        completionHandler(value as? [NSDictionary], nil)
                    //completionHandler(value as? NSDictionary, nil)
//                    case 401:
//                        message = ThisErrorMessage.EXPIREDTOKEN
//                        completionHandler(nil, message)
                    default :
                        let valueResponse = value as? NSDictionary
                        let responseData = JSON(valueResponse!)
                        //print(self.responseData)
                        if let message_response = responseData["message"].string {
                            message = message_response
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                        }
                        completionHandler(nil, message)
                    }
                }else {
                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                    completionHandler(nil, message)
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    /*
    func sendRequestUploadImage(api:String, parameters: Parameters, image:UIImage, completionHandler: @escaping (NSDictionary?, Error?)-> ()){
        let url = URLApi.baseUrl + api
        let imageData = UIImageJPEGRepresentation(image, 0.5) //set ขนาดคุณภาพของรูปภาพ
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(imageData!, withName: "image" , fileName: "post.jpg", mimeType: "image/jpg")
                for (key, val) in parameters {
                    multipartFormData.append((val as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        },
            to: url,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        //debugPrint(response)
                        completionHandler(response.value as? NSDictionary, nil)
                    }
                case .failure(let encodingError):
                    //print(encodingError)
                    completionHandler(nil, encodingError)
                }
        }
        )
    }*/
    
    func sendRequestUploadImageRegister(api:String, parameters: Parameters, image:UIImage ,image2:UIImage, completionHandler: @escaping (NSDictionary?, String?)-> ()){
        
        let headers: [String : String] = [
            "Accept":"application/json",
            "Api-Version":"1.0",
            "Accept-Language":"th"
        ]
        
        let url = URLApi.baseUrl + api
        let imageData = image.jpegData(compressionQuality: 0.3) //set ขนาดคุณภาพของรูปภาพ
        let imageData2 = image2.jpegData(compressionQuality: 0.3) //set ขนาดคุณภาพของรูปภาพ
        let randomString = Utils.shared.randomString(length: 8)
        let randomString2 = Utils.shared.randomString(length: 8)
        let fileImageName = "\(randomString).jpg"
        let fileImageName2 = "\(randomString2).jpg"
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(imageData!, withName: "driving_license" , fileName: fileImageName, mimeType: "image/jpg")
                multipartFormData.append(imageData2!, withName: "selfie_driving_license" , fileName: fileImageName2, mimeType: "image/jpg")
                for (key, val) in parameters {
                    if let temp = val as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if val is Int {
                        multipartFormData.append("(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = val as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if element is Int {
                                    let value = "(num)"
                                    multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
        },
            to: url,
            method:.post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        /*
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)
                        print(response.value)
                        */
                        //print(response.value)
                        let message : String
                        if let httpStatusCode = response.response?.statusCode {
                            switch(httpStatusCode) {
                            case 200:
                                completionHandler(response.value as? NSDictionary, nil)
                            case 201:
                                //create complete
                                completionHandler(response.value as? NSDictionary, nil)
//                            case 401:
//                                message = ThisErrorMessage.EXPIREDTOKEN
//                                completionHandler(nil, message)
                            default :
                                let valueResponse = response.value as? NSDictionary
                                let responseData = JSON(valueResponse!)
                                //print(self.responseData)
                                if let message_response = responseData["message"].string {
                                    message = message_response
                                }else {
                                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                                }
                                completionHandler(nil, message)
                            }
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                            completionHandler(nil, message)
                        }
                        
                    }
                case .failure(let error):
                    completionHandler(nil, error.localizedDescription)
                }
        }
        )
    }
    
    func sendRequestUploadImageResubmit(api:String, parameters: Parameters, image:UIImage? ,image2:UIImage?, completionHandler: @escaping (NSDictionary?, String?)-> ()){
        
        let headers: [String : String] = [
            "Accept":"application/json",
            "Api-Version":"1.0",
            "Accept-Language":"th"
        ]
        
        let url = URLApi.baseUrl + api
        let randomString = Utils.shared.randomString(length: 8)
        let randomString2 = Utils.shared.randomString(length: 8)
        let fileImageName = "\(randomString).jpg"
        let fileImageName2 = "\(randomString2).jpg"
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if image != nil {
                    let imageData = image!.jpegData(compressionQuality: 0.3) //set ขนาดคุณภาพของรูปภาพ
                    multipartFormData.append(imageData!, withName: "driving_license" , fileName: fileImageName, mimeType: "image/jpg")
                }
                if image2 != nil {
                    let imageData2 = image2!.jpegData(compressionQuality: 0.3) //set ขนาดคุณภาพของรูปภาพ
                    multipartFormData.append(imageData2!, withName: "selfie_driving_license" , fileName: fileImageName2, mimeType: "image/jpg")
                }
                
                
                for (key, val) in parameters {
                    if let temp = val as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if val is Int {
                        multipartFormData.append("(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = val as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if element is Int {
                                    let value = "(num)"
                                    multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
        },
            to: url,
            method:.post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        /*
                         print(response.request)  // original URL request
                         print(response.response) // URL response
                         print(response.data)     // server data
                         print(response.result)
                         print(response.value)
                         */
                        //print(response.value)
                        let message : String
                        if let httpStatusCode = response.response?.statusCode {
                            switch(httpStatusCode) {
                            case 200:
                                completionHandler(response.value as? NSDictionary, nil)
                            case 201:
                                //create complete
                                completionHandler(response.value as? NSDictionary, nil)
                                //                            case 401:
                                //                                message = ThisErrorMessage.EXPIREDTOKEN
                            //                                completionHandler(nil, message)
                            default :
                                let valueResponse = response.value as? NSDictionary
                                let responseData = JSON(valueResponse!)
                                //print(self.responseData)
                                if let message_response = responseData["message"].string {
                                    message = message_response
                                }else {
                                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                                }
                                completionHandler(nil, message)
                            }
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                            completionHandler(nil, message)
                        }
                        
                    }
                case .failure(let error):
                    completionHandler(nil, error.localizedDescription)
                }
        }
        )
    }
    
    func sendRequestUploadMultiImageWithHeader(api:String, parameters: Parameters, image:[UIImage], token: String , completionHandler: @escaping (NSDictionary?, String?)-> ()){
        
        let headers: [String : String] = [
            "Authorization": "Bearer \(token)",
            "Accept":"application/json",
            "Api-Version":"1.0",
            "Accept-Language":"th"
        ]
        
        let url = URLApi.baseUrl + api
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for index in 0..<image.count {
                    let image = image[index]
                    let imageData = image.jpegData(compressionQuality: 0.1) //set ขนาดคุณภาพของรูปภาพ
                    let randomString = Utils.shared.randomString(length: 8)
                    let fileImageName = "\(randomString).jpg"
                    multipartFormData.append(imageData!, withName: "images[]" , fileName: fileImageName, mimeType: "image/jpg")
                }
                
                for (key, val) in parameters {
                    if let temp = val as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if val is Int {
                        multipartFormData.append("(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = val as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if element is Int {
                                    let value = "(num)"
                                    multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
        },
            to: url,
            method:.post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        //print(response.value)
                        let message : String
                        if let httpStatusCode = response.response?.statusCode {
                            switch(httpStatusCode) {
                            case 200:
                                completionHandler(response.value as? NSDictionary, nil)
                            case 201:
                                //create complete
                                completionHandler(response.value as? NSDictionary, nil)
//                            case 401:
//                                message = ThisErrorMessage.EXPIREDTOKEN
//                                completionHandler(nil, message)
                            case 413:
                                //create complete
                                message = "Payload Too Large"
                                completionHandler(nil, message)
                            default :
                                let valueResponse = response.value as? NSDictionary
                                let responseData = JSON(valueResponse!)
                                //print(self.responseData)
                                if let message_response = responseData["message"].string {
                                    message = message_response
                                }else {
                                    message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                                }
                                completionHandler(nil, message)
                            }
                        }else {
                            message = ThisErrorMessage.ERRORCANNOTTRANSACTION
                            completionHandler(nil, message)
                        }
                    }
                case .failure(let error):
                    completionHandler(nil, error.localizedDescription)
                }
        }
        )
    }
    
    
}

