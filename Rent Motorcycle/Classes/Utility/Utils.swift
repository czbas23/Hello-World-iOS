//
//  Utils.swift
//  Rent Motorcycle
//
//  Created by CNT on 27/11/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import Foundation
import UIKit


class Utils: NSObject {
    
    static let shared = Utils()
    
    func setNotextBackBarButtonItem(backButton:UIBarButtonItem?) -> UIBarButtonItem {
        return UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func convertTimeSecondToTimeString(time:Int) -> String {
        //let hours = Int(time) / 3600
        let minutes = time / 60 % 60
        let seconds = time % 60
        //return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        return String(format:"%02i:%02i",minutes, seconds)
    }
    
    func timeLocalToTimeZone(date:Date,formatter:String,timezone:String) -> String {
        let gregorianCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = gregorianCalendar! as Calendar
        dateFormatter.dateFormat = formatter
        dateFormatter.timeZone = TimeZone.current
        let dateString = dateFormatter.string(from: date)
        
        let dt = dateFormatter.date(from: dateString)
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.timeZone = TimeZone(abbreviation:timezone)
        dateFormatter.dateFormat = formatter
        
        return dateFormatter.string(from: dt!)
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func convertTimestampToDateString(timestamp:Int) -> String {
        /*
        let unixTimestamp = timestamp
        let date = Date(timeIntervalSince1970: TimeInterval(unixTimestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate*/
        
        var now_Date = ""
        let unixTimestamp = timestamp
        let date = Date(timeIntervalSince1970: TimeInterval(unixTimestamp))
        let buddhistCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.buddhist)
        let dateFormate = DateFormatter()
        dateFormate.timeZone = TimeZone(abbreviation: "GMT+7") //Set timezone that you want
        dateFormate.locale = NSLocale.current
        dateFormate.calendar = buddhistCalendar! as Calendar
        dateFormate.dateFormat = "dd"
        let dd = dateFormate.string(from: date)
        dateFormate.dateFormat = "yyyy"
        let yyyy = dateFormate.string(from: date)
        dateFormate.dateFormat = "HH:mm น."
        let time = dateFormate.string(from: date)
        
        let calendar = Calendar.current
        let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date)
        
        let mont  = "\(anchorComponents.month!)"
        var mont_str:String
        switch (mont) {
        case "1" :
            mont_str = "มกราคม"
        case "2" :
            mont_str = "กุมภาพันธ์"
        case "3" :
            mont_str = "มีนาคม"
        case "4" :
            mont_str = "เมษายน"
        case "5" :
            mont_str = "พฤษภาคม"
        case "6" :
            mont_str = "มิถุนายน"
        case "7" :
            mont_str = "กรกฎาคม"
        case "8" :
            mont_str = "สิงหาคม"
        case "9" :
            mont_str = "กันยายน"
        case "10" :
            mont_str = "ตุลาคม"
        case "11" :
            mont_str = "พฤศจิกายน"
        case "12" :
            mont_str = "ธันวาคม"
        default:
            mont_str = ""
            break
        }
        
        //เชคถ้าเป็น today ไม่ต้องแสดงวันที่
        if buddhistCalendar!.isDateInToday(date) {
            //Today
            now_Date =  time
        }else {
            now_Date =  time + " " + dd + " " + mont_str + " "  + yyyy
        }
        
        
        return now_Date
    }
    
    func getVersionApp() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        //let build = dictionary["CFBundleVersion"] as! String
        //return "\(version) (\(build))"
        return "V.\(version)"
    }
    
}

public extension String {
    
    func substring(to : Int) -> String {
        let toIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[...toIndex])
    }
    
    func substring(from : Int) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: from)
        return String(self[fromIndex...])
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
