//
//  ErrorMessage.swift
//  Rent Motorcycle
//
//  Created by wisuij on 4/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import Foundation

struct ThisErrorMessage {
    static let ERRORLOGIN = "ไม่สามารถเข้าสู่ระบบได้"
    static let ERRORCANNOTTRANSACTION = "ไม่สามารถทำรายการได้ในขณะนี้"
    static let PLEASEFILLINTHEFORMATION  = "กรุณากรอกข้อมูลให้ครบ"
    static let EXPIREDTOKEN  = "Token หมดอายุ"
    static let NOTCOMPLETEINFORMATION  = "กรุณากรอกข้อมูลให้ครบ"
    
    static let LEAVES_THE_SERVICE_AREA = "รถกำลังใกล้ออกจากเขตบริการ"
    static let AMOUNT_REMAINING = "ยอดเงินของท่านกำลังจะหมด กรุณาเติมเงินเข้าระบบ"
    
    static let ERRORSTATUS400 = "400"
}

