//
//  BaseViewModel.swift
//  Cu Cudson
//
//  Created by wisuij on 7/21/2561 BE.
//  Copyright © 2561 wisuij. All rights reserved.
//

import UIKit

class BaseViewModel: NSObject {
    
    public weak var delegate: BaseViewModelDelegate?
    
    required public init(delegate: BaseViewModelDelegate) {
        self.delegate = delegate
    }

}
