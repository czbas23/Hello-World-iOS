//
//  BaseProtocol.swift
//  Cu Cudson
//
//  Created by wisuij on 7/21/2561 BE.
//  Copyright © 2561 wisuij. All rights reserved.
//

import Foundation

public protocol BaseViewModelDelegate: class {
    
    // Load data
    
    func onDataDidLoad()
    func onDataDidLoadErrorWithMessage(errorMessage:String)
    
    // Loading
    func showLoading()
    func hideLoading()
    
    // Uploade image
    func onUploadeImageComplete()
    
    // Load data trip action
    func onDataTripActionDidLoad()
    func onDataTripActionDidLoadErrorWithMessage(errorMessage:String)
    
}
