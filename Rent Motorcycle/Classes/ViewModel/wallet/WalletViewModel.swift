//
//  WalletViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 18/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol WalletProtocol {
    func getBalance(api:String ,parameters:Parameters?, token:String)
    func getCash(api:String ,parameters:Parameters?, token:String)
    func topUpGenerateQRcode(api:String ,parameters:Parameters?, token:String)
}

class WalletViewModel: BaseViewModel, WalletProtocol{

    var apiType:String!
    var balanceData:JSON!
    var cashData:JSON!
    var generateQRcodeData:JSON!
    
    func getBalance(api:String ,parameters:Parameters?, token:String) {
        apiType = "getBalance"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.balanceData = JSON(responseObject!)
                    print(self.balanceData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getCash(api:String ,parameters:Parameters?, token:String) {
        apiType = "getCash"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.cashData = JSON(responseObject!)
                    print(self.cashData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func topUpGenerateQRcode(api:String ,parameters:Parameters?, token:String) {
        apiType = "topUpGenerateQRcode"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.generateQRcodeData = JSON(responseObject!)
                    print(self.generateQRcodeData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
}
