//
//  RegisterViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 31/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol RegisterProtocol {
    func getNamePrefix(api:String ,parameters:Parameters?)
    func requestOTP(api:String ,parameters:Parameters)
    func verifyOTP(api:String ,parameters:Parameters)
    func register(api:String ,parameters:Parameters,imageDrivingLicense:UIImage,imageSelfieDriving:UIImage)
    func getProfile(api:String ,parameters:Parameters?, token:String)
    func resubmit(api:String ,parameters:Parameters,imageDrivingLicense:UIImage?,imageSelfieDriving:UIImage?)
}

class RegisterViewModel: BaseViewModel, RegisterProtocol {
    
    var apiType:String!
    var namePrefixData:JSON!
    var otpData:JSON!
    var verifyOtpData:JSON!
    var registerData:JSON!
    var profileData:JSON!
    var profileModel:ProfileModel!
    var resubmitData:JSON!
    
    func getNamePrefix(api:String ,parameters:Parameters?) {
        apiType = "getNamePrefix"
        self.delegate?.showLoading()
        
        APIService.sharedManager.getRequestAPIAndResponseList(api: api, parameters: parameters){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.namePrefixData = JSON(responseObject!)
                    print(self.namePrefixData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func requestOTP(api:String ,parameters:Parameters) {
        apiType = "requestOTP"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPI(api:api, parameters: parameters){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.otpData = JSON(responseObject!)
                    print(self.otpData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func verifyOTP(api:String ,parameters:Parameters) {
        apiType = "verifyOTP"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPI(api:api, parameters: parameters){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.verifyOtpData = JSON(responseObject!)
                    print(self.verifyOtpData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func register(api:String ,parameters:Parameters,imageDrivingLicense:UIImage,imageSelfieDriving:UIImage) {
        apiType = "register"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestUploadImageRegister(api: api, parameters: parameters, image: imageDrivingLicense, image2: imageSelfieDriving){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.registerData = JSON(responseObject!)
                    print(self.registerData)
                    //save api token
                    UserDefaults.standard.set(self.registerData["api_token"].stringValue, forKey:"APITOKEN")
                    UserModel.ApiToken = self.registerData["api_token"].stringValue
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getProfile(api:String ,parameters:Parameters?, token:String) {
        apiType = "getProfile"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.profileData = JSON(responseObject!)
                    print(self.profileData)
                    self.profileModel = ProfileModel(dict: responseObject as! Dictionary<String, Any>)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func resubmit(api:String ,parameters:Parameters,imageDrivingLicense:UIImage?,imageSelfieDriving:UIImage?) {
        apiType = "resubmit"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestUploadImageResubmit(api: api, parameters: parameters, image: imageDrivingLicense, image2: imageSelfieDriving){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.resubmitData = JSON(responseObject!)
                    print(self.resubmitData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }

}

//APIService.sharedManager.sendRequestAPI(api:api, parameters: parameters){ responseObject,error in
//    DispatchQueue.main.async {


//func sendRequestUploadImage(api:String, parameters: Parameters?, image:UIImage ,image2:UIImage, token: String, completionHandler: @escaping (NSDictionary?, String?)-> ()){
