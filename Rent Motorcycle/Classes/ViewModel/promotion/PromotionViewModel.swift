//
//  PromotionViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol PromotionProtocol {
    func getPromotion(api:String ,parameters:Parameters?, token:String)
    func getPromotionFindID(api:String ,parameters:Parameters?, token:String)
}

class PromotionViewModel: BaseViewModel, PromotionProtocol{
    
    var apiType:String!
    var promotionData:JSON!
    var promotionFindIDData:JSON!
    
    func getPromotion(api:String ,parameters:Parameters?, token:String) {
        apiType = "getPromotion"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeaderAndResponseList(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.promotionData = JSON(responseObject!)
                    print(self.promotionData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getPromotionFindID(api:String ,parameters:Parameters?, token:String) {
        apiType = "getPromotionFindID"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.promotionFindIDData = JSON(responseObject!)
                    print(self.promotionFindIDData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }

}
