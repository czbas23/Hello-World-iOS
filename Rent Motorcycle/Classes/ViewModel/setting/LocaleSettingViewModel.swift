//
//  LocaleSettingViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 24/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol LocaleSettingProtocol {
    func getLocale(api:String ,parameters:Parameters?, token:String)
    func updateLocale(api:String ,parameters:Parameters?, token:String)
}

class LocaleSettingViewModel: BaseViewModel , LocaleSettingProtocol {
    
    var apiType:String!
    var localeData:JSON!
    var updatelocaleData:JSON!
    
    func getLocale(api:String ,parameters:Parameters?, token:String) {
        apiType = "getLocale"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.localeData = JSON(responseObject!)
                    print(self.localeData)
                    //save local
                    UserDefaults.standard.set(self.localeData["locale"].stringValue, forKey:"LOCAL_LANGUAGE")
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func updateLocale(api:String ,parameters:Parameters?, token:String) {
        apiType = "updateLocale"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.updatelocaleData = JSON(responseObject!)
                    print(self.updatelocaleData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
}
