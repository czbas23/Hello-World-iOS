//
//  ProfileViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 16/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol ProfileProtocol {
    func getProfile(api:String ,parameters:Parameters?, token:String)
    func updateProfile(api:String ,parameters:Parameters?, token:String)
    func requestOtpUpdateProfile(api:String ,parameters:Parameters?, token:String)
}

class ProfileViewModel: BaseViewModel , ProfileProtocol {
    
    var apiType:String!
    var profileData:JSON!
    var updateProfileData:JSON!
    var profileModel:ProfileModel!
    var otpData:JSON!
    
    func getProfile(api:String ,parameters:Parameters?, token:String) {
        apiType = "getProfile"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.profileData = JSON(responseObject!)
                    print(self.profileData)
                    self.profileModel = ProfileModel(dict: responseObject as! Dictionary<String, Any>)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func updateProfile(api:String ,parameters:Parameters?, token:String) {
        apiType = "updateProfile"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.updateProfileData = JSON(responseObject!)
                    print(self.updateProfileData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func requestOtpUpdateProfile(api:String ,parameters:Parameters?, token:String) {
        apiType = "requestOtpUpdateProfile"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.otpData = JSON(responseObject!)
                    print(self.otpData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }

}
