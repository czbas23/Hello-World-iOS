//
//  NotificationViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 4/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol NotificationProtocol {
    func getNotificationHistory(api:String ,parameters:Parameters?, token:String)
    func getFindNotificationHistory(api:String ,parameters:Parameters?, token:String)
}

class NotificationViewModel: BaseViewModel ,  NotificationProtocol{
    
    var apiType:String!
    var notificationHistoryData:JSON!
    var FindNotificationHistoryData:JSON!
    var notificationModel:NotificationModel!
    
    func getNotificationHistory(api:String ,parameters:Parameters?, token:String) {
        apiType = "getNotificationHistory"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeaderAndResponseList(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.notificationHistoryData = JSON(responseObject!)
                    print(self.notificationHistoryData)
                    self.notificationModel = NotificationModel(dict:  responseObject as! [Dictionary<String, Any>])
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    
    func getFindNotificationHistory(api:String ,parameters:Parameters?, token:String) {
        apiType = "getFindNotificationHistory"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.FindNotificationHistoryData = JSON(responseObject!)
                    print(self.FindNotificationHistoryData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }

}
