//
//  IssueViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 3/1/2563 BE.
//  Copyright © 2563 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol IssueProtocol {
    func issueMalfunction(api:String ,parameters:Parameters,image:[UIImage],token:String)
    func issueDamageEquipment(api:String ,parameters:Parameters,image:[UIImage],token:String)
}

class IssueViewModel: BaseViewModel, IssueProtocol {

    var apiType:String!
    var issueMalfunctionData:JSON!
    var issueDamageEquipmentData:JSON!
    
    func issueMalfunction(api:String ,parameters:Parameters,image:[UIImage],token:String) {
        apiType = "issueMalfunction"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestUploadMultiImageWithHeader(api:api, parameters: parameters,image:image, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.issueMalfunctionData = JSON(responseObject!)
                    print(self.issueMalfunctionData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func issueDamageEquipment(api:String ,parameters:Parameters,image:[UIImage],token:String) {
        apiType = "issueDamageEquipment"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestUploadMultiImageWithHeader(api:api, parameters: parameters,image:image, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.issueDamageEquipmentData = JSON(responseObject!)
                    print(self.issueDamageEquipmentData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
}
