//
//  LoginViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 4/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol LoginProtocol {
    func login(api:String ,parameters:Parameters)
    func requestOTP(api:String ,parameters:Parameters)
}

class LoginViewModel: BaseViewModel, LoginProtocol {
    
    var apiType:String!
    var loginData:JSON!
    var otpData:JSON!
    
    func login(api:String ,parameters:Parameters) {
        apiType = "login"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPI(api:api, parameters: parameters){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.loginData = JSON(responseObject!)
                    print(self.loginData)
                    //save api token
                    UserDefaults.standard.set(self.loginData["api_token"].stringValue, forKey:"APITOKEN")
                    UserModel.ApiToken = self.loginData["api_token"].stringValue
                    
                    /*
                    UserModel.name = self.loginData["user"]["name"].stringValue
                    UserModel.surname = self.loginData["user"]["surname"].stringValue
                    */
                    
                    //ลบข้อมูล  name + surname เก่า
                    let prefs = UserDefaults.standard
                    prefs.removeObject(forKey: "USERFULLNAME")

                    
                    /*
                    //check first login เอาไว้เชคการแสดง How to
                    if UserDefaults.standard.string(forKey:"FIRSTLOGIN") != nil {
                        //มีการ login ครั้งแรกแล้ว
                        UserDefaults.standard.removeObject(forKey:"FIRSTLOGIN")
                    }else {
                        UserDefaults.standard.set("first", forKey:"FIRSTLOGIN")
                    }
                    //
                    */
                    
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func requestOTP(api:String ,parameters:Parameters) {
        apiType = "requestOTP"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPI(api:api, parameters: parameters){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.otpData = JSON(responseObject!)
                    print(self.otpData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }

}
