//
//  MainMapViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 6/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol MainMapProtocol {
    func getHomeParking(api:String ,parameters:Parameters?, token:String)
    func getHomeParkingFindID(api:String ,parameters:Parameters?, token:String)
    func getHomeArea(api:String ,parameters:Parameters?, token:String)
    func getUserStatus(api:String ,parameters:Parameters?, token:String)
    func getBalance(api:String ,parameters:Parameters?, token:String)
    func getQRCodeMotorcycle(api:String ,parameters:Parameters?, token:String)
    func getHometrip(api:String ,parameters:Parameters?, token:String)
    func tripActionStart(api:String ,parameters:Parameters?, token:String)
    func tripActionPause(api:String ,parameters:Parameters?, token:String)
    func tripActionResume(api:String ,parameters:Parameters?, token:String)
    func tripActionEnd(api:String ,parameters:Parameters?, token:String)
    func getTripBill(api:String ,parameters:Parameters?, token:String)
    func getRejectreason(api:String ,parameters:Parameters?, token:String)
    func tripSatisfaction(api:String ,parameters:Parameters?, token:String)
}

class MainMapViewModel: BaseViewModel, MainMapProtocol {
    
    var apiType:String!
    var parkingData:JSON!
    var parkingModel:ParkingModel!
    var parkingFindIDData:JSON!
    var parkingIDModel:ParkingIDModel!
    var areaData:JSON!
    var parkingAreaModel:ParkingAreaModel!
    var userData:JSON!
    var balanceData:JSON!
    var qrCodeMotorcycleData:JSON!
    var motorcycleModel:MotorcycleModel!
    var hometripData:JSON!
    var hometripModel:HometripModel!
    
    var actionStartData:JSON!
    var actionPauseData:JSON!
    var actionResumeData:JSON!
    var actionEndData:JSON!
    
    var tripBillData:JSON!
    var tripBillModel:TripBillModel!
    
    var getRejectreasonData:JSON!
    var tripSatisfactionData:JSON!
    
    func getHomeParking(api:String ,parameters:Parameters?, token:String) {
        apiType = "getHomeParking"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeaderAndResponseList(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.parkingData = JSON(responseObject!)
                    print(self.parkingData)
                    self.parkingModel =  ParkingModel(dict: responseObject as! [Dictionary<String, Any>])
                    //print(self.parkingModel.parkingList?[0].name!)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getHomeArea(api:String ,parameters:Parameters?, token:String) {
        apiType = "getHomeArea"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeaderAndResponseList(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.areaData = JSON(responseObject!)
                    print(self.areaData)
                    self.parkingAreaModel =  ParkingAreaModel(dict: responseObject as! [Dictionary<String, Any>])
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getHomeParkingFindID(api:String ,parameters:Parameters?, token:String) {
        apiType = "getHomeParkingFindID"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.parkingFindIDData = JSON(responseObject!)
                    print(self.parkingFindIDData)
                    self.parkingIDModel = ParkingIDModel(dict: responseObject as! Dictionary<String, Any>)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getUserStatus(api:String ,parameters:Parameters?, token:String) {
        apiType = "getUserStatus"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.userData = JSON(responseObject!)
                    print(self.userData)
                    UserModel.Status = self.userData["status"].intValue
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getBalance(api:String ,parameters:Parameters?, token:String) {
        apiType = "getBalance"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.balanceData = JSON(responseObject!)
                    print(self.balanceData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getQRCodeMotorcycle(api:String ,parameters:Parameters?, token:String) {
        apiType = "getQRCodeMotorcycle"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.qrCodeMotorcycleData = JSON(responseObject!)
                    print(self.qrCodeMotorcycleData)
                    self.motorcycleModel = MotorcycleModel(dict: responseObject as! Dictionary<String, Any>)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getHometrip(api:String ,parameters:Parameters?, token:String) {
        apiType = "getHometrip"
        //self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                //self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.hometripData = JSON(responseObject!)
                    print(self.hometripData)
                    self.hometripModel = HometripModel(dict: responseObject as! Dictionary<String, Any>)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    
    func tripActionStart(api:String ,parameters:Parameters?, token:String) {
        apiType = "tripActionStart"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.actionStartData = JSON(responseObject!)
                    print(self.actionStartData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func tripActionPause(api:String ,parameters:Parameters?, token:String) {
        apiType = "tripActionPause"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.actionPauseData = JSON(responseObject!)
                    print(self.actionPauseData)
                    //self.delegate?.onDataDidLoad()
                    self.delegate?.onDataTripActionDidLoad()
                }else{
                    let errorMessage = error
                    //self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                    self.delegate?.onDataTripActionDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func tripActionResume(api:String ,parameters:Parameters?, token:String) {
        apiType = "tripActionResume"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.actionResumeData = JSON(responseObject!)
                    print(self.actionResumeData)
                    //self.delegate?.onDataDidLoad()
                    self.delegate?.onDataTripActionDidLoad()
                }else{
                    let errorMessage = error
                    //self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                    self.delegate?.onDataTripActionDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func tripActionEnd(api:String ,parameters:Parameters?, token:String) {
        apiType = "tripActionEnd"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.actionEndData = JSON(responseObject!)
                    print(self.actionEndData)
                    //self.delegate?.onDataDidLoad()
                    self.delegate?.onDataTripActionDidLoad()
                }else{
                    let errorMessage = error
                    //self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                    self.delegate?.onDataTripActionDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getTripBill(api:String ,parameters:Parameters?, token:String) {
        apiType = "getTripBill"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.tripBillData = JSON(responseObject!)
                    print(self.tripBillData)
                    self.tripBillModel = TripBillModel(dict:responseObject as! Dictionary<String, Any>)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getRejectreason(api:String ,parameters:Parameters?, token:String) {
        apiType = "getRejectreason"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeaderAndResponseList(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.getRejectreasonData = JSON(responseObject!)
                    print(self.getRejectreasonData)
                    //self.parkingAreaModel =  ParkingAreaModel(dict: responseObject as! [Dictionary<String, Any>])
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func tripSatisfaction(api:String ,parameters:Parameters?, token:String) {
        apiType = "tripSatisfaction"
        self.delegate?.showLoading()
        APIService.sharedManager.sendRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.tripSatisfactionData = JSON(responseObject!)
                    print(self.tripSatisfactionData)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
}
