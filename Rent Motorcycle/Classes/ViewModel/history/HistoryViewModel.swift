//
//  HistoryViewModel.swift
//  Rent Motorcycle
//
//  Created by wisuij on 29/12/2562 BE.
//  Copyright © 2562 suij. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol HistoryProtocol {
    func getTripHistory(api:String ,parameters:Parameters?, token:String)
    func getFindTripHistory(api:String ,parameters:Parameters?, token:String)
}

class HistoryViewModel: BaseViewModel, HistoryProtocol {
    
    var apiType:String!
    var tripHistoryData:JSON!
    var findtripHistoryData:JSON!
    var tripHistoryModel:TripHistoryModel!
    var findTripHistoryModel:FindTripHistoryModel!

    
    func getTripHistory(api:String ,parameters:Parameters?, token:String) {
        apiType = "getTripHistory"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeaderAndResponseList(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.tripHistoryData = JSON(responseObject!)
                    print(self.tripHistoryData)
                    self.tripHistoryModel = TripHistoryModel(dict: responseObject as! [Dictionary<String, Any>])
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }
    
    func getFindTripHistory(api:String ,parameters:Parameters?, token:String) {
        apiType = "getFindTripHistory"
        self.delegate?.showLoading()
        APIService.sharedManager.getRequestAPIWithHeader(api:api, parameters: parameters, token:token){ responseObject,error in
            DispatchQueue.main.async {
                self.delegate?.hideLoading()
                if(responseObject != nil){
                    self.findtripHistoryData = JSON(responseObject!)
                    print(self.findtripHistoryData)
                    self.findTripHistoryModel = FindTripHistoryModel(dict: responseObject as! Dictionary<String, Any>)
                    self.delegate?.onDataDidLoad()
                }else{
                    let errorMessage = error
                    self.delegate?.onDataDidLoadErrorWithMessage(errorMessage: errorMessage!)
                }
            }
        }
    }

}
